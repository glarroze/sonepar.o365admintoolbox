﻿using System;
using Microsoft.Graph;
using Graph = Microsoft.Graph;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sonepar.O365AdminToolbox.Domain;
using Sonepar.O365AdminToolbox.Entity;
using System.Data.Entity;

namespace Sonepar.O365AdminToolbox.GraphPort
{
    public class LicenseService : BaseUserService, ILicenseService
    {
        private IUserService userService;
        private ToolboxDbContext toolboxContext;

        public LicenseService(
            IAuthenticationService helper,
            IAppSettings settings,
            IUserService userService,
            IFriendlyNameService nameService,
            ToolboxDbContext toolboxContext) : base(helper, settings, nameService)
        {
            this.userService = userService;
            this.toolboxContext = toolboxContext;
        }      

        public async Task<IEnumerable<License>> GetServicePlansForUser(string userId, IEnumerable<string> countries)
        {
            var plans = await GetServicePlansAvailable();
            var client = helper.GetGraphClientAsAdmin();

            var user = await GetGraphUser(userId, countries);

            var lic = user.AssignedLicenses.Select(x =>
            {
                var plan = plans.FirstOrDefault(p => p.Id == x.SkuId).Copy();
                foreach (var s in x.DisabledPlans)
                {
                    var service = plan.Services.FirstOrDefault(serv => serv.Service.Id == s);
                    if (service != null)
                    {
                        service.IsActive = false;
                    }
                }
                return plan;
            });
            return lic;
        }

        public async Task AddOrUpdateServicePlanForUser(string userId, License servicePlan, IEnumerable<string> countries)
        {
            var plans = await GetServicePlansAvailable();
            var planRef = plans.FirstOrDefault(p => p.Id == servicePlan.Id).Copy();

            var inGroupNames = base.GetCountryGroupNames(countries);
            var client = helper.GetGraphClientAsAdmin();

            Graph.User graphUser = await GetGraphUser(userId, countries);
            graphUser.AdditionalData = null;
            if (!String.IsNullOrWhiteSpace(servicePlan.UsageLocation) && string.Compare(graphUser.UsageLocation, servicePlan.UsageLocation, true) != 0)
            {
                graphUser.UsageLocation = servicePlan.UsageLocation;
                await Client.Users[graphUser.Id].Request().UpdateAsync(graphUser);
            }
            var servicesToDisable = servicePlan.Services.Where(x => !x.IsActive)
                .Select(x => planRef.Services.FirstOrDefault(y => y.Service.Id == x.Service.Id))
                .Concat(planRef.Services.Where(x => !servicePlan.Services.Any(z => z.Service.Id == x.Service.Id)))
                .Select(x => x.Service.Id);
            var toAssign = new AssignedLicense() { SkuId = servicePlan.Id, DisabledPlans = servicesToDisable };
            var managedUser = toolboxContext.ManagedUsers.FirstOrDefault(selectedUser => selectedUser.Id == graphUser.Id);
            managedUser.HasLicenseAssigned = true;
            toolboxContext.SaveChanges();
            await client.Users[userId].AssignLicense(new AssignedLicense[] { toAssign }, Enumerable.Empty<Guid>()).Request().PostAsync();
            
        }

        public async Task RemoveServicePlanForUser(string userId, Guid servicePlanId, IEnumerable<string> countries)
        {
            var plans = await GetServicePlansAvailable();
            var planRef = plans.FirstOrDefault(p => p.Id == servicePlanId).Copy();

            var client = helper.GetGraphClientAsAdmin();
            var user = await userService.GetUser(userId, countries);

            await client.Users[userId].AssignLicense(Enumerable.Empty<AssignedLicense>(), new Guid[] { servicePlanId }).Request().PostAsync();

            var userUpdated = await userService.GetUser(userId, countries);
            if (userUpdated.AssignedLicenses.Count() == 0)
            {
                var managedUser = toolboxContext.ManagedUsers.FirstOrDefault(selectedUser => selectedUser.Id == userUpdated.Id);
                if(managedUser != null)
                {
                    managedUser.HasLicenseAssigned = false;
                    await toolboxContext.SaveChangesAsync();
                }
            }
        }

    }
}
