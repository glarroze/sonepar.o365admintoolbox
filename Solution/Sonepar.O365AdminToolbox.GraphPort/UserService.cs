﻿using System;
using Graph = Microsoft.Graph;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Reflection;
using Newtonsoft.Json;
using Sonepar.O365AdminToolbox.GraphPort;
using System.Management.Automation;
using System.Threading;
using System.Collections.ObjectModel;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using System.Management.Automation.Runspaces;
using System.Security;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.SharePoint.Client.UserProfiles;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web;
using Sonepar.O365AdminToolbox.Entity;

namespace Sonepar.O365AdminToolbox.Domain
{
    public class UserService : BaseUserService, IUserService
    {
        private const int UserPageSize = 999;
        private const int maxFilterCount = 10;
        private IGlobalSettings globalSettings;
        private ToolboxDbContext toolboxContext;



        public UserService(
            IAuthenticationService helper,
            IAppSettings settings,
            IGlobalSettings globalSettings,
            ToolboxDbContext toolboxContext,
            IFriendlyNameService nameService) : base(helper, settings, nameService)
        {
            this.globalSettings = globalSettings;
            this.toolboxContext = toolboxContext;
        }

        public bool IsAadIdentity(ClaimsIdentity user) => user.Claims.All(c => c.Issuer == "https://sts.windows.net/" + settings.TenantId);
        public bool IsAppIdentity(ClaimsIdentity user) => user.Claims.All(c => c.Issuer == settings.Issuer);

        public async Task<IEnumerable<Claim>> GetUserManagedGroups(string userPrincipalName)
        {
            var user = (await Client.Users.Request().Filter($"UserPrincipalName eq '{userPrincipalName}'").GetAsync()).FirstOrDefault();

            if (user == null)
            {
                return Enumerable.Empty<Claim>();
            }
            var groupsOfUser = await Client.Users[user.Id].MemberOf.Request().GetAsync();

            List<Graph.Group> groups = groupsOfUser.CurrentPage.OfType<Graph.Group>().ToList();
            while (groupsOfUser.NextPageRequest != null)
            {
                groupsOfUser = await groupsOfUser.NextPageRequest.GetAsync();
                groups.AddRange(groupsOfUser.CurrentPage.OfType<Graph.Group>());
            }

            Regex re = new Regex(settings.ManagedGroupsRegEx, RegexOptions.IgnoreCase);
            var managed = groups.Where(g =>
                re.IsMatch(g.DisplayName)
                ).Select(
                    g =>
                    new System.Security.Claims.Claim(AdminOfClaimType, re.Replace(g.DisplayName, "$1").ToUpper()));
            return managed;
        }
        private static string[] UserProperties = new String[] { "id", "accountEnabled", "assignedLicenses", "assignedPlans", "businessPhones", "city", "companyName", "country", "department", "displayName", "givenName", "jobTitle", "mail", "mailNickname", "mobilePhone", "onPremisesImmutableId", "onPremisesLastSyncDateTime", "onPremisesSecurityIdentifier", "onPremisesSyncEnabled", "passwordPolicies", "passwordProfile", "officeLocation", "postalCode", "preferredLanguage", "provisionedPlans", "proxyAddresses", "state", "streetAddress", "surname", "usageLocation", "userPrincipalName", "userType" };
        public async Task<bool> isCurrentUserMemberOfLicenceManagers(string userPrincipalName)
        {
            var targetGroupCollection = await Client.Groups.Request()
                                        .Filter($"startsWith(displayName,'{settings.LicencesNamesEditorGroup}')")
                                        .GetAsync();
            if (targetGroupCollection.Count() == 0)
            {
                throw new InvalidOperationException($"{settings.LicencesNamesEditorGroup} can't be found");
            }
            var targetGroup = targetGroupCollection.ToList().Where(g => g.DisplayName.ToUpper() == settings.LicencesNamesEditorGroup.ToUpper()).FirstOrDefault();

            var groupIds = new List<String>()
            {
                targetGroup.Id
            };

            var Membership = await Client.Users[userPrincipalName].CheckMemberGroups(groupIds).Request().PostAsync();
            return Membership.Count > 0;
        }

        public async Task<IEnumerable<Graph.Group>> GetUserGroups(IEnumerable<string> countries)
        {           
            return await GetUserGroupsByPage(countries);
        }
        private async Task<IEnumerable<Graph.Group>> GetUserGroupsByPage(IEnumerable<string> groupNames)
        {
            List<Graph.Group> groups = new List<Graph.Group>();
            for (var i = 0; i < Math.Ceiling(((double)groupNames.Count()) / maxFilterCount); i++)
            {
                var currentPageGroups = await GetUserGroups(groupNames, i);
                groups.AddRange(currentPageGroups);
            }
            return groups;
        }

        private async Task<IEnumerable<Graph.Group>> GetUserGroups(IEnumerable<string> countries, int page)
        {
            var groupNames = GetCountryGroupNames(countries);
            var filterGroups = String.Join(" or ", groupNames.Skip(page * maxFilterCount).Take(maxFilterCount).Select(x => $"DisplayName eq '{x}'"));

            var groups = await FetchGroups(filterGroups);
            return groups;
        }


        public async Task<IEnumerable<User>> GetUsersByGroup(IEnumerable<string> countries, string groupId)
        {
            var servs = await base.GetServicePlansAvailable();
            var users = new List<User>();
            int numberByPage = settings.MembersByRequest != null ? int.Parse(settings.MembersByRequest) : 999;
            var groupMembers = await Client.Groups[groupId].Members.Request().Select("*").Top(numberByPage).GetAsync();
            while (true)
            {
                users.AddRange(groupMembers.OfType<Microsoft.Graph.User>().Where(u => !users.Any(x => x.Id == u.Id)).Select(u => MapUser(u, servs)));
                // groupMembers.OfType<Microsoft.Graph.User>().Select(u => users.Single(x => x.Id == u.Id)).ToList().ForEach(u => u.TenantCountries = (u.TenantCountries ?? Enumerable.Empty<string>()).Concat(countries.Where(c => String.Compare(GetCountryGroupName(c), group.DisplayName, true) == 0)));
                if (groupMembers.NextPageRequest == null)
                {
                    break;
                }
                groupMembers = await groupMembers.NextPageRequest.GetAsync();
            }
            return users;
        }


        public async Task<PagedUsersResponse> getGroupMemberPaged(IEnumerable<string> countries, string groupId, string groupDisplayName, string skipToken)
        {

            var requestUrl = $"https://graph.microsoft.com/v1.0/groups/{groupId}/members?$select=*&$top={UserPageSize}";
            // var requestUrl = $"https://graph.microsoft.com/v1.0/groups/{groupId}/members?$select=*&$top={3}";
            if (!string.IsNullOrEmpty(skipToken))
            {
                requestUrl = $"{requestUrl}&$skiptoken={skipToken}";
            }
            var servs = await base.GetServicePlansAvailable();
            var users = new List<User>();

            using (var httpClient = new HttpClient())
            {
                var token = await helper.AcquireTokenAsyncForAdmin(globalSettings.GraphResourceUrl);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = await httpClient.GetAsync(requestUrl);
                var jsonString = response.Content.ReadAsStringAsync().Result;

                string nextSkipToken = "";
                if (JObject.Parse(jsonString)["@odata.nextLink"] != null)
                {
                    var skipTokenResult = JObject.Parse(jsonString)["@odata.nextLink"].Value<string>();
                    Uri nextPageUri = new Uri(skipTokenResult);
                    nextSkipToken = HttpUtility.ParseQueryString(nextPageUri.Query).Get("$skiptoken");
                }                                         

                var jsonUsers = JObject.Parse(jsonString)["value"].ToString();
                var groupMembers = JsonConvert.DeserializeObject<List<Graph.User>>(jsonUsers);

                users.AddRange(groupMembers.OfType<Graph.User>().Where(u => !users.Any(x => x.Id == u.Id)).Select(u => MapUser(u, servs)));
                groupMembers.OfType<Graph.User>().Select(u => users.Single(x => x.Id == u.Id)).ToList().ForEach(u => u.TenantCountries = (u.TenantCountries ?? Enumerable.Empty<string>()).Concat(countries.Where(c => String.Compare(GetCountryGroupName(c), groupDisplayName, true) == 0)));
                var pagedResponse = new PagedUsersResponse
                {
                    skipToken = nextSkipToken,
                    users = users
                };
                return pagedResponse;
            }

            // var groupMembers = await Client.Groups[groupId].Members.Request().Skip(2).Top(2).Select("*").GetAsync();
            
        }

        //public async Task<IEnumerable<User>> GetUsersManaged(IEnumerable<string> countries)
        //{
        //    var users = new List<User>();
        //    var servs = await base.GetServicePlansAvailable();
        //    await GetUserByGroupPage(countries, users, servs);
        //    return users;
        //}
        public async Task<IEnumerable<ManagedUsers>> GetUsersManaged(IEnumerable<string> countries)
        {
            var users = new List<ManagedUsers>();
            // var servs = await base.GetServicePlansAvailable();
            // await GetUserByGroupPage(countries, users, servs);
            foreach (string country in countries)
            {
                var _country = country;
                var managedUsersRow = (from u in toolboxContext.ManagedUsers
                                       where u.GroupIds.Contains(country)
                                       select u).ToList().Where(u => u.GroupIds.Split(';').Contains(country)).ToList();

               

                users.AddRange(managedUsersRow);
            };
            var distinctUser = users.Distinct().ToList();
            distinctUser.ForEach(u =>
            {
                var grpIdsTemp = u.GroupIds.Split(';');

                foreach (var uc in u.GroupIds.Split(';'))
                {
                    if (!countries.Contains(uc))
                    {
                        grpIdsTemp = grpIdsTemp.Where(s => s != uc).ToArray();
                    }
                }

                u.GroupIds = string.Join(";", grpIdsTemp);
            });
            return distinctUser;
        }

        public async Task<IEnumerable<Graph.User>> GetAllUsers()
        {
            return await Client.Users.Request().GetAsync();
        }

        

        private async Task GetUserByGroupPage(IEnumerable<string> groupNames, List<User> users, IEnumerable<License> servs)
        {
            for (var i = 0; i < Math.Ceiling(((double)groupNames.Count()) / maxFilterCount); i++)
            {
                await GetUsersByGroupPageNumber(groupNames, users, servs, i);
            }
        }

       

        private async Task GetUsersByGroupPageNumber(IEnumerable<string> countries, List<User> users, IEnumerable<License> servs, int page)
        {

            var groupNames = GetCountryGroupNames(countries);
            var filterGroups = String.Join(" or ", groupNames.Skip(page * maxFilterCount).Take(maxFilterCount).Select(x => $"DisplayName eq '{x}'"));





            // var groups = await FetchGroups(filterGroups);

            //foreach (var group in groups)
            //{
            //    var groupMembers = await Client.Groups[group.Id].Members.Request().Select("*").Top(UserPageSize).GetAsync();
            //    while (true)
            //    {
            //        users.AddRange(groupMembers.OfType<Microsoft.Graph.User>().Where(u => !users.Any(x => x.Id == u.Id)).Select(u => MapUser(u, servs)));
            //        groupMembers.OfType<Microsoft.Graph.User>().Select(u => users.Single(x => x.Id == u.Id)).ToList().ForEach(u => u.TenantCountries = (u.TenantCountries ?? Enumerable.Empty<string>()).Concat(countries.Where(c => String.Compare(GetCountryGroupName(c), group.DisplayName, true) == 0)));
            //        if (groupMembers.NextPageRequest == null)
            //        {
            //            break;
            //        }
            //        groupMembers = await groupMembers.NextPageRequest.GetAsync();
            //    }
            //}
        }

        private async Task<List<Microsoft.Graph.Group>> FetchGroups(string filterGroups)
        {
            var groupsPaged = await Client.Groups.Request().Filter(filterGroups).Select(x => new { x.Id, x.DisplayName }).GetAsync();
            var groups = groupsPaged.CurrentPage.ToList();
            while (groupsPaged.NextPageRequest != null)
            {
                groupsPaged = await groupsPaged.NextPageRequest.GetAsync();
                groups.AddRange(groupsPaged.CurrentPage.ToList());
            }

            return groups;
        }

        public async void BindUser(UserPassCredentials user)
        {
            var userClient = helper.GetGraphClientAsUser(user);
            if (userClient == null) throw new Exception("Could not authenticate user");

            var me = await userClient.Me.Request().GetAsync();
            if (userClient == null) throw new Exception("Could not retreive user information");
        }
        public async Task<User> GetUser(string Id, IEnumerable<string> countries)
        {
            //var inGroupNames = GetCountryGroupNames(countries);
            Graph.User user = await GetGraphUser(Id, countries);
            var plans = await base.GetServicePlansAvailable();

            return MapUser(user, plans);
        }
        public async Task CheckUser(string Id, IEnumerable<string> countries)
        {
            //var inGroupNames = GetCountryGroupNames(countries);
            await GetGraphUser(Id, countries);
        }

        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        public async Task<RevokationState> UpdateUserStatut(string Id, bool isEnable)
        {

            var revokationState = new RevokationState
            {
                IsRevoked = false,
                IsBloqued = false,
                IsDeconnectedFromSPO = false
            };

            if (!isEnable)
            {
                var currentGraphUserToDisconnect = await Client.Users[Id]
                            .Request().GetAsync();
                var SPSIgnoutStatus = await RevokeSPOSignIn(currentGraphUserToDisconnect.Mail);
                if (SPSIgnoutStatus.userStatut == "Disabled")
                {
                    revokationState.IsDeconnectedFromSPO = true;
                }
            }

            var userUpdate = new Graph.User
            {
                AccountEnabled = isEnable,
                //PasswordProfile = new Graph.PasswordProfile
                //{
                //    ForceChangePasswordNextSignIn = false,
                //    Password = CreatePassword(12)
                //}
            };
            await Client.Users[Id]
                            .Request()
                            .UpdateAsync(userUpdate);
            revokationState.IsBloqued = !isEnable;



            if (!isEnable)
            {
                //await Client.Users[Id]
                //            .RevokeSignInSessions()
                //            .Request().PostAsync();
                //     



                using (var httpClient = new HttpClient())
                {
                    var token = await helper.AcquireTokenAsyncForAdmin(globalSettings.GraphResourceUrl);
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    // 
                    var revoqueSessionResponse = await httpClient.PostAsync($"https://graph.microsoft.com/v1.0/users/{Id}/revokeSignInSessions", null);
                    var workspaceContent = await revoqueSessionResponse.Content.ReadAsStringAsync();
                    if (revoqueSessionResponse.StatusCode == HttpStatusCode.OK)
                    {
                        revokationState.IsRevoked = true;
                    }
                    else
                    {
                        revokationState.IsRevoked = false;
                        revokationState.Message = revoqueSessionResponse.ReasonPhrase;
                    }


                }
            }
            return revokationState;
        }

        public static object GetDefault(Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return null;
        }

        private string GetName(PropertyInfo p)
        {
            var ca = p.GetCustomAttributes<JsonPropertyAttribute>().FirstOrDefault();
            if (ca != null)
            {
                return ca.PropertyName;
            }
            return p.Name;
        }



        public async Task<AzureFunctionResponse> GetMFAStatus(string userPrincipalName)
        {

            var askForlicenseURL = $"{settings.GetMFAStatusFunctionUrl}&UserPrincipalName={userPrincipalName}";
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    var reponse = new AzureFunctionResponse { userStatut = "undefined" };
                    // client.DefaultRequestHeaders.Add("x-functions-key", securityCode);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.GetAsync(askForlicenseURL);
                    using (HttpContent respContent = response.Content)
                    {
                        // ... Read the response as a string.
                        var tr = respContent.ReadAsStringAsync().Result;
                        // ... deserialize the response, we know it has a 'result' field
                        dynamic azureResponse = JsonConvert.DeserializeObject<AzureFunctionResponse>(tr);
                        // ... read the data we want
                        reponse = azureResponse;
                        reponse.userPrincipalName = userPrincipalName;
                    }
                    return reponse;
                }
                catch(Exception e)
                {
                    throw e;
                }
                
            }

        }


        public async Task<AzureFunctionResponse> ChangeMFAStatus(string userPrincipalName, string status)
        {

            using (var httpClient = new HttpClient())
            {
                var reponse = new AzureFunctionResponse { userStatut = "undefined" };
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var payload = new Dictionary<string, string>
                {
                    {"status", status},
                    {"userPrincipalName", userPrincipalName}
                };
                string strPayload = JsonConvert.SerializeObject(payload);
                HttpContent content = new StringContent(strPayload, Encoding.UTF8, "application/json");

                var mfaStatutResponse = await httpClient.PostAsync($"{settings.ChangeMFAStatusFunctionUrl}", content);

                var tr = mfaStatutResponse.Content.ReadAsStringAsync().Result;

                dynamic azureResponse = JsonConvert.DeserializeObject<AzureFunctionResponse>(tr);
                // ... read the data we want
                reponse = azureResponse;
                return reponse;
            }
        }



        public async Task<AzureFunctionResponse> RevokeSPOSignIn(string userPrincipalName)
        {

            using (HttpClient client = new HttpClient())
            {
                var reponse = new AzureFunctionResponse { userStatut = "undefined" };
                var askForlicenseURL = $"{settings.SPOSignOutFunctionUrl}&UserPrincipalName={userPrincipalName}";
                // client.DefaultRequestHeaders.Add("x-functions-key", securityCode);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                using (HttpResponseMessage response = await client.GetAsync(askForlicenseURL))
                using (HttpContent respContent = response.Content)
                {
                    // ... Read the response as a string.
                    var tr = respContent.ReadAsStringAsync().Result;
                    // ... deserialize the response, we know it has a 'result' field
                    dynamic azureResponse = JsonConvert.DeserializeObject<AzureFunctionResponse>(tr);
                    // ... read the data we want
                    reponse = azureResponse;
                }
                return reponse;
            }
        }
    }
}



