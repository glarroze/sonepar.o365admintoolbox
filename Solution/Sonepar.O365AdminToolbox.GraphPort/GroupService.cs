﻿using System;
using Graph = Microsoft.Graph;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sonepar.O365AdminToolbox.Domain;
using Sonepar.O365AdminToolbox.Entity;
using System.Data.Entity;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;


namespace Sonepar.O365AdminToolbox.GraphPort
{
    public class GroupService : BaseUserService, IGroupService
    {
        private static readonly IEnumerable<String> GroupTypes = new String[] { "Unified" };
        private const int maxFilterCount = 10;
        private IUserService userService;
        private ToolboxDbContext toolboxContext;
        private IGlobalSettings globalSettings;

        public GroupService(
            IAuthenticationService helper,
            IAppSettings settings,
            IUserService userService,
            IGlobalSettings globalSettings,
            IFriendlyNameService nameService,
            ToolboxDbContext toolboxContext) : base(helper, settings, nameService)
        {
            this.userService = userService;
            this.toolboxContext = toolboxContext;
            this.globalSettings = globalSettings;
        }

        #region IGroupService

        public async Task<IEnumerable<Group>> GetGroupsManagedByUser(IEnumerable<string> countries)
        {
            var groups = new List<Microsoft.Graph.Group>();
            await GetGroupsByCountryPage(countries, groups);

            var officeObjectIds = await toolboxContext.OfficeObjects.Select(t => t.Id).ToListAsync();
            groups = groups.Where(t => !officeObjectIds.Contains(t.Id)).ToList();

            return groups.Select(MapGroup);
        }

        public async Task<Group> GetGroup(string id, IEnumerable<string> countries)
        {
            var group = await Client.Groups[id].Request().GetAsync();
            group.Owners = await Client.Groups[id].Owners.Request().GetAsync();

            if (countries.Any(x => x.ToLower() == group.AdditionalData[settings.GroupCountryExtendedPropertyName].ToString().ToLower()))
            {
                return await MapGroupWithOwners(group);
            }
            return null;
        }

        public async Task<bool> GroupExists(string groupName)
        {
            var officeObjectList = await toolboxContext.OfficeObjects.Where(o=> o.Name == groupName).ToListAsync();
            var officeObjectListFull = await toolboxContext.OfficeObjects.ToListAsync();
            bool groupExistsInDatabase = officeObjectList.Count() > 0;
            if (groupExistsInDatabase)
            {
                throw new InvalidOperationException($"{groupName} allready exists in toolbox database");
            }

            var targetGroupCollection = await Client.Groups.Request()
                                        .Filter($"startsWith(displayName,'{groupName}')")
                                        .GetAsync();
            var targetGroup = targetGroupCollection.ToList().Where(g => g.DisplayName.ToUpper() == groupName.ToUpper()).FirstOrDefault();
            bool groupExistsInOffice = targetGroupCollection.ToList().Where(g => g.DisplayName.ToUpper() == groupName.ToUpper()).Count() > 0;
            if (groupExistsInOffice)
            {
                throw new InvalidOperationException($"{groupName} allready exists in Office");
            }

            return false;
        }

        public async Task<Group> NewGroup(Group g, IEnumerable<string> countries)
        {
            CheckCountry(countries, g.Country);
            var groupexist = await GroupExists(g.Name);
            if (groupexist)
            {
                throw new InvalidOperationException("Group allready exists");
            }
            var group = await Client.Groups.Request().AddAsync(UnmapGroup(g));
            group = await Client.Groups[group.Id].Request().GetAsync();

            var defaultOwners = (await FetchOwners(group)) ?? Enumerable.Empty<Member>();

            if (g.Owners != null && g.Owners.Count() > 0)
            {
                await AddGroupOwners(group, g.Owners, countries);

                await Task.WhenAll(defaultOwners.Select(owner => Client.Groups[group.Id].Owners[owner.Id].Reference.Request().DeleteAsync()));
            }

            return await GetGroup(group.Id, countries);
        }

        public async Task<Group> UpdateGroup(Group group, IEnumerable<string> countries)
        {
            // await CheckCountryCountry(group.Id, countries);
            var groupExists = await GroupExists(group.Name);
            if (groupExists)
            {
                throw new InvalidOperationException("Group allready exists");
            }
            var userUpdate = new Graph.Group
            {
                DisplayName = group.Name,
                MailNickname = System.Text.RegularExpressions.Regex.Replace(group.Name, "[^a-zA-Z0-9.\\-_]", String.Empty)
            };

            await Client.Groups[group.Id]
            .Request()
            .UpdateAsync(userUpdate);
            return await GetGroup(group.Id, countries);

            // await Client.Groups[groupId].Request().DeleteAsync();
        }

        public async Task DeleteGroup(string groupId, IEnumerable<string> countries)
        {
            await CheckCountryCountry(groupId, countries);
            await Client.Groups[groupId].Request().DeleteAsync();
        }

        public async Task<IEnumerable<Member>> GetGroupMembers(string id, IEnumerable<string> countries)
        {
            IEnumerable<Member> members = null;
            await CheckGroupCountry(id, countries);
            var memberReq = Client.Groups[id].Members.Request();
            var memberPage = await memberReq.GetAsync();
            if (memberPage != null)
            {
                List<Graph.DirectoryObject> memberList = memberPage.ToList();
                while (memberPage.NextPageRequest != null)
                {
                    memberPage = await memberPage.NextPageRequest.GetAsync();
                    memberList.AddRange(memberPage);
                }
                members = memberList.OfType<Graph.Group>().Select(z => MapGroup(z)).Cast<Member>()
                    .Concat(memberList.OfType<Graph.User>().Select(MapUser));
            }

            return members;
        }

        public async Task AddGroupMember(string groupId, string memberId, IEnumerable<string> countries)
        {
            await CheckCountryCountry(groupId, countries);

            var member = await Client.DirectoryObjects[memberId].Request().GetAsync();
            await CheckMemberCountry(countries, member);

            await Client.Groups[groupId].Members.References.Request().AddAsync(member);
        }

        public async Task RemoveGroupMember(string groupId, string memberId, IEnumerable<string> countries)
        {
            await CheckCountryCountry(groupId, countries);
            var member = await Client.Groups[groupId].Members[memberId].Request().GetAsync();

            await CheckMemberCountry(countries, member);

            await Client.Groups[groupId].Members[memberId].Reference.Request().DeleteAsync();
        }

        public async Task<IEnumerable<Member>> GetGroupOwners(string id, IEnumerable<string> countries)
        {
            IEnumerable<Member> owners = null;
            await CheckGroupCountry(id, countries);
            var memberReq = Client.Groups[id].Owners.Request();
            var ownerPage = await memberReq.GetAsync();
            if (ownerPage != null)
            {
                List<Graph.DirectoryObject> ownerList = ownerPage.ToList();
                while (ownerPage.NextPageRequest != null)
                {
                    ownerPage = await ownerPage.NextPageRequest.GetAsync();
                    ownerList.AddRange(ownerPage);
                }
                owners = ownerList.OfType<Graph.Group>().Select(z => MapGroup(z)).Cast<Member>()
                    .Concat(ownerList.OfType<Graph.User>().Select(MapUser));
            }

            return owners;
        }

        public async Task AddGroupOwner(string groupId, string ownerId, IEnumerable<string> allowedCountries)
        {
            await CheckCountryCountry(groupId, allowedCountries);
            var owner = await Client.DirectoryObjects[ownerId].Request().GetAsync();
            await CheckMemberCountry(allowedCountries, owner);
            await Client.Groups[groupId].Owners.References.Request().AddAsync(owner);

        }

        public async Task RemoveGroupOwner(string groupId, string ownerId, IEnumerable<string> allowedCountries)
        {
            await CheckCountryCountry(groupId, allowedCountries);
            var owner = await Client.Groups[groupId].Owners[ownerId].Request().GetAsync();
            await CheckMemberCountry(allowedCountries, owner);
            await Client.Groups[groupId].Owners[ownerId].Reference.Request().DeleteAsync();
        }

        public Group MapGroup(OfficeObject o)
        {
            return new Group
            {
                Id = o.Id,
                Name = o.Name,
                Country = o.Country.ToLower(),
                Description = o.Name,
                MailNickname = o.Name,
                Visibility = o.Visibility,
                Owners = o.Owners
            };
        }

        public OfficeObject MapOfficeObject(Group g)
        {
            return new OfficeObject
            {
                Id = g.Id,
                Country = g.Country,
                Name = g.Name,
                Owners = g.Owners,
                Visibility = g.Visibility
            };
        }

        #endregion

        #region Helpers

        private async Task GetGroupsByCountryPage(IEnumerable<string> countries, List<Microsoft.Graph.Group> groupsResult)
        {
            for (var i = 0; i < Math.Ceiling(((double)countries.Count()) / maxFilterCount); i++)
            {
                await GetGroupsByCountryPageNumber(countries, groupsResult, i);
            }
        }

        private async Task GetGroupsByCountryPageNumber(IEnumerable<string> countries, List<Microsoft.Graph.Group> groupsResult, int page)
        {
            var filterGroups = String.Join(" or ", countries.Skip(page * maxFilterCount).Take(maxFilterCount).Select(x => $"{settings.GroupCountryExtendedPropertyName} eq '{x.ToLower()}'"));
            var groupsPaged = await Client.Groups.Request().Filter($"groupTypes/any(x:x eq 'unified') and ({filterGroups})").GetAsync();

            // var groupsPaged = await Client.Groups.Request().GetAsync();
            var groups = groupsPaged.CurrentPage.ToList();
            while (groupsPaged.NextPageRequest != null)
            {
                groupsPaged = await groupsPaged.NextPageRequest.GetAsync();
                groups.AddRange(groupsPaged.CurrentPage.ToList());
            }
            groupsResult.AddRange(groups.Where(x => !groupsResult.Any(g => g.Id == x.Id)));
        }

        public async Task<List<Graph.Group>> GetUnregisteredGroups()
        {
            //var allGroups = await Client.Groups.Request().Select($"id,{settings.GroupCountryExtendedPropertyName},displayName,groupTypes,createdDateTime").GetAsync();

            //var groups = allGroups.CurrentPage.ToList();
            //return groups;

            var allGroups = await Client.Groups.Request().GetAsync();
            foreach (var group in allGroups)
            {
                if (group.DisplayName == "FR_IAE_PLANNER")
                //|| group.DisplayName == "FR_IES_PLANNERTESTRenamed"
                //|| group.DisplayName == "FR_AES_TEAMTEST")

                {
                    var test = group.DisplayName;
                    var associatedTeam = false;
                    var associatedPlanner = false;
                    var associatedSite = false;


                    var adminId = settings.AdminId;
                    var members = await Client.Groups[group.Id].Members
                                                                .Request()
                                                                .GetAsync();
                    var isServiceAccountMember = members.CurrentPage.Where(o => o.Id == adminId).Count() > 0;
                    if (!isServiceAccountMember)
                    {
                        var tempOwner = await Client.DirectoryObjects[adminId].Request().GetAsync();
                        await Client.Groups[group.Id].Members.References.Request().AddAsync(tempOwner);

                        var IsServiceAccountNowMember = false;
                        while (!IsServiceAccountNowMember)
                        {
                            await Task.Delay(1000);
                            var tempMembers = await Client.Groups[group.Id].Members
                                                                .Request()
                                                                .GetAsync();
                            IsServiceAccountNowMember = tempMembers.CurrentPage.Where(o => o.Id == adminId).Count() > 0;

                        }
                    }                    

                    using (var httpClient = new HttpClient())
                    {

                        var token = await helper.AcquireTokenAsyncForAdmin(globalSettings.GraphResourceUrl);
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);


                        //var workspaceResponse = await httpClient.GetAsync($"https://graph.microsoft.com/v1.0/groups/{group.Id}/sites/root");
                        //var workspaceContent = await workspaceResponse.Content.ReadAsStringAsync();
                        //if (workspaceResponse.StatusCode == HttpStatusCode.OK)
                        //    associatedSite = true;


                        var teamResponse = await httpClient.GetAsync($"https://graph.microsoft.com/v1.0/groups/{group.Id}/team");
                        var teamContent = await teamResponse.Content.ReadAsStringAsync();
                        if (teamResponse.StatusCode == HttpStatusCode.OK)
                            associatedTeam = true;

                        var PlannerResponse = await httpClient.GetAsync($"https://graph.microsoft.com/v1.0/groups/{group.Id}/planner/plans");
                        var plannerContent = await PlannerResponse.Content.ReadAsStringAsync();
                        if (PlannerResponse.StatusCode == HttpStatusCode.OK || PlannerResponse.StatusCode == HttpStatusCode.Forbidden)
                            associatedPlanner = true;
                    }

                    if (!isServiceAccountMember)
                    {
                        await Client.Groups[group.Id].Members[adminId].Reference.Request().DeleteAsync();
                    }


                }
            }

            return allGroups.ToList();
        }

        private async Task<IEnumerable<Member>> FetchOwners(Graph.Group g)
        {
            IEnumerable<Member> owners = null;
            var ownerPage = await Client.Groups[g.Id].Owners.Request().GetAsync();
            if (ownerPage != null)
            {
                List<Graph.DirectoryObject> ownerList = ownerPage.ToList();
                while (ownerPage.NextPageRequest != null)
                {
                    ownerPage = await ownerPage.NextPageRequest.GetAsync();
                    ownerList.AddRange(ownerPage);
                }
                owners = ownerList.OfType<Graph.Group>().Select(MapGroup);
                owners = owners.Concat(ownerList.OfType<Graph.User>().Select(MapUser));
            }
            return owners;
        }

        private async Task AddGroupOwners(Graph.Group group, IEnumerable<Member> owners, IEnumerable<string> countries)
        {
            await CheckMembersCountries(countries, owners);

            var groupRef = Client.Groups[group.Id];
            var addOwners = owners.Select(owner => groupRef.Owners.References.Request().AddAsync(DirObj(owner)));

            await Task.WhenAll(addOwners);
        }

        private async Task CheckMembersCountries(IEnumerable<string> countries, IEnumerable<Member> members)
        {
            if (members != null)
            {
                await Task.WhenAll(members.OfType<Group>().Select(x => CheckGroupCountry(x.Id, countries)));

                await Task.WhenAll(members.OfType<User>().Select(x => userService.CheckUser(x.Id, countries)));
            }
        }

        private async Task CheckGroupCountry(string id, IEnumerable<string> countries)
        {
            var g = await Client.Groups[id].Request().Select(settings.GroupCountryExtendedPropertyName).GetAsync();
            CheckCountry(countries, g.AdditionalData[settings.GroupCountryExtendedPropertyName].ToString());
        }

        private static Graph.DirectoryObject DirObj(Member owner)
        {
            return new Graph.DirectoryObject() { Id = owner.Id };
        }

        private static void CheckCountry(IEnumerable<string> countries, string country)
        {
            if (!countries.Any(x => String.Compare(country, x, true) == 0))
            {
                throw new InvalidOperationException("Invalid group country");
            }
        }

        private async Task CheckCountryCountry(string groupId, IEnumerable<string> countries)
        {
            var projection = $"Id,{settings.GroupCountryExtendedPropertyName}";
            var group = await Client.Groups[groupId].Request().Select(projection).GetAsync();
            var countryGroup = group.AdditionalData[settings.GroupCountryExtendedPropertyName].ToString().ToLower();
            CheckCountry(countries, countryGroup);
        }

        private async Task CheckMemberCountry(IEnumerable<string> countries, Graph.DirectoryObject member)
        {
            if (member is Graph.Group)
            {
                var country = member.AdditionalData[settings.GroupCountryExtendedPropertyName].ToString().ToLower();
                CheckCountry(countries, country);
            }
            else
            {
                await userService.CheckUser(member.Id, countries);
            }
        }

        private Group MapGroup(Graph.Group g)
        {
            var g_ = g;
            return new Group
            {
                Id = g.Id,
                Name = g.DisplayName,
                //TODO Uncomment After avoir vu c'est quoi ce GroupCountryExtendedPropertyName
                Country = g.AdditionalData[settings.GroupCountryExtendedPropertyName].ToString().ToLower(),
                Description = g.Description,
                IsSubscribedByMail = g.IsSubscribedByMail,
                Mail = g.Mail,
                SecurityEnabled = g.SecurityEnabled,
                Visibility = g.Visibility
            };
        }

        protected async Task<Group> MapGroupWithOwners(Graph.Group g)
        {
            IEnumerable<Member> owners = await FetchOwners(g);
            Group result = MapGroup(g);
            result.Owners = owners;
            return result;
        }

        protected Graph.Group UnmapGroup(Group g)
        {
            var group = new Graph.Group
            {
                Id = g.Id,
                DisplayName = g.Name,
                Description = g.Description,
                IsSubscribedByMail = null,
                MailNickname = g.MailNickname ?? g.Mail.Substring(0, g.Mail.IndexOf('@')),
                MailEnabled = true,
                GroupTypes = GroupTypes,
                SecurityEnabled = false,
                Visibility = g.Visibility,
                AdditionalData = new Dictionary<string, object>()
                {
                    {settings.GroupCountryExtendedPropertyName,g.Country},
                }
            };
            return group;
        }

        #endregion
    }
}
