﻿using Microsoft.Azure.ActiveDirectory.GraphClient;
using Microsoft.Graph;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Sonepar.O365AdminToolbox.Domain;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Sonepar.O365AdminToolbox.GraphPort
{
    public class AadAuthenticationService : IAuthenticationService
    {
        private IAppSettings appSettings;
        private IGlobalSettings globalSettings;
        private IAdminCredentials adminCred;

        public IDictionary<string, AuthenticationResult> TokensForUser = new Dictionary<string, AuthenticationResult>(StringComparer.InvariantCultureIgnoreCase);
        public string TokenForUser;

        public AadAuthenticationService(
            IAppSettings appSettings,
            IGlobalSettings globalSettings,
            IAdminCredentials adminCred)
        {
            this.appSettings = appSettings;
            this.globalSettings = globalSettings;
            this.adminCred = adminCred;
        }

        /// <summary>
        /// Get Active Directory Client for User.
        /// </summary>
        /// <returns>ActiveDirectoryClient for User.</returns>
        public ActiveDirectoryClient GetActiveDirectoryClientAsAdmin()
        {
            Uri servicePointUri = new Uri(globalSettings.AadResourceUrl);
            Uri serviceRoot = new Uri(servicePointUri, appSettings.TenantId);
            ActiveDirectoryClient activeDirectoryClient = new ActiveDirectoryClient(serviceRoot,
                async () => await AcquireTokenAsyncForAdmin(globalSettings.AadResourceUrl));
            return activeDirectoryClient;   
        }

        /// <summary>
        /// Gets the graph client as admin.
        /// </summary>
        /// <returns></returns>
        public GraphServiceClient GetGraphClientAsAdmin()
        {
            GraphServiceClient graphclient = new GraphServiceClient(
                new DelegateAuthenticationProvider(
                    async (r) => { r.Headers.Authorization = new AuthenticationHeaderValue("Bearer", await AcquireTokenAsyncForAdmin(globalSettings.GraphResourceUrl)); }
                )
            );
            return graphclient;
        }

        /// <summary>
        /// Gets the graph client as User.
        /// </summary>
        /// <param name="user">The user credentials.</param>
        /// <returns></returns>
        public GraphServiceClient GetGraphClientAsUser(UserPassCredentials user)
        {
            GraphServiceClient graphclient = new GraphServiceClient(
                new DelegateAuthenticationProvider(
                    async (r) => { r.Headers.Authorization = new AuthenticationHeaderValue("Bearer", await AcquireTokenAsyncForUser(globalSettings.GraphResourceUrl, user)); }
                )
            );
            return graphclient;
        }
        /// <summary>
        /// Async task to acquire token for User.
        /// </summary>
        /// <param name="resource">The resource to authorize.</param>
        /// <returns>Token for user.</returns>
        public async Task<string> AcquireTokenAsyncForAdmin(string resource)
        {
            AuthenticationResult token = null;
            if (!TokensForUser.ContainsKey(resource) || TokensForUser[resource].ExpiresOn < DateTimeOffset.Now.AddSeconds(10))
            {
                var redirectUri = appSettings.AppRedirectUrl;
                AuthenticationContext authenticationContext = new AuthenticationContext(appSettings.AuthString, false);

                var admin = adminCred.GetAdminCredentials();
                UserCredential uc = new UserPasswordCredential(admin.UserLogin, admin.Password);
                AuthenticationResult userAuthnResult = await authenticationContext.AcquireTokenAsync(resource, appSettings.ClientIdAdmin, uc);

                TokensForUser[resource] = userAuthnResult;
            }
            TokensForUser.TryGetValue(resource, out token);
            if (token == null || String.IsNullOrWhiteSpace(token.AccessToken))
            {
                throw new Exception("Could not authenticate user");
            }
            return token.AccessToken;
        }
        /// <summary>
        /// Async task to acquire token for User.
        /// </summary>
        /// <param name="resource">The resource to authorize.</param>
        /// <param name="userCrendentials">The user crendentials.</param>
        /// <returns>
        /// Token for user.
        /// </returns>
        public async Task<string> AcquireTokenAsyncForUser(string resource, UserPassCredentials userCrendentials)
        {
            AuthenticationResult token = null;
            var redirectUri = appSettings.AppRedirectUrl;
            AuthenticationContext authenticationContext = new AuthenticationContext(appSettings.AuthString, false);

            UserCredential uc = new UserPasswordCredential(userCrendentials.UserLogin, userCrendentials.Password);
            AuthenticationResult userAuthnResult = await authenticationContext.AcquireTokenAsync(resource, appSettings.ClientIdAdmin, uc);

            if (token == null || String.IsNullOrWhiteSpace(token.AccessToken))
            {
                throw new Exception("Could not authenticate user");
            }

            return token.AccessToken;
        }

    }
}
