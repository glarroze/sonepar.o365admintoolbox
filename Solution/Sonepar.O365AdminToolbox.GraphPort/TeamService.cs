﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sonepar.O365AdminToolbox.Domain;
using Sonepar.O365AdminToolbox.Entity;
using System.Data.Entity;
using System.Net.Http;
using System.Text;
using System.Net;
using System.Net.Http.Headers;
using System;
using System.Text.RegularExpressions;
using Graph = Microsoft.Graph;

namespace Sonepar.O365AdminToolbox.GraphPort
{
    public class TeamService : BaseUserService, ITeamService
    {
        private IGlobalSettings globalSettings;
        private IGroupService groupService;
        private ToolboxDbContext toolboxContext;

        public TeamService(
            IAuthenticationService helper,
            IAppSettings settings,
            IFriendlyNameService nameService,
            IGlobalSettings globalSettings,
            IGroupService groupService,
            ToolboxDbContext toolboxContext) : base(helper, settings, nameService)
        {
            this.globalSettings = globalSettings;
            this.groupService = groupService;
            this.toolboxContext = toolboxContext;
        }

        #region ITeamService

        public async Task<IEnumerable<OfficeObject>> GetTeamsManagedByUser(IEnumerable<string> countries)
        {
            var teams = await toolboxContext.OfficeObjects.Where(t => t.Type == OfficeObjectType.Team && countries.Contains(t.Country)).OrderBy(t => t.Country).ThenBy(t => t.Name).ToListAsync();

            return teams;
        }

        public async Task<OfficeObject> GetTeam(string id, IEnumerable<string> countries)
        {
            var group = await groupService.GetGroup(id, countries);
            var team = groupService.MapOfficeObject(group);

            return team;
        }        

        public async Task<OfficeObject> NewTeam(OfficeObject team, IEnumerable<string> countries)
        {
            using (var transactionScope = toolboxContext.Database.BeginTransaction())
            {

                var groupExists = await groupService.GroupExists(team.Name);
                if (groupExists)
                {
                    throw new InvalidOperationException("Group allready exists");
                }
                // Add team (group) to Azure AD
                var group = groupService.MapGroup(team);
                group.MailNickname = Regex.Replace(group.MailNickname, "[^a-zA-Z0-9.\\-_]", String.Empty);
                group = await groupService.NewGroup(group, countries);

                // Owners member assocation (prerequisite)
                foreach (var owner in team.Owners)
                    await groupService.AddGroupMember(group.Id, owner.Id, countries);

                // Admin member association (prerequisite)
                var adminId = settings.AdminId;
                var member = await Client.DirectoryObjects[adminId].Request().GetAsync();
                await Client.Groups[group.Id].Owners.References.Request().AddAsync(member);

                // Team creation retry process
                using (var httpClient = new HttpClient())
                {
                    var token = await helper.AcquireTokenAsyncForAdmin(globalSettings.GraphResourceUrl);
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                    for (var retry = 0; retry < settings.RetryCount; retry++)
                    {
                        // Delay operation because Azure owner could not be fully associated at this moment
                        await Task.Delay(1000);

                        // Create team
                        var content = new StringContent("{}", Encoding.UTF8, "application/json");
                        var response = await httpClient.PutAsync($"https://graph.microsoft.com/beta/groups/{group.Id}/team", content);

                        // Not associated member
                        if (response.StatusCode == HttpStatusCode.Created)
                            break;
                    }
                }

                // Admin member dissociation
                await Client.Groups[group.Id].Owners[adminId].Reference.Request().DeleteAsync();

                // Add team to database
                team.Id = group.Id;
                team.Type = OfficeObjectType.Team;

                toolboxContext.OfficeObjects.Add(team);
                await toolboxContext.SaveChangesAsync();

                transactionScope.Commit();

                return team;
            }
        }

        public async Task<OfficeObject> UpdateTeam(OfficeObject team, IEnumerable<string> countries)
        {
            using (var transactionScope = toolboxContext.Database.BeginTransaction())
            {
                var groupExists = await groupService.GroupExists(team.Name);
                if (groupExists)
                {
                    throw new InvalidOperationException("Group allready exists");
                }
                var userUpdate = new Graph.Group
                {
                    DisplayName = team.Name,
                    MailNickname = Regex.Replace(team.Name, "[^a-zA-Z0-9.\\-_]", String.Empty)
                };
                await Client.Groups[team.Id]
                .Request()
                .UpdateAsync(userUpdate);

                var teamDB = toolboxContext.OfficeObjects.Single(t => t.Id == team.Id);
                teamDB.Name = team.Name;
                await toolboxContext.SaveChangesAsync();


                var group = await groupService.GetGroup(team.Id, countries);
                var teamUpdated = groupService.MapOfficeObject(group);

                transactionScope.Commit();

                return teamUpdated;
            }
        }

        public async Task DeleteTeam(string id, IEnumerable<string> countries)
        {
            using (var transactionScope = toolboxContext.Database.BeginTransaction())
            {
                // Remove team from database
                var team = toolboxContext.OfficeObjects.Single(t => t.Id == id);
                toolboxContext.OfficeObjects.Remove(team);
                await toolboxContext.SaveChangesAsync();

                // Remove team (group) from Azure AD
                await groupService.DeleteGroup(id, countries);

                transactionScope.Commit();
            }
        }

        public async Task<IEnumerable<Member>> GetTeamOwners(string id, IEnumerable<string> countries)
        {
            return await groupService.GetGroupOwners(id, countries);
        }

        public async Task AddTeamOwner(string id, string userId, IEnumerable<string> countries)
        {
            await groupService.AddGroupOwner(id, userId, countries);

            var existingGroupMembers = await groupService.GetGroupMembers(id, countries);
            if (!existingGroupMembers.Any(t => t.Id == userId))
                await groupService.AddGroupMember(id, userId, countries);
        }

        public async Task RemoveTeamOwner(string id, string userId, IEnumerable<string> countries)
        {
            await groupService.RemoveGroupOwner(id, userId, countries);
        }

        public async Task<IEnumerable<Member>> GetTeamMembers(string id, IEnumerable<string> countries)
        {
            return await groupService.GetGroupMembers(id, countries);
        }

        public async Task AddTeamMember(string id, string userId, IEnumerable<string> countries)
        {
            using (var httpClient = new HttpClient())
            {
                var token = await helper.AcquireTokenAsyncForAdmin(globalSettings.GraphResourceUrl);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var userODataUrl = $"https://graph.microsoft.com/beta/directoryObjects/{userId}";
                var content = new StringContent($"{{\"@odata.id\":\"{userODataUrl}\"}}", Encoding.UTF8, "application/json");

                var response = await httpClient.PostAsync($"https://graph.microsoft.com/beta/groups/{id}/members/$ref", content);
                if (response.StatusCode != HttpStatusCode.NoContent)
                    throw new Exception($"Expected 204 http status, got {(int)response.StatusCode}");
            }
        }

        public async Task RemoveTeamMember(string id, string userId, IEnumerable<string> countries)
        {
            using (var httpClient = new HttpClient())
            {
                var token = await helper.AcquireTokenAsyncForAdmin(globalSettings.GraphResourceUrl);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = await httpClient.DeleteAsync($"https://graph.microsoft.com/beta/groups/{id}/members/{userId}/$ref");
                if (response.StatusCode != HttpStatusCode.NoContent)
                    throw new Exception($"Expected 204 http status, got {(int)response.StatusCode}");
            }
        }

        #endregion
    }
}
