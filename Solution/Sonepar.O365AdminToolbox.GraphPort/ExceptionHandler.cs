﻿using Sonepar.O365AdminToolbox.Domain;
using System;

namespace Sonepar.O365AdminToolbox.GraphPort
{
    public class ExceptionHandler : IExceptionHandler
    {
        public ExceptionHandler() { }
        public string MakeMessage(Exception e)
        {
            var se = e as Microsoft.Graph.ServiceException;
            if (se != null )
            {
                return "Azure Active directory error: "+se.Error.Message;
            }
            else {
                return e.Message;
            }
        }
    }
}
