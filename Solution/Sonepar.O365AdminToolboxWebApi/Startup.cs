﻿using Owin;
using System;
using System.IO;
using System.Reflection;
using System.Web;

namespace Sonepar.O365AdminToolboxWebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //AppDomain.CurrentDomain.AssemblyResolve += (sender, resolveArgs) =>
            //{
            //    if (resolveArgs.Name.Contains(".resources"))
            //        return null;

            //    string assemblyInfo = resolveArgs.Name;// e.g "Lib1, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"
            //    var parts = assemblyInfo.Split(',');
            //    string name = parts[0];
            //    var version = Version.Parse(parts[1].Split('=')[1]);
            //    string fullName;
            //    string LocalPath = HttpRuntime.AppDomainAppPath;
            //    if (name == "System.IdentityModel.Tokens.Jwt" && version.Major == 4 && version.Minor == 0)
            //    {
            //        fullName = new FileInfo($@"{LocalPath}\JWTDLLOLD\System.IdentityModel.Tokens.Jwt.dll").FullName;
            //    }
            //    else if (name == "System.IdentityModel.Tokens.Jwt" && version.Major == 5 && version.Minor == 2)

            //    {
            //        fullName = new FileInfo($@"{LocalPath}\JWTDLL\System.IdentityModel.Tokens.Jwt.dll").FullName;
            //    }
            //    else
            //    {
            //        return null;
            //    }

            //    return Assembly.LoadFile(fullName);
            //};
            ConfigureAuth(app);
        }

        private string FindFileInPath(string filename, string path)
        {
            filename = filename.ToLower();

            foreach (var fullFile in Directory.GetFiles(path))
            {
                var file = Path.GetFileName(fullFile).ToLower();
                if (file == filename)
                    return fullFile;

            }
            foreach (var dir in Directory.GetDirectories(path))
            {
                var file = FindFileInPath(filename, dir);
                if (!string.IsNullOrEmpty(file))
                    return file;
            }

            return null;
        }
    }
}