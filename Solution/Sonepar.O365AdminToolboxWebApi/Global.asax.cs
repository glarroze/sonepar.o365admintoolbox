﻿using Microsoft.Practices.Unity;
using Sonepar.O365AdminToolbox.Domain;
using Sonepar.O365AdminToolbox.Entity;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Sonepar.O365AdminToolboxWebApi
{
    /// <summary>
    /// The web app
    /// </summary>
    /// <seealso cref="System.Web.HttpApplication" />
    public class WebApiApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Application start handler.
        /// </summary>
        protected void Application_Start()
        {
            var container = UnityConfig.RegisterComponents();
            GlobalConfiguration.Configure((HttpConfiguration config) => {
                WebApiConfig.Register(config, t => container.Resolve(t));
            });
            RouteTable.Routes.Add(new Route("config", new ConfigJSRouteHandler(container.Resolve<IAppSettings>())));
            AreaRegistration.RegisterAllAreas();

            Database.SetInitializer<ToolboxDbContext>(null);
        }
    }
}
