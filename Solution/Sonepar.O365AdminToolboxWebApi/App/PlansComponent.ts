﻿import { app } from './app';
import * as angular from 'angular';
import { IAuthenticationService } from './Services/AuthenticationService';
import { IDataService } from './Services/DataService';
import { INotifyOptions } from 'interfaces/INotifyOptions';
import { IFeedbackService } from './Services/FeedbackService';
import { IConfig } from 'interfaces/Config';

import Component from './ComponentDecorator';
import User from './Model/User';
import OfficeObject from './Model/OfficeObject';

@Component(app, 'plans', {
    controllerAs: 'ctrl',
    templateUrl: 'app/views/plans.html'
}) export class PlansComponent {
    public get userCountries(): string[] { return this.auth.userCountries; }
    public plans: OfficeObject[];
    public users: User[];
    public planOwners: User[];
    public dataPlan: OfficeObject;
    public ownerPlaceHolder: string = 'Loading users...';
    public message: string;
    public saving: boolean = false;
    public loading: boolean = false;
    public isCreateMode: boolean = false;
    public isOwnersMode: boolean = false;
    public isMembersMode: boolean = false;
    public isEditPlanMode: boolean = false;
    public selectedPlan: OfficeObject | undefined;
    public ownerPlanId: string;
    public planNamePlaceholder: string;

    public addOwners(users: User[]) {
        users.forEach(u => {
            if (!this.dataPlan.Owners.some(o => o.Id == u.Id)) {
                this.dataPlan.Owners.push(u);
            }
        })
    }
    public removeOwners(users: User[]) {
        this.dataPlan.Owners = this.dataPlan.Owners.filter(o => !users.some(u => u.Id === o.Id));
    }

    public showCreateForm() {
        this.scroll("edit")
        this.isCreateMode = true;
        this.isMembersMode = false;
        this.isOwnersMode = false;
        this.isEditPlanMode = false;
        this.selectedPlan = undefined;
    }

    public showMembersForm(plan: OfficeObject) {
        this.scroll("edit")
        this.isMembersMode = true;
        this.isCreateMode = false;
        this.isOwnersMode = false;
        this.isEditPlanMode = false;
        this.selectedPlan = plan;
    }
    public showOwnersForm(plan: OfficeObject) {
        this.scroll("edit")
        this.isMembersMode = false;
        this.isCreateMode = false;
        this.isOwnersMode = true;
        this.isEditPlanMode = false;
        this.selectedPlan = plan;
    }

    public showEditForm(plan: OfficeObject) {
        console.log(plan);        
        this.scroll("edit")
        this.isEditPlanMode = true;
        this.isCreateMode = false;
        this.isMembersMode = false;
        this.isOwnersMode = false;
        this.selectedPlan = { ...plan };
    }

    public hideCreateForm() {
        this.dataPlan = new OfficeObject();
        this.isCreateMode = false;
    }

    

    public hideEditForm() {
        this.dataPlan = new OfficeObject();
        this.isEditPlanMode = false;
    }

    public sendPlan() {
        this.saving = true;
        var name = this.dataPlan.Name;
        this.dataService.PutPlan(this.dataPlan)
            .then(x => {
                this.hideCreateForm();
                this.getPlan();
                var c = angular.copy(this.notifyOkConfig);
                c.message = "Plan saved";
                this.notify(c);
                this.logService.addMessage(`Plan ${name} created`, "Trace");
                this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(e => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.$exceptionHandler(e);
            }).finally(() => { this.saving = false })

    }
    

    public savePlan() {
        this.saving = true;
        let groupId = this.selectedPlan ? this.selectedPlan.Id : "";
        let groupName = this.selectedPlan ? this.selectedPlan.Name : "";
        let datasToUpdate = {
            Id: groupId,
            Name: groupName,
        }
        console.log("planName", groupName);

        this.dataService.UpdatePlan(datasToUpdate)
            .then(x => {
                this.hideEditForm();
                this.getPlan();
                var c = angular.copy(this.notifyOkConfig);
                c.message = "Plan saved";
                this.notify(c);
                this.logService.addMessage(`Plan ${name} updated`, "Trace");
                this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(e => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.$exceptionHandler(e);
            }).finally(() => { this.saving = false })
    }

    public getPlan() {
        this.loading = true;
        this.dataService.GetPlans().then(x => {
            this.plans = x;
            this.loading = false;
        });
    }

    private isMatchFilter(filter: string) {

        return (value: string) => value != null && value.toLowerCase().indexOf(filter.toLowerCase()) > -1;
    }
    public matchUser(filter: string) {
        return (u: User) => {
            var isMatch = this.isMatchFilter(filter);
            return (filter == null
                || filter.length === 0
                || isMatch(u.DisplayName)
                || isMatch(u.City)
                || isMatch(u.Department)
                || isMatch(u.Country)
                || isMatch(u.JobTitle)
                || isMatch(u.Mail)
                || isMatch(u.UserPrincipalName));
        };
    }

    public tryDelete(plan: OfficeObject) {
        var modal = this.modal.open({
            size: 'lg',
            controller: DeletePlanController,
            controllerAs: "ctrl",
            templateUrl: 'app/views/deletePlan.html',
            resolve: {
                "plan": plan
            }
        });
        modal.closed.catch(() => { });
        modal.result.then(() => this.dataService.DeletePlan(plan)
        ).then(
            () => {
                this.getPlan();
                var notification = angular.copy(this.notifyOkConfig);
                notification.message = `Plan "${plan.Name}" has been successfully deleted`
                this.notify(notification);
                this.logService.addMessage(`Plan "${plan.Name}" has been successfully deleted`, "Info");
                this.logService.addMessage("Operation finished successfully", "Info");
            }
            ).catch(e => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.$exceptionHandler(e)
            });
    }

    constructor(
        private $exceptionHandler: ng.IExceptionHandlerService,
        private scroll: ng.IAnchorScrollService,
        private modal: ng.ui.bootstrap.IModalService,
        private auth: IAuthenticationService,
        private dataService: IDataService,
        private logService: IFeedbackService,
        private notify: ng.cgNotify.INotifyService,
        private notifyOkConfig: INotifyOptions,
        private config: IConfig
    ) {
        this.scroll.yOffset = 150;
        auth.acquireToken().then(() => {
            this.getPlan();
            return dataService.GetUsers().then(x => {
                this.loading = false;
                this.ownerPlaceHolder = "Search for users"
                this.users = x;
            });
        }).catch(e => { });
        this.dataPlan = new OfficeObject();
        this.planNamePlaceholder = config.planNamePlaceholder;
    }

    public static $inject = ["$exceptionHandler", "$anchorScroll", "$uibModal", "authenticationService", "dataService", "feedbackService", "notify", "notifyOkConfig", "config"];
}

class DeletePlanController {
    constructor(
        private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance, private plan: OfficeObject
    ) {

    }
    public cancel() {
        this.$uibModalInstance.dismiss();
    }
    public ok() {
        this.$uibModalInstance.close();
    }

    public static $inject = ["$uibModalInstance", "plan"];

}