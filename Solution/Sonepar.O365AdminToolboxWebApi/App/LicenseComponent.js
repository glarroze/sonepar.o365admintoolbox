var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
define(["require", "exports", "./app", "angular", "./ComponentDecorator", "./Model/License"], function (require, exports, app_1, angular, ComponentDecorator_1, License_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var LicenseComponent = /** @class */ (function () {
        function LicenseComponent($q, $exceptionHandler, auth, dataService, logservice, notify, notifyOkConfig) {
            var _this = this;
            this.$q = $q;
            this.$exceptionHandler = $exceptionHandler;
            this.auth = auth;
            this.dataService = dataService;
            this.logservice = logservice;
            this.notify = notify;
            this.notifyOkConfig = notifyOkConfig;
            this.IsMFAEnable = false;
            this.IsLoadingMFAStatut = true;
            this.LicensesRef = [];
            this.loading = false;
            this.saving = false;
            auth.acquireToken().then(function () {
                // this.loading = true;
            });
            this.loadedProm = this.$q.all([
                dataService.GetLicenses().then(function (x) {
                    console.log(x);
                    _this.LicensesRef = x;
                }),
                dataService.GetCountries().then(function (x) {
                    _this.countries = x;
                })
            ]).then(function () {
                _this.loading = false;
            });
            this.$onChanges = this.onChanges.bind(this);
            this.$changedUserStatut = this.changedUserStatut.bind(this);
        }
        Object.defineProperty(LicenseComponent.prototype, "usersCol", {
            get: function () {
                var col = [];
                if (!angular.isArray(this.users)) {
                    col = [this.users];
                }
                else {
                    col = this.users;
                }
                return col;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LicenseComponent.prototype, "user", {
            // public get user(): User | undefined {
            get: function () {
                return this.users && (angular.isArray(this.users) && this.users && this.users.length === 1 && this.users[0]
                    || this.users) || undefined;
            },
            enumerable: true,
            configurable: true
        });
        LicenseComponent.prototype.feedback = function (a, u, licName) {
            this.logservice.addMessage("User " + u.DisplayName + ", license " + licName + " " + a, "Trace");
            this.counters[a] = (this.counters[a] || 0) + 1;
        };
        LicenseComponent.prototype.save = function (userChanged) {
            var _this = this;
            this.saving = true;
            this.counters = {};
            this.Licenses.forEach(function (l) { return l.License.UsageLocation = _this.UsageLocation; });
            return this.dataService.SaveLicenses(this.usersCol, this.Licenses, this.feedback.bind(this)).then(function () {
                _this.dataService.GetLicenses().then(function (x) {
                    _this.LicensesRef = x;
                });
            }).then(function () { return __awaiter(_this, void 0, void 0, function () {
                var c;
                return __generator(this, function (_a) {
                    c = angular.copy(this.notifyOkConfig);
                    c.message = "User licenses saved :";
                    c.message += this.makeMessage();
                    this.notify(c);
                    this.logservice.addMessage("Operation finished successfully", "Info");
                    if (this.onSaved) {
                        this.onSaved();
                    }
                    ;
                    return [2 /*return*/];
                });
            }); }).catch(function (e) {
                var errorMessage = "List is not up to date any longer, please hit refresh";
                _this.logservice.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.logservice.addMessage("Operation cancelled", "Error");
                _this.$exceptionHandler(errorMessage);
                _this.$exceptionHandler(e, _this.makeMessage());
            }).finally(function () {
                _this.saving = false;
            });
        };
        LicenseComponent.prototype.makeMessage = function () {
            var m = "";
            for (var a in this.counters) {
                m = (m && (m + ",")) + (this.counters[a] + " " + a);
            }
            return m;
        };
        LicenseComponent.prototype.changedUserStatut = function () {
            var _this = this;
            if (this.userForm) {
                this.userForm.$setPristine();
            }
            var accountStateToSet = this.user.AccountEnabled ? true : false;
            console.log("accountStatut to Set", accountStateToSet);
            return this.auth.acquireToken().then(function () {
                // this.loading = true;
            }).then(function () {
                return _this.dataService.UpdateUserStatut(_this.user, accountStateToSet).then(function (x) {
                    _this.user.AccountEnabled = accountStateToSet;
                    var newStatut = accountStateToSet ? "enabled" : "disabled";
                    var c = angular.copy(_this.notifyOkConfig);
                    c.message = "Account " + _this.user.UserPrincipalName + " " + newStatut;
                    c.message += _this.makeMessage();
                    _this.notify(c);
                    _this.logservice.addMessage("Account " + _this.user.UserPrincipalName + " " + newStatut + " successfully", "Info");
                });
            }).catch(function (e) { return _this.$exceptionHandler(e); })
                .finally(function () { _this.loading = false; });
        };
        LicenseComponent.prototype.changedUserMFAStatut = function () {
            var _this = this;
            if (this.userForm) {
                this.userForm.$setPristine();
            }
            this.IsLoadingMFAStatut = true;
            if (this.IsMFAEnable) {
                this.dataService.UpdateUserMFAStatut(this.user, "enabled").then(function (response) {
                    _this.IsLoadingMFAStatut = false;
                    var newStatut = "enabled";
                    var c = angular.copy(_this.notifyOkConfig);
                    c.message = "Account " + _this.user.UserPrincipalName + " MFA " + newStatut;
                    c.message += _this.makeMessage();
                    _this.notify(c);
                    _this.logservice.addMessage("Account " + _this.user.UserPrincipalName + " MFA " + newStatut + " successfully", "Info");
                });
            }
            else {
                this.dataService.UpdateUserMFAStatut(this.user, "disabled").then(function (response) {
                    _this.IsLoadingMFAStatut = false;
                    var newStatut = "disable";
                    var c = angular.copy(_this.notifyOkConfig);
                    c.message = "Account " + _this.user.UserPrincipalName + " MFA " + newStatut;
                    c.message += _this.makeMessage();
                    _this.notify(c);
                    _this.logservice.addMessage("Account " + _this.user.UserPrincipalName + " MFA " + newStatut + " successfully", "Info");
                });
            }
        };
        LicenseComponent.prototype.onChanges = function (x) {
            var _this = this;
            if (this.LicensesRef) {
                console.log(this.LicensesRef);
                this.onChangesSync(x);
            }
            else {
                this.loadedProm.then(function () {
                    console.log(x);
                    _this.onChangesSync(x);
                });
            }
        };
        LicenseComponent.prototype.onChangesSync = function (x) {
            return __awaiter(this, void 0, void 0, function () {
                var val, col, selectedUser_1;
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            console.log(x);
                            val = x['users'].currentValue;
                            col = [];
                            if (!val) {
                                this.Licenses = [];
                            }
                            else if (!angular.isArray(val)) {
                                col = [val];
                            }
                            else {
                                col = val;
                            }
                            console.log(col);
                            console.log(col.length);
                            if (!(col.length == 1)) return [3 /*break*/, 2];
                            this.loading = true;
                            return [4 /*yield*/, this.dataService.GetUser(col[0].Id)];
                        case 1:
                            selectedUser_1 = _a.sent();
                            this.loading = false;
                            this.IsLoadingMFAStatut = true;
                            this.dataService.GetUserMFAStatut(selectedUser_1).then(function (response) {
                                _this.IsLoadingMFAStatut = false;
                                if (response.userStatut == "Enabled") {
                                    _this.IsMFAEnable = true;
                                }
                                else {
                                    _this.IsMFAEnable = false;
                                }
                            });
                            console.log(selectedUser_1);
                            this.users = [selectedUser_1];
                            this.Licenses = this.LicensesRef.map(function (x) { return new License_1.LicenseAssignment(x, selectedUser_1.AssignedLicenses.some(function (l) { return _this.eq(l.Id, x.Id); })); });
                            this.Licenses.forEach(function (l) {
                                var al = selectedUser_1.AssignedLicenses.filter(function (al) { return _this.eq(al.Id, l.License.Id); })[0];
                                l.License.Services.forEach(function (s) {
                                    s.IsActive = (!al) || al.Services.some(function (p) { return _this.eq(p.Service.Id, s.Service.Id) && (p.IsActive || false); });
                                });
                            });
                            this.UsageLocation = selectedUser_1.UsageLocation;
                            return [3 /*break*/, 3];
                        case 2:
                            if (col.length > 1) {
                                this.loading = false;
                                this.Licenses = this.LicensesRef.map(function (x) { return new License_1.LicenseAssignment(x); });
                                this.Licenses.forEach(function (l) { return l.License.Services.forEach(function (s) { return s.IsActive = undefined; }); });
                                this.UsageLocation = col.reduce(function (p, n) { return p === undefined ? n.UsageLocation : p === n.UsageLocation ? p : null; }, undefined) || undefined;
                            }
                            _a.label = 3;
                        case 3:
                            if (this.userForm) {
                                this.userForm.$setPristine();
                            }
                            return [2 /*return*/];
                    }
                });
            });
        };
        LicenseComponent.prototype.requireCountry = function () {
            return this.usersCol.some(function (u) { return !u.UsageLocation; }) && (this.usersCol.length == 1 || this.Licenses.some(function (x) { return x.IsActive === true; }));
        };
        LicenseComponent.prototype.eq = function (a, b) {
            return a && b && a.toLowerCase() === b.toLowerCase() || false;
        };
        LicenseComponent.$inject = [
            "$q",
            "$exceptionHandler",
            "authenticationService",
            "dataService",
            "feedbackService",
            "notify", "notifyOkConfig"
        ];
        LicenseComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'license', {
                controllerAs: 'ctrl',
                bindings: { "users": "<", 'onSaved': "&", 'onChange': "&" },
                templateUrl: 'app/views/license.html'
            })
        ], LicenseComponent);
        return LicenseComponent;
    }());
    exports.LicenseComponent = LicenseComponent;
});
//# sourceMappingURL=LicenseComponent.js.map