﻿import { module } from 'angular';
import * as angular from 'angular';
import { IConfig, IConfigProvider } from 'interfaces/config';
import 'adal';
import { INotifyOptions } from 'interfaces/INotifyOptions';
import { IAuthenticationService } from 'services/AuthenticationService';

String.prototype.startsWith = String.prototype.startsWith || function (x: string) { return (<string>this).indexOf(x) == 0; }

export let app = module('app', [
    'appConfig',
    'ui.router',
    'ui.toggle',
    'ui.bootstrap',
    'ui.select',
    'ngAnimate',
    'cgNotify',
    'AdalAngular'
]);

app.config([
    "$locationProvider",
    "$httpProvider",
    "$stateProvider",
    "$urlRouterProvider",
    "adalAuthenticationServiceProvider",
    "configProvider",
    "$animateProvider",
    "appConfig",
    function (
        $locationProvider: ng.ILocationProvider,
        $httpProvider: angular.IHttpProvider,
        $stateProvider: angular.ui.IStateProvider,
        $urlRouterProvider: angular.ui.IUrlRouterProvider,
        adalAuthenticationServiceProvider: adal.AdalAuthenticationServiceProvider,
        configProvider: IConfigProvider,
        $animateProvider: ng.animate.IAnimateProvider,
        appConfig: IConfig) {
        var config = appConfig;

        $animateProvider.classNameFilter(/^((?!(no-animate)).*)*$/);

        adalAuthenticationServiceProvider.init(
            <adal.Config>{
                clientId: config.clientId,
                tenant: config.tenant,
                endpoints: config.endpoints
            },
        );

        function needConnectedUser(auth: IAuthenticationService) {
            return auth.acquireToken().then(() => auth.userCountries.length > 0).catch(() => false);
        }
        needConnectedUser.$inject = ['authenticationService'];
        var needUser: any = { _user: needConnectedUser };
        $stateProvider.state({
            name: 'users',
            url: '/menu/users',
            template: '<users></users>',
            resolve: needUser
        }).state({
            name: 'groups',
            url: '/menu/groups',
            template: '<groups></groups>',
            resolve: needUser
        }).state({
            name: 'teams',
            url: '/menu/teams',
            template: '<teams></teams>',
            resolve: needUser
        }).state({
            name: 'plans',
            url: '/menu/plans',
            template: '<plans></plans>',
            resolve: needUser
        }).state({
            name: 'workspaces',
            url: '/menu/workspaces',
            template: '<workspaces></workspaces>',
            resolve: needUser
        }).state({
            name: 'logs',
            url: '/menu/logs',
            template: '<feedback></feedback>',
            resolve: needUser
        }).state({
            name: 'settings',
            url: '/menu/settings',
            template: '<settings></settings>',
            resolve: needUser
        });
        $urlRouterProvider.otherwise('/menu/users');

        if ($httpProvider && $httpProvider.interceptors) {
            $httpProvider.interceptors.push('MyProtectedResourceInterceptor');
            $httpProvider.interceptors.push('AppApiInterceptor');
        }


        $locationProvider.html5Mode({
            requireBase: true,
            enabled: true,
        }).hashPrefix('!');
    }]
);
app.factory('$exceptionHandler', ['$log', '$injector', function ($log: ng.ILogService, $injector: ng.auto.IInjectorService) {
    return function myExceptionHandler(exception?: any, cause?: string) {
        console.log(exception, cause);
        var notify: ng.cgNotify.INotifyService = <any>$injector.get('notify');
        var notifyOptions: INotifyOptions = angular.extend({}, <any>$injector.get('notifyErrorConfig'));
        var e = (exception && exception.data || exception);
        if (e) {
            notifyOptions.message = e.ExceptionMessage || e.Message || e;
        }
        if (cause) {
            notifyOptions.message += (notifyOptions.message ? "," : "") + cause;
        }
        if (notifyOptions.message) {
            if (notifyOptions.message.toLowerCase().indexOf("Transition Superseded") >= 0) {
                return;
            }
            notify(notifyOptions);
        }
        $log.warn(exception, cause);
    };
}]);
app.factory('MyProtectedResourceInterceptor', ['ProtectedResourceInterceptor', 'config', '$q', function (interceptor: ng.IHttpInterceptor, myconfig: IConfig, $q: ng.IQService) {

    return {
        request: function (config: ng.IRequestConfig) {
            if (config.url.startsWith(myconfig.BaseServiceUrl + 'api/authenticat')) {
                return interceptor.request && interceptor.request(config) || config;
            }
            return config;
        },
        responseError: function (rejection: any) {
            return rejection && (interceptor.responseError && interceptor.responseError(rejection) || $q.reject(rejection));
        }
    };
}]).constant('notifyErrorConfig', <INotifyOptions>{
    duration: 5000,
    classes: 'alert-danger',
    position: 'center'
}).constant('notifyOkConfig', <INotifyOptions>{
    duration: 2500,
    classes: 'alert-success',
    position: 'center'
});