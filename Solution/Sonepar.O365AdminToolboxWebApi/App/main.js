﻿require.config({
    baseUrl: "app",
    // alias libraries paths
    paths: {
        'jquery': '../scripts/jquery-bundle.min',
        'bootstrap': '../scripts/bootstrap-bundle.min',
        'moment': '../scripts/moment.min',
        'angular': '../scripts/ng.min',
        'adal': '../scripts/adal',
        'adalAngular': '../scripts/adal-angular',
        'appconfig': '../config'
    },
    name: 'main',
    waitSeconds: 60,
    //generateSourceMaps: true,
    // angular does not support AMD out of the box, put it in a shim
    shim: {
        'angular': {
            exports: 'angular', deps: ['jquery']
        },
        'adalAngular': {
            deps: ['angular']
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'appconfig': {deps : ['angular']}
    }
});

require([
    'jquery',
    'angular',
    'bootstrap',
    'adal',
    'adalAngular',
    'moment',
    'AppComponent',
    'LoginComponent',
    'feedbackComponent',
    'LicenseComponent',
    'UserComponent',
    'UserPickerComponent',
    'GroupsComponent',
    'GroupOwnersComponent',
    'GroupMembersComponent',
    'SettingsComponent',
    'TeamsComponent',
    'TeamOwnersComponent',
    'TeamMembersComponent',
    'PlansComponent',
    'PlanOwnersComponent',
    'PlanMembersComponent',
    'WorkspacesComponent',
    'WorkspaceOwnersComponent',
    'services/Config',
    'services/TokenStore',
    'services/authenticationService',
    'services/feedbackService',
    'services/dataService',
    'interceptor/apiInterceptor',
    'appconfig'],
function ($, angular) {
    $(function () {
        angular.bootstrap(document, ['app']);
    });
});
