﻿import { app } from './app';
import * as angular from 'angular';
import { IAuthenticationService } from './Services/AuthenticationService';
import { IDataService } from './Services/DataService';
import { INotifyOptions } from 'interfaces/INotifyOptions';

import Component from './ComponentDecorator';
import User from './Model/User';
import Group from './Model/Group';

@Component(app, 'userPicker', {
    controllerAs: 'ctrl',
    bindings: {
        'withLicense': '<',
        'pickerSize': '<size'
    },
    require: {
        model: 'ngModel'
    },
    templateUrl: 'app/views/userpicker.html'
}) export class UserPickerComponent {
    public get userCountries(): string[] { return this.auth.userCountries; }
    public groups: Group[];
    public users: User[];
    public selection: User[];
    public required: boolean;
    public pickerSize: number = 8;
    public model: ng.INgModelController;
    public ownerPlaceHolder: string = 'Loading users...';
    public loading: boolean = false;
    public withLicense: boolean = false;
    public $onInit : () => void;
    public $onChanges: (x: ng.IOnChangesObject) => void;

    public addOwners(users: User[]) {
        (users||[]).forEach(u => {
            if (!this.selection.some(o => o.Id == u.Id)) {
                this.selection.push(u);
            }
        })
        this.onChange();
    }
    public removeOwners(users: User[]) {
        this.selection = this.selection.filter(o => !(users||[]).some(u => u.Id === o.Id));
        this.onChange();
    }

    constructor(
        private auth: IAuthenticationService,
        private dataService: IDataService,
        private notify: ng.cgNotify.INotifyService,
        private notifyOkConfig: INotifyOptions
    ) {
        auth.acquireToken().then(() => {
            return dataService.GetUsers().then(x => {
                this.loading = false;
                this.ownerPlaceHolder = "Search for users"
                this.users = x;
            });
        }).catch(e => { });
        this.$onInit = this.onInit.bind(this);
    }
    private isMatchFilter(filter: string) {
        return (value: string) => value != null && value.toLowerCase().indexOf(filter.toLowerCase()) > -1;
    }
    public matchUser(filter: string) {
        return (u: User) => {
            var isMatch = this.isMatchFilter(filter);
            var res = (!this.withLicense || u.HasLicenseAssigned);
            res = res && 
            (filter == null
                || filter.length === 0
                || isMatch(u.DisplayName)
                || isMatch(u.City)
                || isMatch(u.Department)
                || isMatch(u.Country)
                || isMatch(u.JobTitle)
                || isMatch(u.Mail)
                || isMatch(u.UserPrincipalName));
            return res;
        };
    }


    onInit(): void {
        this.model.$render = () => {
            this.selection = this.model.$viewValue;
        }
        this.model.$isEmpty = (value: any) => {
            return !(value && angular.isArray(value) && value.length > 0);
        };
    };

    onChange(): void {
        this.model.$setValidity("required", true);
        this.model.$setViewValue(this.selection);
        this.model.$commitViewValue();
    }

    public static $inject = ["authenticationService", "dataService", "notify", "notifyOkConfig"];
}