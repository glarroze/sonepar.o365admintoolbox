define(["require", "exports", "../app"], function (require, exports, app_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    app_1.app.factory('AppApiInterceptor', ['$q', '$rootScope', 'tokenStore', 'config', function ($q, $rootScope, tokenStore, config) {
            return {
                request: function (config) {
                    if (config) {
                        config.headers = config.headers || {};
                        if (tokenStore.token && config.url.match(/\/api\/(?!authenticat)/)) {
                            config.headers["Authorization"] = 'Bearer ' + tokenStore.token.access_token;
                        }
                    }
                    return config;
                }
            };
        }]);
});
//# sourceMappingURL=ApiInterceptor.js.map