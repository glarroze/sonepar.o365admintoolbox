﻿import { app } from '../app';
import { ITokenStore } from '../services/tokenstore';
import { IConfig } from '../interfaces/config';

app.factory('AppApiInterceptor', ['$q', '$rootScope', 'tokenStore', 'config', function ($q: ng.IQService, $rootScope: ng.IRootScopeService, tokenStore: ITokenStore, config: IConfig) {
    return {
        request: function (config: ng.IRequestConfig) {
            if (config) {
                config.headers = config.headers || {};
                if (tokenStore.token && config.url.match(/\/api\/(?!authenticat)/)) {
                    config.headers["Authorization"] = 'Bearer ' + tokenStore.token.access_token;
                }
            }
            return config;
        }
    };
}]);

