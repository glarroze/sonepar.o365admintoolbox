﻿import { app } from './app';
import * as angular from 'angular';
import { INotifyOptions } from 'interfaces/INotifyOptions';
import { IAuthenticationService } from './Services/AuthenticationService';
import { IDataService } from './Services/DataService';
import { IFeedbackService, Message } from './Services/FeedbackService';
import Country from './Model/Country';
import Component from './ComponentDecorator';
import User from './Model/User';
import License from './Model/License';
import { LicenseAssignment } from './Model/License';

@Component(app, 'feedback', {
    controllerAs: 'ctrl',
    bindings: { },
    templateUrl: 'app/views/feedback.html'
}) export class FeedbackComponent {
    public messages: Message[];
    private registration: {};
    public static $inject = [
        "feedbackService",
    ];
    constructor(
        private logservice: IFeedbackService
    ) {
        this.logservice.markRead();
        this.messages = logservice.getMessages();
        this.registration = logservice.registerForMessages((message) => {
            this.messages.unshift(message);
            this.logservice.markRead();
        });
    }
    private $onDestroy() {
        this.logservice.unregisterForMessages(this.registration);
    }
}
