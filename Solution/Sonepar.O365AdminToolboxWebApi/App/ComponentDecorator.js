define(["require", "exports", "angular"], function (require, exports, angular) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function Component(moduleOrName, selector, options) {
        return function (controller) {
            var module = typeof moduleOrName === "string"
                ? angular.module(moduleOrName)
                : moduleOrName;
            module.component(selector, angular.extend(options, { controller: controller }));
        };
    }
    exports.default = Component;
});
//# sourceMappingURL=ComponentDecorator.js.map