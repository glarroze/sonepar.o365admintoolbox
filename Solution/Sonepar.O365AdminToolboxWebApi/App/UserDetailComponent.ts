﻿import { app } from './app';
import { IAuthenticationService } from './Services/AuthenticationService';
import { IDataService } from './Services/DataService';
import Component from './ComponentDecorator';
import User from './Model/User';

@Component(app, 'userDetail', {
    controllerAs: 'ctrl',
    templateUrl: 'app/views/userDetail.html',
    bindings: { userId : "<" }
}) export class UserDetailComponent {
    public users: User[];
    public loading: boolean = false;
    constructor(private auth: IAuthenticationService, private dataService: IDataService) {
        auth.acquireToken().then(() => { this.loading = true });
        dataService.GetUsers().then(x => {
            this.users = x;
            this.loading = false;
        });
    }
    public static $inject = ["authenticationService", "dataService"];
}
