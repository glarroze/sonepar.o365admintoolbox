var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define(["require", "exports", "./app", "angular", "./ComponentDecorator", "./Model/OfficeObject"], function (require, exports, app_1, angular, ComponentDecorator_1, OfficeObject_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var PlansComponent = /** @class */ (function () {
        function PlansComponent($exceptionHandler, scroll, modal, auth, dataService, logService, notify, notifyOkConfig, config) {
            var _this = this;
            this.$exceptionHandler = $exceptionHandler;
            this.scroll = scroll;
            this.modal = modal;
            this.auth = auth;
            this.dataService = dataService;
            this.logService = logService;
            this.notify = notify;
            this.notifyOkConfig = notifyOkConfig;
            this.config = config;
            this.ownerPlaceHolder = 'Loading users...';
            this.saving = false;
            this.loading = false;
            this.isCreateMode = false;
            this.isOwnersMode = false;
            this.isMembersMode = false;
            this.isEditPlanMode = false;
            this.scroll.yOffset = 150;
            auth.acquireToken().then(function () {
                _this.getPlan();
                return dataService.GetUsers().then(function (x) {
                    _this.loading = false;
                    _this.ownerPlaceHolder = "Search for users";
                    _this.users = x;
                });
            }).catch(function (e) { });
            this.dataPlan = new OfficeObject_1.default();
            this.planNamePlaceholder = config.planNamePlaceholder;
        }
        Object.defineProperty(PlansComponent.prototype, "userCountries", {
            get: function () { return this.auth.userCountries; },
            enumerable: true,
            configurable: true
        });
        PlansComponent.prototype.addOwners = function (users) {
            var _this = this;
            users.forEach(function (u) {
                if (!_this.dataPlan.Owners.some(function (o) { return o.Id == u.Id; })) {
                    _this.dataPlan.Owners.push(u);
                }
            });
        };
        PlansComponent.prototype.removeOwners = function (users) {
            this.dataPlan.Owners = this.dataPlan.Owners.filter(function (o) { return !users.some(function (u) { return u.Id === o.Id; }); });
        };
        PlansComponent.prototype.showCreateForm = function () {
            this.scroll("edit");
            this.isCreateMode = true;
            this.isMembersMode = false;
            this.isOwnersMode = false;
            this.isEditPlanMode = false;
            this.selectedPlan = undefined;
        };
        PlansComponent.prototype.showMembersForm = function (plan) {
            this.scroll("edit");
            this.isMembersMode = true;
            this.isCreateMode = false;
            this.isOwnersMode = false;
            this.isEditPlanMode = false;
            this.selectedPlan = plan;
        };
        PlansComponent.prototype.showOwnersForm = function (plan) {
            this.scroll("edit");
            this.isMembersMode = false;
            this.isCreateMode = false;
            this.isOwnersMode = true;
            this.isEditPlanMode = false;
            this.selectedPlan = plan;
        };
        PlansComponent.prototype.showEditForm = function (plan) {
            console.log(plan);
            this.scroll("edit");
            this.isEditPlanMode = true;
            this.isCreateMode = false;
            this.isMembersMode = false;
            this.isOwnersMode = false;
            this.selectedPlan = __assign({}, plan);
        };
        PlansComponent.prototype.hideCreateForm = function () {
            this.dataPlan = new OfficeObject_1.default();
            this.isCreateMode = false;
        };
        PlansComponent.prototype.hideEditForm = function () {
            this.dataPlan = new OfficeObject_1.default();
            this.isEditPlanMode = false;
        };
        PlansComponent.prototype.sendPlan = function () {
            var _this = this;
            this.saving = true;
            var name = this.dataPlan.Name;
            this.dataService.PutPlan(this.dataPlan)
                .then(function (x) {
                _this.hideCreateForm();
                _this.getPlan();
                var c = angular.copy(_this.notifyOkConfig);
                c.message = "Plan saved";
                _this.notify(c);
                _this.logService.addMessage("Plan " + name + " created", "Trace");
                _this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.$exceptionHandler(e);
            }).finally(function () { _this.saving = false; });
        };
        PlansComponent.prototype.savePlan = function () {
            var _this = this;
            this.saving = true;
            var groupId = this.selectedPlan ? this.selectedPlan.Id : "";
            var groupName = this.selectedPlan ? this.selectedPlan.Name : "";
            var datasToUpdate = {
                Id: groupId,
                Name: groupName,
            };
            console.log("planName", groupName);
            this.dataService.UpdatePlan(datasToUpdate)
                .then(function (x) {
                _this.hideEditForm();
                _this.getPlan();
                var c = angular.copy(_this.notifyOkConfig);
                c.message = "Plan saved";
                _this.notify(c);
                _this.logService.addMessage("Plan " + name + " updated", "Trace");
                _this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.$exceptionHandler(e);
            }).finally(function () { _this.saving = false; });
        };
        PlansComponent.prototype.getPlan = function () {
            var _this = this;
            this.loading = true;
            this.dataService.GetPlans().then(function (x) {
                _this.plans = x;
                _this.loading = false;
            });
        };
        PlansComponent.prototype.isMatchFilter = function (filter) {
            return function (value) { return value != null && value.toLowerCase().indexOf(filter.toLowerCase()) > -1; };
        };
        PlansComponent.prototype.matchUser = function (filter) {
            var _this = this;
            return function (u) {
                var isMatch = _this.isMatchFilter(filter);
                return (filter == null
                    || filter.length === 0
                    || isMatch(u.DisplayName)
                    || isMatch(u.City)
                    || isMatch(u.Department)
                    || isMatch(u.Country)
                    || isMatch(u.JobTitle)
                    || isMatch(u.Mail)
                    || isMatch(u.UserPrincipalName));
            };
        };
        PlansComponent.prototype.tryDelete = function (plan) {
            var _this = this;
            var modal = this.modal.open({
                size: 'lg',
                controller: DeletePlanController,
                controllerAs: "ctrl",
                templateUrl: 'app/views/deletePlan.html',
                resolve: {
                    "plan": plan
                }
            });
            modal.closed.catch(function () { });
            modal.result.then(function () { return _this.dataService.DeletePlan(plan); }).then(function () {
                _this.getPlan();
                var notification = angular.copy(_this.notifyOkConfig);
                notification.message = "Plan \"" + plan.Name + "\" has been successfully deleted";
                _this.notify(notification);
                _this.logService.addMessage("Plan \"" + plan.Name + "\" has been successfully deleted", "Info");
                _this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.$exceptionHandler(e);
            });
        };
        PlansComponent.$inject = ["$exceptionHandler", "$anchorScroll", "$uibModal", "authenticationService", "dataService", "feedbackService", "notify", "notifyOkConfig", "config"];
        PlansComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'plans', {
                controllerAs: 'ctrl',
                templateUrl: 'app/views/plans.html'
            })
        ], PlansComponent);
        return PlansComponent;
    }());
    exports.PlansComponent = PlansComponent;
    var DeletePlanController = /** @class */ (function () {
        function DeletePlanController($uibModalInstance, plan) {
            this.$uibModalInstance = $uibModalInstance;
            this.plan = plan;
        }
        DeletePlanController.prototype.cancel = function () {
            this.$uibModalInstance.dismiss();
        };
        DeletePlanController.prototype.ok = function () {
            this.$uibModalInstance.close();
        };
        DeletePlanController.$inject = ["$uibModalInstance", "plan"];
        return DeletePlanController;
    }());
});
//# sourceMappingURL=PlansComponent.js.map