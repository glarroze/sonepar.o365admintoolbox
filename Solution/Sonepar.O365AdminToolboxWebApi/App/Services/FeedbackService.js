define(["require", "exports", "../app", "moment"], function (require, exports, app_1, moment) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var re = /[xy]/g;
    function guid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(re, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
    var FeedbackService = /** @class */ (function () {
        function FeedbackService() {
            this.$inject = [];
            this.messages = [];
            this.registered = {};
            this._unreadFeedbacks = 0;
        }
        FeedbackService.prototype.markRead = function () {
            this._unreadFeedbacks = 0;
        };
        Object.defineProperty(FeedbackService.prototype, "unreadFeedbacks", {
            get: function () {
                return this._unreadFeedbacks;
            },
            enumerable: true,
            configurable: true
        });
        FeedbackService.prototype.getMessages = function () {
            return this.messages.map(function (x) { return x; });
        };
        FeedbackService.prototype.addMessage = function (message, severity) {
            var m = { message: message, severity: severity, date: moment() };
            this.messages.unshift(m);
            this._unreadFeedbacks++;
            var f = this.registered;
            Object.keys(f).forEach(function (x) { return f[x](m); });
        };
        FeedbackService.prototype.registerForMessages = function (onMessage) {
            var id = { id: guid() };
            this.registered[id.id] = onMessage;
            return id;
        };
        FeedbackService.prototype.unregisterForMessages = function (id) {
            delete this.registered[id.id];
        };
        return FeedbackService;
    }());
    exports.FeedbackService = FeedbackService;
    app_1.app.service("feedbackService", FeedbackService);
});
//# sourceMappingURL=FeedbackService.js.map