﻿import { app } from '../app';
import * as  moment from 'moment';

export type MessageSeverity = 'Info' | 'Warning' | 'Error' | 'Trace';
export type Message = { message: string, severity: MessageSeverity, date?: moment.Moment };

var re = /[xy]/g;
function guid(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(re, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

export interface IFeedbackService {
    getMessages(): Message[];
    addMessage(message: string, severity: MessageSeverity): void;
    registerForMessages(onMessage: ((message: Message) => void)): {};
    unregisterForMessages(id: {}): void;
    unreadFeedbacks: number;
    markRead():void;
}

export class FeedbackService implements IFeedbackService {
    private $inject: string[] = [];
    private messages: Message[] = [];

    private registered: { [id: string]: ((message: Message) => void) } = {};

    private _unreadFeedbacks: number = 0;
    public markRead() {
        this._unreadFeedbacks = 0;
    }
    public get unreadFeedbacks() {
        return this._unreadFeedbacks;
    }

    public constructor() {
    }
    getMessages(): Message[] {
        return this.messages.map(x=>x);
    }
    addMessage(message: string, severity: MessageSeverity): void {
        var m = { message: message, severity: severity, date: moment() };
        this.messages.unshift(m);
        this._unreadFeedbacks++;
        var f = this.registered;
        Object.keys(f).forEach(x => f[x](m));
    }
    registerForMessages(onMessage: (message: Message) => void): {} {
        var id = { id: guid() };
        this.registered[id.id] = onMessage;
        return id;
    }
    unregisterForMessages(id: {}) {
        delete this.registered[(<any>id).id];
    }

}
app.service("feedbackService", FeedbackService);