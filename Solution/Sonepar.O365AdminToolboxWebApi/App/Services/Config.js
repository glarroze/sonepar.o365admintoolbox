define(["require", "exports", "../app"], function (require, exports, app_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ConfigProvider = /** @class */ (function () {
        function ConfigProvider() {
            this.$get.$inject = ['appConfig'];
        }
        ConfigProvider.prototype.$get = function (appConfig) { return appConfig; };
        return ConfigProvider;
    }());
    //class Config implements IConfig {
    //    public BaseServiceUrl: string = "https://localhost:44305/";
    //    public ApiBaseUrl: string = "https://localhost:44305/api/";
    //    public clientId :string = 'b9ce623f-946f-41e1-8eb0-ea45581d567f';
    //    public tenant: string = "MOD933703.onmicrosoft.com";
    //    public appId: string = "https://MOD933703.onmicrosoft.com/Sonepar.O365AdminToolboxWebApi";
    //    //public endpoints: { [uri: string]: string } = { 'http://localhost:44305/api/authentication': 'https://MOD933703.onmicrosoft.com/Sonepar.O365AdminToolboxWebApi' }
    //    public endpoints: {}; //{ [uri: string]: string } = { 'http://localhost:44305/api/authentication': 'b9ce623f-946f-41e1-8eb0-ea45581d567f' }
    //}
    app_1.app.provider("config", ConfigProvider);
});
//# sourceMappingURL=Config.js.map