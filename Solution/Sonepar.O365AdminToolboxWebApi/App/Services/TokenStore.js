define(["require", "exports", "../app"], function (require, exports, app_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var TokenStore = /** @class */ (function () {
        function TokenStore() {
        }
        Object.defineProperty(TokenStore.prototype, "token", {
            get: function () {
                return this._token;
            },
            set: function (value) {
                this._token = value;
            },
            enumerable: true,
            configurable: true
        });
        TokenStore.prototype.clear = function () {
            this.token = undefined;
        };
        return TokenStore;
    }());
    app_1.app.service('tokenStore', TokenStore);
});
//# sourceMappingURL=TokenStore.js.map