define(["require", "exports", "../app", "../Model/User", "../Model/License", "adal"], function (require, exports, app_1, User_1, License_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var DataService = /** @class */ (function () {
        function DataService($q, $http, $timeout, config, auth) {
            this.$q = $q;
            this.$http = $http;
            this.$timeout = $timeout;
            this.config = config;
            this.auth = auth;
            this.cacheDate = new Date(0);
        }
        DataService.prototype.GetUsers = function (refresh) {
            var _this = this;
            if (refresh === void 0) { refresh = false; }
            var now = new Date();
            var cacheExpirationInMs = this.config.cacheExpiration * 1000;
            var expired = (now.getTime() - this.cacheDate.getTime() > cacheExpirationInMs);
            if (!this.usersProm || refresh || expired) {
                this.usersProm = this.auth.acquireToken().then(function (x) {
                    _this.cacheDate = now;
                    return _this.$http.get(_this.config.ApiBaseUrl + 'user');
                }).then(function (x) {
                    return (x.data || []);
                });
            }
            return this.usersProm;
        };
        DataService.prototype.GetUserGroups = function (refresh) {
            var _this = this;
            if (refresh === void 0) { refresh = false; }
            var now = new Date();
            var cacheExpirationInMs = this.config.cacheExpiration * 1000;
            var expired = (now.getTime() - this.cacheDate.getTime() > cacheExpirationInMs);
            if (!this.usersProm || refresh || expired) {
                this.userGroupsProm = this.auth.acquireToken().then(function (x) {
                    _this.cacheDate = now;
                    return _this.$http.get(_this.config.ApiBaseUrl + 'user/groups');
                }).then(function (x) {
                    console.log(x);
                    return x.data || [];
                });
            }
            return this.userGroupsProm;
        };
        //TODO Comment
        DataService.prototype.GetAllUsers = function () {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + 'Users');
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.UpdateUserStatut = function (user, statut) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + ("user/" + user.Id + "/statut"), statut);
            }).then(function (x) {
                return true;
            }).catch(function (e) { return false; });
        };
        DataService.prototype.GetUserMFAStatut = function (user) {
            var _this = this;
            console.log('GetUserMFAStatut', user);
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + ("User/" + user.UserPrincipalName + "/mfastatus"));
            }).then(function (x) {
                return x.data;
            }).catch(function (e) { return false; });
        };
        DataService.prototype.UpdateUserMFAStatut = function (user, statut) {
            var _this = this;
            console.log(user, statut);
            var payload = {
                userStatut: statut,
                userPrincipalName: user.Mail
            };
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + "user/mfastatut", payload);
            }).then(function (x) {
                return x.data;
            }).catch(function (e) { return false; });
        };
        DataService.prototype.IsCurrentUserLicenceManager = function () {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + 'user/iscurrentuserlicencemanager');
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.GetUser = function (id) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + ("user/" + id), { cache: false });
            }).then(function (x) {
                return new User_1.default(x.data);
            });
        };
        DataService.prototype.SetUserLicense = function (user) {
        };
        DataService.prototype.GetCountries = function () {
            return this.$http.get(this.config.BaseServiceUrl + "Content/countries.json")
                .then(function (x) { return x.data; });
        };
        DataService.prototype.GetLicenses = function () {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + 'ServicePlan');
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.GetLicensesNames = function () {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + 'ServicePlan/friendlynames');
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.UpdateLicencesNames = function (licenceNameList) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + 'ServicePlan/update', licenceNameList);
            }).then(function (x) { return x; });
        };
        DataService.prototype.SaveLicenses = function (users, Licenses, cb) {
            var _this = this;
            var toAdd = [];
            var toUpd = [];
            var toRemove = [];
            users.forEach(function (u) {
                Licenses.forEach(function (l) {
                    var prevActive = u.AssignedLicenses.filter(function (al) { return al.Id === l.License.Id; })[0];
                    if (prevActive && !_this.defaultVal(true, l.IsActive) && l.License.Id) {
                        toRemove.push({ user: u, license: l.License });
                    }
                    else if (!prevActive && _this.defaultVal(false, l.IsActive)) {
                        var nl = new License_1.default(l.License);
                        nl.Services.forEach(function (s) { return s.IsActive = _this.defaultVal(true, s.IsActive); });
                        toAdd.push({ user: u, license: nl });
                    }
                    else if (prevActive && l.IsActive) {
                        var nl = new License_1.default(prevActive);
                        nl.Services.forEach(function (s) {
                            var ns = l.License.Services.filter(function (ss) { return s.Service.Id === ss.Service.Id; })[0];
                            s.IsActive = _this.defaultVal(_this.defaultVal(true, s.IsActive), ns && ns.IsActive);
                        });
                        var changed = nl.Services.some(function (s) {
                            var ns = prevActive.Services.filter(function (ss) { return s.Service.Id === ss.Service.Id; })[0];
                            return !ns || ns.IsActive != s.IsActive;
                        });
                        if (changed) {
                            toUpd.push({ user: u, license: nl });
                        }
                    }
                });
                u.AssignedLicenses.forEach(function (l) {
                    l.Services.map(function (s) { return s.IsActive; });
                });
            });
            var promsUpd = toUpd.map(function (r) {
                return ({ u: r.user, f: function () { return _this.$http.post(_this.config.ApiBaseUrl + ("ServicePlan/" + r.user.Id + "/plans"), r.license).then(function () { cb && cb('updated', r.user, r.license.FriendlyName); }); } });
            });
            var promsDel = toRemove.map(function (r) {
                return ({ u: r.user, f: function () { return _this.$http.delete(_this.config.ApiBaseUrl + ("ServicePlan/" + r.user.Id + "/plans/" + r.license.Id)).then(function () { return cb && cb('removed', r.user, r.license.FriendlyName); }); } });
            });
            var promsAdd = toAdd.map(function (r) {
                return ({ u: r.user, f: function () { return _this.$http.post(_this.config.ApiBaseUrl + ("ServicePlan/" + r.user.Id + "/plans"), r.license).then(function () { return cb && cb('added', r.user, r.license.FriendlyName); }); } });
            });
            var proms = promsDel.concat(promsUpd).concat(promsAdd);
            var prom = this.$q.when(0);
            var prev = null;
            for (var _i = 0, proms_1 = proms; _i < proms_1.length; _i++) {
                var p = proms_1[_i];
                if (prev && prev == p.u) {
                    prom = prom.then(function () { return _this.$timeout(1000); });
                }
                prom = prom.then(p.f).catch(function (e) {
                    throw new Error("operation failed for user " + p.u.DisplayName + " : " + (e && (e.data || e.ExceptionMessage || e.Message) || e));
                });
            }
            if (proms.length === 0) {
                return this.$q.reject("Nothing to update");
            }
            return prom;
        };
        DataService.prototype.GetUnregisteredGroups = function () {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + 'unregisteredgroups');
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.GetGroups = function () {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + 'group');
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.UpdateGroup = function (datasToUpdate) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + "group/update", JSON.stringify(datasToUpdate));
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.DeleteGroup = function (group) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.delete(_this.config.ApiBaseUrl + ("group/" + group.Id));
            }).then(function (x) { });
        };
        DataService.prototype.PutGroup = function (data) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + 'group', JSON.stringify(data));
            }).then(function (x) {
                return x.statusText;
            });
        };
        DataService.prototype.GetGroupOwners = function (id) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + ("group/" + id + "/owners"));
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.UpdateGroupOwners = function (id, users, cb) {
            var _this = this;
            var toAdd = [];
            var toRemove = [];
            users = users || [];
            return this.GetGroupOwners(id).then(function (oldMembers) {
                toRemove = oldMembers.filter(function (om) { return !users.some(function (u) { return u.Id === om.Id; }); });
                toAdd = users.filter(function (u) { return !oldMembers.some(function (om) { return u.Id === om.Id; }); });
            }).then(function () { return _this.$q.all(toAdd.map(function (a) {
                return _this.$http.put(_this.config.ApiBaseUrl + ("group/" + id + "/owners"), a).then(function () { return cb && cb('added', a); });
            })); }).then(function () { return _this.$q.all(toRemove.map(function (r) {
                return _this.$http.delete(_this.config.ApiBaseUrl + ("group/" + id + "/owners/" + r.Id)).then(function () { return cb && cb('removed', r); });
            })); }).then(function () { });
        };
        DataService.prototype.GetGroupMembers = function (id) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + ("group/" + id + "/users"));
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.UpdateGroupMembers = function (id, users, cb) {
            var _this = this;
            var toAdd = [];
            var toRemove = [];
            users = users || [];
            return this.GetGroupMembers(id).then(function (oldMembers) {
                toRemove = oldMembers.filter(function (om) { return !users.some(function (u) { return u.Id === om.Id; }); });
                toAdd = users.filter(function (u) { return !oldMembers.some(function (om) { return u.Id === om.Id; }); });
            }).then(function () { return _this.$q.all(toAdd.map(function (a) {
                return _this.$http.put(_this.config.ApiBaseUrl + ("group/" + id + "/users"), a).then(function () { return cb && cb('added', a); });
            })); }).then(function () { return _this.$q.all(toRemove.map(function (r) {
                return _this.$http.delete(_this.config.ApiBaseUrl + ("group/" + id + "/users/" + r.Id)).then(function () { return cb && cb('removed', r); });
            })); }).then(function () { });
        };
        DataService.prototype.GetTeams = function () {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + 'team');
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.UpdateTeam = function (datasToUpdate) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + "team/update", JSON.stringify(datasToUpdate));
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.DeleteTeam = function (team) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.delete(_this.config.ApiBaseUrl + ("team/" + team.Id));
            }).then(function (x) { });
        };
        DataService.prototype.PutTeam = function (data) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + 'team', JSON.stringify(data));
            }).then(function (x) {
                return x.statusText;
            });
        };
        DataService.prototype.GetTeamOwners = function (id) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + ("team/" + id + "/owners"));
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.UpdateTeamOwners = function (id, users, cb) {
            var _this = this;
            var toAdd = [];
            var toRemove = [];
            users = users || [];
            return this.GetTeamOwners(id).then(function (oldMembers) {
                toRemove = oldMembers.filter(function (om) { return !users.some(function (u) { return u.Id === om.Id; }); });
                toAdd = users.filter(function (u) { return !oldMembers.some(function (om) { return u.Id === om.Id; }); });
            }).then(function () { return _this.$q.all(toAdd.map(function (a) {
                return _this.$http.put(_this.config.ApiBaseUrl + ("team/" + id + "/owners"), a).then(function () { return cb && cb('added', a); });
            })); }).then(function () { return _this.$q.all(toRemove.map(function (r) {
                return _this.$http.delete(_this.config.ApiBaseUrl + ("team/" + id + "/owners/" + r.Id)).then(function () { return cb && cb('removed', r); });
            })); }).then(function () { });
        };
        DataService.prototype.GetTeamMembers = function (id) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + ("team/" + id + "/users"));
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.UpdateTeamMembers = function (id, users, cb) {
            var _this = this;
            var toAdd = [];
            var toRemove = [];
            users = users || [];
            return this.GetTeamMembers(id).then(function (oldMembers) {
                toRemove = oldMembers.filter(function (om) { return !users.some(function (u) { return u.Id === om.Id; }); });
                toAdd = users.filter(function (u) { return !oldMembers.some(function (om) { return u.Id === om.Id; }); });
            }).then(function () { return _this.$q.all(toAdd.map(function (a) {
                return _this.$http.put(_this.config.ApiBaseUrl + ("team/" + id + "/users"), a).then(function () { return cb && cb('added', a); });
            })); }).then(function () { return _this.$q.all(toRemove.map(function (r) {
                return _this.$http.delete(_this.config.ApiBaseUrl + ("team/" + id + "/users/" + r.Id)).then(function () { return cb && cb('removed', r); });
            })); }).then(function () { });
        };
        DataService.prototype.GetPlans = function () {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + 'plan');
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.UpdatePlan = function (datasToUpdate) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + "plan/update", JSON.stringify(datasToUpdate));
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.DeletePlan = function (plan) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.delete(_this.config.ApiBaseUrl + ("plan/" + plan.Id));
            }).then(function (x) { });
        };
        DataService.prototype.PutPlan = function (data) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + 'plan', JSON.stringify(data));
            }).then(function (x) {
                return x.statusText;
            });
        };
        DataService.prototype.GetPlanOwners = function (id) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + ("plan/" + id + "/owners"));
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.UpdatePlanOwners = function (id, users, cb) {
            var _this = this;
            var toAdd = [];
            var toRemove = [];
            users = users || [];
            return this.GetPlanOwners(id).then(function (oldMembers) {
                toRemove = oldMembers.filter(function (om) { return !users.some(function (u) { return u.Id === om.Id; }); });
                toAdd = users.filter(function (u) { return !oldMembers.some(function (om) { return u.Id === om.Id; }); });
            }).then(function () { return _this.$q.all(toAdd.map(function (a) {
                return _this.$http.put(_this.config.ApiBaseUrl + ("plan/" + id + "/owners"), a).then(function () { return cb && cb('added', a); });
            })); }).then(function () { return _this.$q.all(toRemove.map(function (r) {
                return _this.$http.delete(_this.config.ApiBaseUrl + ("plan/" + id + "/owners/" + r.Id)).then(function () { return cb && cb('removed', r); });
            })); }).then(function () { });
        };
        DataService.prototype.GetPlanMembers = function (id) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + ("plan/" + id + "/users"));
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.UpdatePlanMembers = function (id, users, cb) {
            var _this = this;
            var toAdd = [];
            var toRemove = [];
            users = users || [];
            return this.GetPlanMembers(id).then(function (oldMembers) {
                toRemove = oldMembers.filter(function (om) { return !users.some(function (u) { return u.Id === om.Id; }); });
                toAdd = users.filter(function (u) { return !oldMembers.some(function (om) { return u.Id === om.Id; }); });
            }).then(function () { return _this.$q.all(toAdd.map(function (a) {
                return _this.$http.put(_this.config.ApiBaseUrl + ("plan/" + id + "/users"), a).then(function () { return cb && cb('added', a); });
            })); }).then(function () { return _this.$q.all(toRemove.map(function (r) {
                return _this.$http.delete(_this.config.ApiBaseUrl + ("plan/" + id + "/users/" + r.Id)).then(function () { return cb && cb('removed', r); });
            })); }).then(function () { });
        };
        DataService.prototype.GetWorkspaces = function () {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + 'workspace');
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.GetUnregisteredWorkspaces = function () {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + 'unregisteredworkspaces');
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.GetGroupMembersByPage = function (userPayload) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.post(_this.config.ApiBaseUrl + "user/group/members", JSON.stringify(userPayload));
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.GetGroupAllMembers = function (grpId) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + ("user/group/allmembers/" + grpId), { cache: false });
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.UpdateWorkspace = function (workspaceId, datas) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + ("workspace/update/" + workspaceId), JSON.stringify(datas));
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.DeleteWorkspace = function (workspace) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + 'workspace/delete', JSON.stringify(workspace));
            }).then(function (x) { });
        };
        DataService.prototype.CreateTeamSite = function (data) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + 'workspace/createteamsite', JSON.stringify(data), { timeout: 600000 });
            }).then(function (x) { return x.data; });
        };
        DataService.prototype.AddWorkspaceOwners = function (data) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + 'workspace/addowners', JSON.stringify(data), { timeout: 600000 });
            }).then(function (x) { return x.data; });
        };
        DataService.prototype.PutWorkspace = function (data) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.put(_this.config.ApiBaseUrl + 'workspace', JSON.stringify(data), { timeout: 600000 });
            }).then(function (x) { return x.data; });
        };
        DataService.prototype.GetWorkspaceOwners = function (workspaceId) {
            var _this = this;
            return this.auth.acquireToken().then(function (x) {
                return _this.$http.get(_this.config.ApiBaseUrl + ("workspace/" + workspaceId + "/owners"));
            }).then(function (x) {
                return x.data;
            });
        };
        DataService.prototype.UpdateWorkspaceOwners = function (workspaceId, users, cb) {
            var _this = this;
            var toAdd = [];
            var toRemove = [];
            users = users || [];
            return this.GetWorkspaceOwners(workspaceId).then(function (oldMembers) {
                toRemove = oldMembers.filter(function (om) { return !users.some(function (u) { return u.Id === om.Id; }); });
                toAdd = users.filter(function (u) { return !oldMembers.some(function (om) { return u.Id === om.Id; }); });
            }).then(function () { return _this.$q.all(toAdd.map(function (a) {
                return _this.$http.put(_this.config.ApiBaseUrl + ("workspace/" + workspaceId + "/owners"), a).then(function () { return cb && cb('added', a); });
            })); }).then(function () { return _this.$q.all(toRemove.map(function (r) {
                return _this.$http.delete(_this.config.ApiBaseUrl + ("workspace/" + workspaceId + "/owners/" + r.Id)).then(function () { return cb && cb('removed', r); });
            })); }).then(function () { });
        };
        DataService.prototype.defaultVal = function (d, b) {
            return b == null ? d : b;
        };
        DataService.prototype.defaultTrue = function (b) {
            return this.defaultVal(true, b);
        };
        DataService.prototype.defaultFalse = function (b) {
            return this.defaultVal(false, b);
        };
        DataService.$inject = ["$q", "$http", "$timeout", "config", "authenticationService"];
        return DataService;
    }());
    app_1.app.service("dataService", DataService);
});
//# sourceMappingURL=DataService.js.map