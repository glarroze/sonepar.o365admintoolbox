﻿import { app } from '../app';
import { IConfig, IConfigProvider } from '../interfaces/config'

class ConfigProvider implements IConfigProvider, ng.IServiceProvider {
    public constructor() {
        this.$get.$inject = ['appConfig'];
    }
    public $get(appConfig: IConfig): IConfig { return appConfig; }
    //public default: IConfig = new Config();

}
//class Config implements IConfig {
//    public BaseServiceUrl: string = "https://localhost:44305/";
//    public ApiBaseUrl: string = "https://localhost:44305/api/";
//    public clientId :string = 'b9ce623f-946f-41e1-8eb0-ea45581d567f';
//    public tenant: string = "MOD933703.onmicrosoft.com";
//    public appId: string = "https://MOD933703.onmicrosoft.com/Sonepar.O365AdminToolboxWebApi";
//    //public endpoints: { [uri: string]: string } = { 'http://localhost:44305/api/authentication': 'https://MOD933703.onmicrosoft.com/Sonepar.O365AdminToolboxWebApi' }
//    public endpoints: {}; //{ [uri: string]: string } = { 'http://localhost:44305/api/authentication': 'b9ce623f-946f-41e1-8eb0-ea45581d567f' }
//}
app.provider("config", ConfigProvider);