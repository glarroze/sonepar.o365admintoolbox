﻿import { app } from '../app';
import TokenResult from '../model/Token'

export interface ITokenStore {
    token: TokenResult | undefined;
    clear(): void;
}

class TokenStore implements ITokenStore{ 
    private _token: TokenResult | undefined;
    public get token(): TokenResult | undefined {
        return this._token;
    }
    public set token(value: TokenResult | undefined) {
        this._token = value;
    }
    public clear() {
        this.token = undefined;
    }
}

app.service('tokenStore', TokenStore);