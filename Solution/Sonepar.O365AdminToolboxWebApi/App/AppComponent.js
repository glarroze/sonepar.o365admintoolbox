var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define(["require", "exports", "./app", "./ComponentDecorator"], function (require, exports, app_1, ComponentDecorator_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var AppComponent = /** @class */ (function () {
        function AppComponent($window, auth) {
            this.$window = $window;
            this.auth = auth;
        }
        Object.defineProperty(AppComponent.prototype, "isMain", {
            get: function () {
                return this.$window.parent === this.$window.self;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AppComponent.prototype, "show", {
            get: function () {
                return this.auth.userCountries.length > 0;
            },
            enumerable: true,
            configurable: true
        });
        AppComponent.$inject = ["$window", "authenticationService"];
        AppComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'app', {
                controllerAs: 'ctrl',
                templateUrl: 'app/views/app.html'
            })
        ], AppComponent);
        return AppComponent;
    }());
    exports.AppComponent = AppComponent;
});
//app.component('appComponent', AppComponent);
//# sourceMappingURL=AppComponent.js.map