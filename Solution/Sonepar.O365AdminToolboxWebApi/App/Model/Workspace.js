var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./OfficeObject"], function (require, exports, OfficeObject_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Workspace = /** @class */ (function (_super) {
        __extends(Workspace, _super);
        function Workspace(workspace) {
            var _this = _super.call(this, workspace) || this;
            if (workspace) {
                _this.Lcid = workspace.Lcid;
                _this.Description = workspace.Description;
                _this.TimeZoneId = workspace.TimeZoneId;
            }
            return _this;
        }
        return Workspace;
    }(OfficeObject_1.default));
    exports.default = Workspace;
});
//# sourceMappingURL=Workspace.js.map