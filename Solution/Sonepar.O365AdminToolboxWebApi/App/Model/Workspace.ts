﻿import OfficeObject from "./OfficeObject";

export default class Workspace extends OfficeObject {

    public constructor(workspace?: Workspace) {
        super(workspace)
        if (workspace) {
            this.Lcid = workspace.Lcid;
            this.Description = workspace.Description;
            this.TimeZoneId = workspace.TimeZoneId;

        }
    }
    public TimeZoneId: number;
    public Lcid: string;
    public Description: string;
}