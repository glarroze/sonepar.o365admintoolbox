﻿import * as moment from 'moment';
export default class TokenResult
{
    constructor(token?: TokenResult) {
        if (token) {
            this.access_token = token.access_token;
            this.token_type = token.token_type;
            this.expires_in = token.expires_in;
        }
        this.expires_ref = moment();
        
    }

    public access_token:string;
    public expires_in:number;
    public expires_ref: moment.Moment;
    public get expires_at(): moment.Moment{
        return this.expires_ref.clone().add(this.expires_in);
    } 
    public token_type: string;

}
