﻿import Member from './Member';
import License from './License';
import Service from './Service';
import * as moment from 'moment';

export default class User extends Member {

    public constructor(user?: User) {
        super(user)
        if (user) {
            this.Country = user.Country;
            this.AccountEnabled = user.AccountEnabled
            this.AssignedLicenses = user.AssignedLicenses;
            this.AssignedPlans = user.AssignedPlans;
            this.City = user.City;
            this.CompanyName = user.CompanyName;
            this.Country = user.Country;
            this.Department = user.Department;
            this.OnPremisesSyncEnabled = user.OnPremisesSyncEnabled;
            this.DisplayName = user.DisplayName;
            this.BusinessPhones = user.BusinessPhones;
            this.GivenName = user.GivenName;
            this.JobTitle = user.JobTitle;
            this.OnPremisesLastSyncDateTime = moment(user.OnPremisesLastSyncDateTime);
            this.Mail = user.Mail;
            this.MailNickname = user.MailNickname;
            this.MobilePhone = user.MobilePhone;
            this.OnPremisesSecurityIdentifier = user.OnPremisesSecurityIdentifier;
            this.PasswordPolicies = user.PasswordPolicies;
            this.OfficeLocation = user.OfficeLocation;
            this.PostalCode = user.PostalCode;
            this.PreferredLanguage = user.PreferredLanguage;
            this.State = user.State;
            this.StreetAddress = user.StreetAddress;
            this.Surname = user.Surname;
            this.UsageLocation = user.UsageLocation;
            this.UserPrincipalName = user.UserPrincipalName;
            this.UserType = user.UserType;
            this.HasLicenseAssigned = user.HasLicenseAssigned;

            // this.TenantCountries = user.TenantCountries.map(x => x);
        }
    }


    public AccountEnabled: boolean | undefined;
    public AssignedLicenses: License[];
    public AssignedPlans: Service[];
    public City: string;
    public CompanyName: string;
    public Country: string;
    public Department: string;
    public OnPremisesSyncEnabled: boolean | undefined;
    public DisplayName: string;
    public BusinessPhones: string;
    public GivenName: string;
    public JobTitle: string;
    public OnPremisesLastSyncDateTime: moment.Moment;
    public Mail: string;
    public MailNickname: string;
    public MobilePhone: string;
    public OnPremisesSecurityIdentifier: string;
    public PasswordPolicies: string;
    public OfficeLocation: string;
    public PostalCode: string;
    public PreferredLanguage: string;
    public State: string;
    public StreetAddress: string;
    public Surname: string;
    public UsageLocation: string;
    public UserPrincipalName: string;
    public UserType: string;
    public TenantCountries: string[];
    public HasLicenseAssigned: boolean;
}