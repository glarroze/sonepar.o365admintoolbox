var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./Member", "moment"], function (require, exports, Member_1, moment) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var User = /** @class */ (function (_super) {
        __extends(User, _super);
        function User(user) {
            var _this = _super.call(this, user) || this;
            if (user) {
                _this.Country = user.Country;
                _this.AccountEnabled = user.AccountEnabled;
                _this.AssignedLicenses = user.AssignedLicenses;
                _this.AssignedPlans = user.AssignedPlans;
                _this.City = user.City;
                _this.CompanyName = user.CompanyName;
                _this.Country = user.Country;
                _this.Department = user.Department;
                _this.OnPremisesSyncEnabled = user.OnPremisesSyncEnabled;
                _this.DisplayName = user.DisplayName;
                _this.BusinessPhones = user.BusinessPhones;
                _this.GivenName = user.GivenName;
                _this.JobTitle = user.JobTitle;
                _this.OnPremisesLastSyncDateTime = moment(user.OnPremisesLastSyncDateTime);
                _this.Mail = user.Mail;
                _this.MailNickname = user.MailNickname;
                _this.MobilePhone = user.MobilePhone;
                _this.OnPremisesSecurityIdentifier = user.OnPremisesSecurityIdentifier;
                _this.PasswordPolicies = user.PasswordPolicies;
                _this.OfficeLocation = user.OfficeLocation;
                _this.PostalCode = user.PostalCode;
                _this.PreferredLanguage = user.PreferredLanguage;
                _this.State = user.State;
                _this.StreetAddress = user.StreetAddress;
                _this.Surname = user.Surname;
                _this.UsageLocation = user.UsageLocation;
                _this.UserPrincipalName = user.UserPrincipalName;
                _this.UserType = user.UserType;
                _this.HasLicenseAssigned = user.HasLicenseAssigned;
                // this.TenantCountries = user.TenantCountries.map(x => x);
            }
            return _this;
        }
        return User;
    }(Member_1.default));
    exports.default = User;
});
//# sourceMappingURL=User.js.map