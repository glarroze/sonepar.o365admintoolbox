define(["require", "exports", "moment"], function (require, exports, moment) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var TokenResult = /** @class */ (function () {
        function TokenResult(token) {
            if (token) {
                this.access_token = token.access_token;
                this.token_type = token.token_type;
                this.expires_in = token.expires_in;
            }
            this.expires_ref = moment();
        }
        Object.defineProperty(TokenResult.prototype, "expires_at", {
            get: function () {
                return this.expires_ref.clone().add(this.expires_in);
            },
            enumerable: true,
            configurable: true
        });
        return TokenResult;
    }());
    exports.default = TokenResult;
});
//# sourceMappingURL=Token.js.map