﻿import Member from './Member'
export default class Group extends Member {

    public constructor(group?: Group) {
        super(group)
        if (group) {
            this.Country = group.Country;
            this.Description = group.Description;
            this.Mail = group.Mail;
            this.MailNickname = group.MailNickname;
            this.Name = group.Name;
            this.Owners = group.Owners;
            this.Visibility = group.Visibility;
        }
        this.Owners = this.Owners || [];
    }

    public Country: string;
    public Description: string;
    public Mail: string;
    public MailNickname: string;
    public Name: string;
    public Owners: Member[];
    public Visibility: 'Private' | 'Public' | undefined = undefined;// = 'Private';
}
