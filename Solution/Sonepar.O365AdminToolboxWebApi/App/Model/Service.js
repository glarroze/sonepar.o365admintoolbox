define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Service = /** @class */ (function () {
        function Service(serv) {
            if (serv) {
                this.FriendlyName = serv.FriendlyName;
                this.Id = serv.Id;
                this.Name = serv.Name;
                this.AppliesTo = serv.AppliesTo;
            }
        }
        Object.defineProperty(Service.prototype, "sId", {
            get: function () {
                return this.Id.replace(/-/g, '');
            },
            enumerable: true,
            configurable: true
        });
        return Service;
    }());
    exports.default = Service;
});
//# sourceMappingURL=Service.js.map