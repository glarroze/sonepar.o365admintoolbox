var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define(["require", "exports", "./app", "angular", "./ComponentDecorator", "./Model/Group"], function (require, exports, app_1, angular, ComponentDecorator_1, Group_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var GroupsComponent = /** @class */ (function () {
        function GroupsComponent($exceptionHandler, scroll, modal, auth, dataService, logService, notify, notifyOkConfig, config) {
            var _this = this;
            this.$exceptionHandler = $exceptionHandler;
            this.scroll = scroll;
            this.modal = modal;
            this.auth = auth;
            this.dataService = dataService;
            this.logService = logService;
            this.notify = notify;
            this.notifyOkConfig = notifyOkConfig;
            this.config = config;
            this.ownerPlaceHolder = 'Loading users...';
            this.saving = false;
            this.loading = false;
            this.isCreateMode = false;
            this.isOwnersMode = false;
            this.isMembersMode = false;
            this.isEditGroupMode = false;
            this.scroll.yOffset = 150;
            auth.acquireToken().then(function () {
                _this.getGroup();
                return dataService.GetUsers().then(function (x) {
                    _this.loading = false;
                    _this.ownerPlaceHolder = "Search for users";
                    _this.users = x;
                });
            }).catch(function (e) { });
            this.dataGroup = new Group_1.default();
            this.groupNamePlaceholder = config.groupNamePlaceholder;
        }
        Object.defineProperty(GroupsComponent.prototype, "userCountries", {
            get: function () { return this.auth.userCountries; },
            enumerable: true,
            configurable: true
        });
        GroupsComponent.prototype.addOwners = function (users) {
            var _this = this;
            users.forEach(function (u) {
                if (!_this.dataGroup.Owners.some(function (o) { return o.Id == u.Id; })) {
                    _this.dataGroup.Owners.push(u);
                }
            });
        };
        GroupsComponent.prototype.removeOwners = function (users) {
            this.dataGroup.Owners = this.dataGroup.Owners.filter(function (o) { return !users.some(function (u) { return u.Id === o.Id; }); });
        };
        GroupsComponent.prototype.showCreateForm = function () {
            this.scroll("edit");
            this.isCreateMode = true;
            this.isMembersMode = false;
            this.isOwnersMode = false;
            this.selectedGroup = undefined;
            this.isEditGroupMode = false;
        };
        GroupsComponent.prototype.showEditForm = function (group) {
            console.log(group);
            this.selectedGroup = __assign({}, group);
            this.scroll("edit");
            this.isEditGroupMode = true;
            this.isCreateMode = false;
            this.isMembersMode = false;
            this.isOwnersMode = false;
        };
        GroupsComponent.prototype.showMembersForm = function (group) {
            this.scroll("edit");
            this.isMembersMode = true;
            this.isCreateMode = false;
            this.isOwnersMode = false;
            this.selectedGroup = group;
            this.isEditGroupMode = false;
        };
        GroupsComponent.prototype.showOwnersForm = function (group) {
            this.scroll("edit");
            this.isMembersMode = false;
            this.isCreateMode = false;
            this.isOwnersMode = true;
            this.selectedGroup = group;
            this.isEditGroupMode = false;
        };
        GroupsComponent.prototype.hideEditForm = function () {
            console.log("hideEditForm");
            this.dataGroup = new Group_1.default();
            this.isEditGroupMode = false;
        };
        GroupsComponent.prototype.hideCreateForm = function () {
            this.dataGroup = new Group_1.default();
            this.isCreateMode = false;
        };
        GroupsComponent.prototype.sendGroup = function () {
            var _this = this;
            this.saving = true;
            var name = this.dataGroup.Name;
            this.dataService.PutGroup(this.dataGroup)
                .then(function (x) {
                _this.hideCreateForm();
                _this.getGroup();
                var c = angular.copy(_this.notifyOkConfig);
                c.message = "Group saved";
                _this.notify(c);
                _this.logService.addMessage("Group " + name + " created", "Trace");
                _this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.$exceptionHandler(e);
            }).finally(function () { _this.saving = false; });
        };
        GroupsComponent.prototype.saveGroup = function () {
            var _this = this;
            this.saving = true;
            var groupId = this.selectedGroup ? this.selectedGroup.Id : "";
            var groupName = this.selectedGroup ? this.selectedGroup.Name : "";
            var groupDescription = this.selectedGroup ? this.selectedGroup.Description : "";
            var datasToUpdate = {
                Id: groupId,
                Name: groupName,
                Description: groupDescription
            };
            console.log("groupName", groupName);
            console.log("groupDescription", groupDescription);
            this.dataService.UpdateGroup(datasToUpdate)
                .then(function (x) {
                _this.getGroup();
                var c = angular.copy(_this.notifyOkConfig);
                c.message = "Group saved";
                _this.notify(c);
                _this.logService.addMessage("Group " + name + " updated", "Trace");
                _this.logService.addMessage("Operation finished successfully", "Info");
                _this.hideEditForm();
            }).catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.$exceptionHandler(e);
            }).finally(function () { _this.saving = false; });
        };
        GroupsComponent.prototype.getGroup = function () {
            var _this = this;
            this.loading = true;
            this.dataService.GetGroups().then(function (x) {
                _this.groups = x;
                _this.loading = false;
            });
        };
        GroupsComponent.prototype.isMatchFilter = function (filter) {
            return function (value) { return value != null && value.toLowerCase().indexOf(filter.toLowerCase()) > -1; };
        };
        GroupsComponent.prototype.matchUser = function (filter) {
            var _this = this;
            return function (u) {
                var isMatch = _this.isMatchFilter(filter);
                return (filter == null
                    || filter.length === 0
                    || isMatch(u.DisplayName)
                    || isMatch(u.City)
                    || isMatch(u.Department)
                    || isMatch(u.Country)
                    || isMatch(u.JobTitle)
                    || isMatch(u.Mail)
                    || isMatch(u.UserPrincipalName));
            };
        };
        GroupsComponent.prototype.tryDelete = function (group) {
            var _this = this;
            var modal = this.modal.open({
                size: 'lg',
                controller: DeleteGroupController,
                controllerAs: "ctrl",
                templateUrl: 'app/views/deleteGroup.html',
                resolve: {
                    "group": group
                }
            });
            modal.closed.catch(function () { });
            modal.result.then(function () { return _this.dataService.DeleteGroup(group); }).then(function () {
                _this.getGroup();
                var notification = angular.copy(_this.notifyOkConfig);
                notification.message = "Group \"" + group.Name + "\" has been successfully deleted";
                _this.notify(notification);
                _this.logService.addMessage("Group \"" + group.Name + "\" has been successfully deleted", "Info");
                _this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.$exceptionHandler(e);
            });
        };
        GroupsComponent.$inject = ["$exceptionHandler", "$anchorScroll", "$uibModal", "authenticationService", "dataService", "feedbackService", "notify", "notifyOkConfig", "config"];
        GroupsComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'groups', {
                controllerAs: 'ctrl',
                templateUrl: 'app/views/groups.html'
            })
        ], GroupsComponent);
        return GroupsComponent;
    }());
    exports.GroupsComponent = GroupsComponent;
    var DeleteGroupController = /** @class */ (function () {
        function DeleteGroupController($uibModalInstance, group) {
            this.$uibModalInstance = $uibModalInstance;
            this.group = group;
        }
        DeleteGroupController.prototype.cancel = function () {
            this.$uibModalInstance.dismiss();
        };
        DeleteGroupController.prototype.ok = function () {
            this.$uibModalInstance.close();
        };
        DeleteGroupController.$inject = ["$uibModalInstance", "group"];
        return DeleteGroupController;
    }());
});
//# sourceMappingURL=GroupsComponent.js.map