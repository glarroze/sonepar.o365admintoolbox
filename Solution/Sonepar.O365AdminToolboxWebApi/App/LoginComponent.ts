﻿import { app } from './app';
import Component from './ComponentDecorator';
import { IFeedbackService } from './Services/FeedbackService';
import { IAuthenticationService } from './Services/AuthenticationService';
import { ITokenStore } from './Services/TokenStore';
import { IConfig } from './interfaces/config';

@Component(app, 'login', {
    controllerAs: 'ctrl',
    templateUrl: 'app/views/login.html'
}) export class LoginComponent {
    public enableTeams: boolean = false;
    public enablePlans: boolean = false;
    public enableWorkspaces: boolean = false;

    public get unreadFeedbacks(): number {
        return this.feedbackService.unreadFeedbacks;
    }
    public log(...vals: any[]) {
        console.log("", ...vals);
    }
    public get UserName(): string {        
        return this.adal.userInfo.userName;
    }
    public get Profile(): any {
        return this.adal.userInfo.profile;
    }
    public get showLogin(): boolean {
        return !(this.adal.userInfo.isAuthenticated || this.tokenStore.token);
    }
    public get isLoggedInAad(): boolean {
        return this.adal.userInfo.isAuthenticated;
    }
    public get isLoggedIn(): boolean {
        return this.adal.userInfo.isAuthenticated && !!this.tokenStore.token;
    }
    public get isLoading(): boolean {
        return this.loading;
    }
    public get ready(): boolean {
        return !(this.loading || this.noRight);
    }
    private loading: boolean = true;
    public get noRight() {
        return this.auth.userCountries.length === 0;
    };
    public login() {
        this.adal.login();
    }
    public logout() {
        this.adal.logOut();
        this.tokenStore.clear();
    }

    private registration: {};

    constructor(
        private adal: adal.AdalAuthenticationService,
        private auth: IAuthenticationService,
        private tokenStore: ITokenStore,
        private feedbackService: IFeedbackService,
        private timeout: ng.ITimeoutService,
        private $exceptionHandler: ng.IExceptionHandlerService,
        private config: IConfig
    ) {
        this.auth.acquireToken().catch(e => {
            if (this.adal.userInfo.isAuthenticated) {
                $exceptionHandler(e);
            }
        }).finally(() => { this.loading = false });
        this.enableTeams = config.enableTeams;
        this.enablePlans = config.enablePlans;
        this.enableWorkspaces = config.enableWorkspaces;
    }
    private $onDestroy() {
        this.feedbackService.unregisterForMessages(this.registration);
    }

    public static $inject = ["adalAuthenticationService", "authenticationService", "tokenStore", "feedbackService", "$timeout", "$exceptionHandler", "config"]
}
