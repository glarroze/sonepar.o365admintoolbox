﻿import { app } from './app';
import { IAuthenticationService } from './Services/AuthenticationService';
import { IDataService } from './Services/DataService';
import Component from './ComponentDecorator';
import User from './Model/User';
import License from './Model/License';

@Component(app, 'users', {
    controllerAs: 'ctrl',
    templateUrl: 'app/views/users.html'
}) export class UserComponent {
    public users: User[] = [];
    public LicensesRef: License[] = [];
    public licencesFriendlyNames: any[] = [];
    public licencesTranslationsChanged: any[] = [];
    public languageList: string[] = [];
    public selectedUsers: User[];
    public allGraphUsers: User[];
    public loading: boolean = false;
    public inited: boolean = false;
    public isEditLicenceNamesMode: boolean = false;
    public savingLicenceNames: boolean = false;
    public isCurrentUserLicenceManager: boolean = false;
    public filter: string;
    public licensed: boolean | null = null;
    public filterFilter: ng.IFilterFilter;
    private isMatch(value: string): boolean {
        return value != null && value.toLowerCase().indexOf(this.filter.toLowerCase()) > -1;
    }
    public matchUser(u: User): boolean {
        return (this.filter == null
            || this.filter.length === 0
            || this.isMatch(u.DisplayName)
            || this.isMatch(u.City)
            || this.isMatch(u.Department)
            || this.isMatch(u.Country)
            || this.isMatch(u.JobTitle)
            || this.isMatch(u.Mail)
            || this.isMatch(u.UserPrincipalName)) && (
                this.licensed === null
            || this.licensed && u.HasLicenseAssigned
            || !this.licensed && !u.HasLicenseAssigned
            );
    }
    public formatUser(user: any) {
        return user.DisplayName + ' (' + user.GroupIds.replace(/;/g, ",") + ')'; //+ (user.AssignedLicenses.length > 0 ? '' : ' * ');
    }
    public toggleLicensed(): void {
        if (this.licensed == null) {
            this.licensed = true;
        } else if (this.licensed == true) {
            this.licensed = false;
        } else {
            this.licensed = null;
        }
    }

    constructor(
        private $filter: ng.IFilterService,
        private auth: IAuthenticationService,
        private dataService: IDataService,
        private $exceptionHandler: ng.IExceptionHandlerService

    ) { 
        
        dataService.GetLicenses().then(licences => {
            this.LicensesRef = licences;
            this.getLicencesFriendlyNames(licences);
            
            this.load(false);
        });
        dataService.IsCurrentUserLicenceManager().then(response => {
            this.isCurrentUserLicenceManager = response;
            console.log('isCurrentUserLicenceManager', response);
        })
    }
    

    public CancelEditLicences() {
        this.isEditLicenceNamesMode = false;
    }

    public translationChanged(line: any) {
        console.log(line);
        let foundLine = this.licencesTranslationsChanged.find(x => x.LicenceGuid === line.Name);
        if (!foundLine) {
            this.licencesTranslationsChanged.push({
                LicenceGuid: line.Name,
                FriendlyName: line.FriendlyName
            });
        }
        else {
            let foundIndex = this.licencesTranslationsChanged.findIndex(x => x.LicenceGuid == line.Name);
            this.licencesTranslationsChanged[foundIndex].FriendlyName = line.FriendlyName;
        }
        console.log(this.licencesTranslationsChanged);
    }

    public saveUpdatedLicences() {
        this.savingLicenceNames = true;
        this.dataService.UpdateLicencesNames(this.licencesTranslationsChanged).then(response => {
            console.log(response);
            this.savingLicenceNames = false;
            window.location.reload();
        })
    }

    private getLicencesFriendlyNames(licences: License[]) {
        let friendlyNameLicenceList: any[] = [];
        this.dataService.GetLicensesNames().then(allLicencesNames => {
            console.log(licences)
            console.log(allLicencesNames);

            licences.forEach(licence => {
                let friendlyNameLicence = allLicencesNames.filter(licenceToFind => licenceToFind.LicenceGuid === licence.Name);
                let licenceFriendlyName;
                if (friendlyNameLicence.length !== 0) {
                    licenceFriendlyName = friendlyNameLicence[0].FriendlyName;
                    }
                else {
                    licenceFriendlyName = licence.Name;
                }
                let friendlyNameList: any[] = [];
                licence.Services.forEach(serviceObject => {
                    let friendlyNameServiceToFind = allLicencesNames.filter(serviceToFind => serviceToFind.LicenceGuid === serviceObject.Service.Name);
                    let serviceFriendlyName;
                    if (friendlyNameServiceToFind.length !== 0) {
                        serviceFriendlyName = friendlyNameServiceToFind[0].FriendlyName;
                    }
                    else {
                        serviceFriendlyName = serviceObject.Service.Name;
                    }
                    friendlyNameList.push({
                        Name: serviceObject.Service.Name,
                        FriendlyName: serviceFriendlyName
                    });
                });
                friendlyNameLicenceList.push({
                    Name: licence.Name,
                    FriendlyName: licenceFriendlyName,
                    services: friendlyNameList
                });
            });
        })
        console.log(friendlyNameLicenceList);
        this.licencesFriendlyNames = friendlyNameLicenceList;
    }

    private load(refresh:boolean = true) {
        return this.auth.acquireToken().then(() => {
            this.loading = true;
            this.users = [];
        }).then(() =>
            this.dataService.GetUsers(refresh).then(x => {
                console.log(x);
                this.users = x;
                this.loading = false;
                this.inited = true;
            })        

            // this.dataService.GetUsers().then(users => console.log(users)));
            // this.dataService.GetUserGroups(false).then(groups => {
            //    console.log(groups);
                
            //    groups.forEach(async group => {
            //        this.loading = true;
            //        // this.getGroupUsers(group, "");

            //        this.dataService.GetUsers().then(users => console.log(users));
            //        let usersToConcat:User[] = []
            //        await this.dataService.GetGroupAllMembers(group.id).then(users => {
            //            console.log(users);
                        
            //            users.forEach(user => {
            //                if (this.users.some(presentUser => presentUser.UserPrincipalName === user.UserPrincipalName)) {
            //                    console.log("existing !");
            //                    user.DisplayName = user.DisplayName.replace(')', ` ${group.displayName})`);

            //                    let objIndex = this.users.findIndex((u => u.UserPrincipalName === user.UserPrincipalName));

            //                    this.users[objIndex].DisplayName = this.users[objIndex].DisplayName.replace(')', `, ${group.displayName.substring(4, 6)})`);                                
            //                }
            //                else {
            //                    usersToConcat.push({ ...user, DisplayName: `${user.DisplayName} (${group.displayName.substring(4, 6)})` });
            //                }
            //            })
            //            this.users = this.users.concat(usersToConcat);
            //            this.loading = false;
            //        });
            //    })
            //})
        ).catch(e => this.$exceptionHandler(e))
    }

    private getGroupUsers(group: any, skipToken: string) {
        console.log(group);
        this.loading = true;
        let payload = {
            groupId: group.id,
            groupDisplayName: group.displayName,
            skipToken: skipToken
        }
        this.dataService.GetGroupMembersByPage(payload).then((pagedUsersResponse: any) => {
            console.log(pagedUsersResponse);            
            const result = [];
            const map = new Map();
            for (const item of this.users) {
                if (!map.has(item.UserPrincipalName)) {
                    map.set(item.UserPrincipalName, true);
                    result.push(item);
                }
            }

            this.users = result;
            this.users.sort(function (a, b) {
                if (a.DisplayName < b.DisplayName) { return -1; }
                if (a.DisplayName > b.DisplayName) { return 1; }
                return 0;
            });

            if (pagedUsersResponse.skipToken !== "") {
                this.getGroupUsers(group, pagedUsersResponse.skipToken);
            }
            else {
                // this.loading = false;
            }
        });
    }


    public editLicences() {
        this.isEditLicenceNamesMode = true;
        this.selectedUsers = [];
        console.log(this.selectedUsers);
    }

    public onUserSelection() {
        this.isEditLicenceNamesMode = false;
    }


    public refresh() {
        this.load(true);
    }

    public static $inject = ["$filter", "authenticationService", "dataService", "$exceptionHandler"];
}