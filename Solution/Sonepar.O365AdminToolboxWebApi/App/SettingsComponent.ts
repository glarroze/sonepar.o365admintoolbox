﻿import { app } from './app';
import * as angular from 'angular';
import { INotifyOptions } from 'interfaces/INotifyOptions';
import { IAuthenticationService } from './Services/AuthenticationService';
import { IDataService } from './Services/DataService';
import { IFeedbackService, Message } from './Services/FeedbackService';
import Country from './Model/Country';
import Component from './ComponentDecorator';
import User from './Model/User';
import License from './Model/License';
import { LicenseAssignment } from './Model/License';
import Group from './Model/Group';
import OfficeObject from './Model/OfficeObject';

@Component(app, 'settings', {
    controllerAs: 'ctrl',
    bindings: { },
    templateUrl: 'app/views/Settings.html'
}) export class SettingsComponent {
    public messages: Message[];
    public loading: boolean = false;
    public groups: Group[] = [];
    public workspaces: OfficeObject[];

    public static $inject = [
        "$q",
        "$exceptionHandler",
        "authenticationService",
        "dataService",
        "feedbackService",
        "notify", "notifyOkConfig"];

    constructor(private $q: ng.IQService,
        private $exceptionHandler: ng.IExceptionHandlerService,
        private auth: IAuthenticationService,
        private dataService: IDataService,
        private logservice: IFeedbackService,
        private notify: ng.cgNotify.INotifyService,
        private notifyOkConfig: INotifyOptions) {
        auth.acquireToken().then(() => {
            this.dataService.GetUnregisteredGroups().then(x => {
                console.log(x);
                this.groups = x;
                this.loading = false;
            });

            this.dataService.GetUnregisteredWorkspaces().then(ws => {
                this.workspaces = ws;
                console.log(ws);
            })
        }).catch(e => { });
    }
}
