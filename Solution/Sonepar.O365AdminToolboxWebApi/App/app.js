define(["require", "exports", "angular", "angular", "adal"], function (require, exports, angular_1, angular) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    String.prototype.startsWith = String.prototype.startsWith || function (x) { return this.indexOf(x) == 0; };
    exports.app = angular_1.module('app', [
        'appConfig',
        'ui.router',
        'ui.toggle',
        'ui.bootstrap',
        'ui.select',
        'ngAnimate',
        'cgNotify',
        'AdalAngular'
    ]);
    exports.app.config([
        "$locationProvider",
        "$httpProvider",
        "$stateProvider",
        "$urlRouterProvider",
        "adalAuthenticationServiceProvider",
        "configProvider",
        "$animateProvider",
        "appConfig",
        function ($locationProvider, $httpProvider, $stateProvider, $urlRouterProvider, adalAuthenticationServiceProvider, configProvider, $animateProvider, appConfig) {
            var config = appConfig;
            $animateProvider.classNameFilter(/^((?!(no-animate)).*)*$/);
            adalAuthenticationServiceProvider.init({
                clientId: config.clientId,
                tenant: config.tenant,
                endpoints: config.endpoints
            });
            function needConnectedUser(auth) {
                return auth.acquireToken().then(function () { return auth.userCountries.length > 0; }).catch(function () { return false; });
            }
            needConnectedUser.$inject = ['authenticationService'];
            var needUser = { _user: needConnectedUser };
            $stateProvider.state({
                name: 'users',
                url: '/menu/users',
                template: '<users></users>',
                resolve: needUser
            }).state({
                name: 'groups',
                url: '/menu/groups',
                template: '<groups></groups>',
                resolve: needUser
            }).state({
                name: 'teams',
                url: '/menu/teams',
                template: '<teams></teams>',
                resolve: needUser
            }).state({
                name: 'plans',
                url: '/menu/plans',
                template: '<plans></plans>',
                resolve: needUser
            }).state({
                name: 'workspaces',
                url: '/menu/workspaces',
                template: '<workspaces></workspaces>',
                resolve: needUser
            }).state({
                name: 'logs',
                url: '/menu/logs',
                template: '<feedback></feedback>',
                resolve: needUser
            }).state({
                name: 'settings',
                url: '/menu/settings',
                template: '<settings></settings>',
                resolve: needUser
            });
            $urlRouterProvider.otherwise('/menu/users');
            if ($httpProvider && $httpProvider.interceptors) {
                $httpProvider.interceptors.push('MyProtectedResourceInterceptor');
                $httpProvider.interceptors.push('AppApiInterceptor');
            }
            $locationProvider.html5Mode({
                requireBase: true,
                enabled: true,
            }).hashPrefix('!');
        }
    ]);
    exports.app.factory('$exceptionHandler', ['$log', '$injector', function ($log, $injector) {
            return function myExceptionHandler(exception, cause) {
                console.log(exception, cause);
                var notify = $injector.get('notify');
                var notifyOptions = angular.extend({}, $injector.get('notifyErrorConfig'));
                var e = (exception && exception.data || exception);
                if (e) {
                    notifyOptions.message = e.ExceptionMessage || e.Message || e;
                }
                if (cause) {
                    notifyOptions.message += (notifyOptions.message ? "," : "") + cause;
                }
                if (notifyOptions.message) {
                    if (notifyOptions.message.toLowerCase().indexOf("Transition Superseded") >= 0) {
                        return;
                    }
                    notify(notifyOptions);
                }
                $log.warn(exception, cause);
            };
        }]);
    exports.app.factory('MyProtectedResourceInterceptor', ['ProtectedResourceInterceptor', 'config', '$q', function (interceptor, myconfig, $q) {
            return {
                request: function (config) {
                    if (config.url.startsWith(myconfig.BaseServiceUrl + 'api/authenticat')) {
                        return interceptor.request && interceptor.request(config) || config;
                    }
                    return config;
                },
                responseError: function (rejection) {
                    return rejection && (interceptor.responseError && interceptor.responseError(rejection) || $q.reject(rejection));
                }
            };
        }]).constant('notifyErrorConfig', {
        duration: 5000,
        classes: 'alert-danger',
        position: 'center'
    }).constant('notifyOkConfig', {
        duration: 2500,
        classes: 'alert-success',
        position: 'center'
    });
});
//# sourceMappingURL=app.js.map