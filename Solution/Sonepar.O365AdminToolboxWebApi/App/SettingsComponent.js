var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define(["require", "exports", "./app", "./ComponentDecorator"], function (require, exports, app_1, ComponentDecorator_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SettingsComponent = /** @class */ (function () {
        function SettingsComponent($q, $exceptionHandler, auth, dataService, logservice, notify, notifyOkConfig) {
            var _this = this;
            this.$q = $q;
            this.$exceptionHandler = $exceptionHandler;
            this.auth = auth;
            this.dataService = dataService;
            this.logservice = logservice;
            this.notify = notify;
            this.notifyOkConfig = notifyOkConfig;
            this.loading = false;
            this.groups = [];
            auth.acquireToken().then(function () {
                _this.dataService.GetUnregisteredGroups().then(function (x) {
                    console.log(x);
                    _this.groups = x;
                    _this.loading = false;
                });
                _this.dataService.GetUnregisteredWorkspaces().then(function (ws) {
                    _this.workspaces = ws;
                    console.log(ws);
                });
            }).catch(function (e) { });
        }
        SettingsComponent.$inject = [
            "$q",
            "$exceptionHandler",
            "authenticationService",
            "dataService",
            "feedbackService",
            "notify", "notifyOkConfig"
        ];
        SettingsComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'settings', {
                controllerAs: 'ctrl',
                bindings: {},
                templateUrl: 'app/views/Settings.html'
            })
        ], SettingsComponent);
        return SettingsComponent;
    }());
    exports.SettingsComponent = SettingsComponent;
});
//# sourceMappingURL=SettingsComponent.js.map