﻿import { app } from '../app';

export interface IConfigProvider extends ng.IServiceProvider {
    $get(appConfig: IConfig): IConfig;
    //default: IConfig;
}

export interface IConfig {
    BaseServiceUrl: string;
    ApiBaseUrl: string;
    clientId: string;
    tenant: string;
    appId: string;

    endpoints: { [uri: string]: string };
    cacheExpiration: number;
    sharePointBaseUrl: string;

    enableTeams: boolean;
    enablePlans: boolean;
    enableWorkspaces: boolean;

    groupNamePlaceholder: string;
    groupNameRegex: string;
    groupNameRegexErrorMessage: string;

    teamNamePlaceholder: string;
    teamNameRegex: string;
    teamNameRegexErrorMessage: string;

    planNamePlaceholder: string;
    planNameRegex: string;
    planNameRegexErrorMessage: string;

    workspaceNamePlaceholder: string;
    workspaceNameRegex: string;
    workspaceNameRegexErrorMessage: string;
}

