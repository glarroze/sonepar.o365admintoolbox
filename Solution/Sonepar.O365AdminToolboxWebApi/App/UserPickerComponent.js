var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define(["require", "exports", "./app", "angular", "./ComponentDecorator"], function (require, exports, app_1, angular, ComponentDecorator_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var UserPickerComponent = /** @class */ (function () {
        function UserPickerComponent(auth, dataService, notify, notifyOkConfig) {
            var _this = this;
            this.auth = auth;
            this.dataService = dataService;
            this.notify = notify;
            this.notifyOkConfig = notifyOkConfig;
            this.pickerSize = 8;
            this.ownerPlaceHolder = 'Loading users...';
            this.loading = false;
            this.withLicense = false;
            auth.acquireToken().then(function () {
                return dataService.GetUsers().then(function (x) {
                    _this.loading = false;
                    _this.ownerPlaceHolder = "Search for users";
                    _this.users = x;
                });
            }).catch(function (e) { });
            this.$onInit = this.onInit.bind(this);
        }
        Object.defineProperty(UserPickerComponent.prototype, "userCountries", {
            get: function () { return this.auth.userCountries; },
            enumerable: true,
            configurable: true
        });
        UserPickerComponent.prototype.addOwners = function (users) {
            var _this = this;
            (users || []).forEach(function (u) {
                if (!_this.selection.some(function (o) { return o.Id == u.Id; })) {
                    _this.selection.push(u);
                }
            });
            this.onChange();
        };
        UserPickerComponent.prototype.removeOwners = function (users) {
            this.selection = this.selection.filter(function (o) { return !(users || []).some(function (u) { return u.Id === o.Id; }); });
            this.onChange();
        };
        UserPickerComponent.prototype.isMatchFilter = function (filter) {
            return function (value) { return value != null && value.toLowerCase().indexOf(filter.toLowerCase()) > -1; };
        };
        UserPickerComponent.prototype.matchUser = function (filter) {
            var _this = this;
            return function (u) {
                var isMatch = _this.isMatchFilter(filter);
                var res = (!_this.withLicense || u.HasLicenseAssigned);
                res = res &&
                    (filter == null
                        || filter.length === 0
                        || isMatch(u.DisplayName)
                        || isMatch(u.City)
                        || isMatch(u.Department)
                        || isMatch(u.Country)
                        || isMatch(u.JobTitle)
                        || isMatch(u.Mail)
                        || isMatch(u.UserPrincipalName));
                return res;
            };
        };
        UserPickerComponent.prototype.onInit = function () {
            var _this = this;
            this.model.$render = function () {
                _this.selection = _this.model.$viewValue;
            };
            this.model.$isEmpty = function (value) {
                return !(value && angular.isArray(value) && value.length > 0);
            };
        };
        ;
        UserPickerComponent.prototype.onChange = function () {
            this.model.$setValidity("required", true);
            this.model.$setViewValue(this.selection);
            this.model.$commitViewValue();
        };
        UserPickerComponent.$inject = ["authenticationService", "dataService", "notify", "notifyOkConfig"];
        UserPickerComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'userPicker', {
                controllerAs: 'ctrl',
                bindings: {
                    'withLicense': '<',
                    'pickerSize': '<size'
                },
                require: {
                    model: 'ngModel'
                },
                templateUrl: 'app/views/userpicker.html'
            })
        ], UserPickerComponent);
        return UserPickerComponent;
    }());
    exports.UserPickerComponent = UserPickerComponent;
});
//# sourceMappingURL=UserPickerComponent.js.map