﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Newtonsoft.Json.Linq;
using Microsoft.ApplicationInsights;

namespace Sonepar.O365AdminToolboxWebApi.Filters
{
    /// <summary>
    /// Log actions
    /// </summary>
    /// <seealso cref="System.Web.Http.Filters.ActionFilterAttribute" />
    /// <seealso cref="System.Web.Http.Filters.IActionFilter" />
    public class LogActionFilterAttribute : ActionFilterAttribute
    {
        private static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Initializes a new instance of the <see cref="LogActionFilterAttribute"/> class.
        /// </summary>
        public LogActionFilterAttribute()
        {
        }

        /// <summary>
        /// Occurs before the action method is invoked.
        /// </summary>
        /// <param name="actionContext">The action context.</param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string message = $"Executing Action : {actionContext.ControllerContext.ControllerDescriptor.ControllerName}.{actionContext.ActionDescriptor.ActionName} by {actionContext.RequestContext.Principal.Identity.Name}";
            Log(actionContext, message);
            base.OnActionExecuting(actionContext);
        }

        private static string Dump(HttpActionContext actionContext)
        {
            return JObject.FromObject(actionContext.ActionArguments).ToString(formatting: Newtonsoft.Json.Formatting.None);
        }

        private void Log(HttpActionContext actioncontext, string message, Exception e = null)
        {
            TelemetryClient telemetry = new TelemetryClient();
            JObject data = JObject.FromObject(actioncontext.ActionArguments);
            Dictionary<string, object> dic = MakeDictFromClaims((actioncontext.RequestContext.Principal.Identity as ClaimsIdentity).Claims);
            data["_Operator"] = JObject.FromObject(dic);
            data["_ActionName"] = $"{actioncontext.ControllerContext.ControllerDescriptor.ControllerName}.{actioncontext.ActionDescriptor.ActionName}";
            if (e != null)
            {
                telemetry.TrackException(e, data.Properties().ToDictionary(x => x.Name, x => x.Value.ToString(Newtonsoft.Json.Formatting.Indented)));
                AddException(e, data);
            }
            telemetry.TrackEvent(message, data.Properties().ToDictionary(x => x.Name, x => x.Value.ToString(Newtonsoft.Json.Formatting.Indented)));
        }

        private static Dictionary<string, object> MakeDictFromClaims(IEnumerable<Claim> claims)
        {
            var dic = new Dictionary<string, object>();
            claims.ToList().ForEach(x =>
            {
                if (dic.ContainsKey(x.Type))
                {
                    if (dic[x.Type] is List<object>)
                    {
                        ((List<object>)dic[x.Type]).Add(x.Value);
                    }
                    else
                    {
                        dic[x.Type] = new List<object> { dic[x.Type], x.Value };
                    }
                }
                else
                {
                    dic.Add(x.Type, x.Value);
                }
            });
            return dic;
        }

        private static void AddException(Exception e, JObject data)
        {
            try
            {
                var exc = JObject.FromObject(e);
                if (e is Microsoft.Graph.ServiceException)
                {
                    try
                    {
                        exc["Error"] = JObject.FromObject(((Microsoft.Graph.ServiceException)e).Error);
                    }
                    catch { }
                }
                data["_Exception"] = exc;
            }
            catch { }
        }

        /// <summary>
        /// Occurs after the action method is invoked.
        /// </summary>
        /// <param name="actionExecutedContext">The action executed context.</param>
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext != null)
            {
                var ctx = actionExecutedContext.ActionContext;
                if (ctx != null)
                {
                    string outcome = actionExecutedContext.Response?.ReasonPhrase;
                    if (actionExecutedContext.Exception != null)
                    {
                        outcome = $"Error : {actionExecutedContext.Exception.Message}";
                    }
                    string message = $"Executed Action : {ctx.ControllerContext.ControllerDescriptor.ControllerName}.{ctx.ActionDescriptor.ActionName} : {outcome}";
                    Log(ctx, message, actionExecutedContext.Exception);
                }
            }
            base.OnActionExecuted(actionExecutedContext);
        }
    }
}
