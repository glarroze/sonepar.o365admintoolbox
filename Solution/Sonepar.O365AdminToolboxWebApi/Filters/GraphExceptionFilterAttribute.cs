﻿using System;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Net;

namespace Sonepar.O365AdminToolboxWebApi.Filters
{
    /// <summary>
    /// Decapsulate exception messages from graph API
    /// </summary>
    /// <seealso cref="System.Web.Http.Filters.IActionFilter" />
    public class GraphExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private Func<Exception, string> MakeMessage = null;
        /// <summary>
        /// Initializes a new instance of the <see cref="GraphExceptionFilterAttribute"/> class.
        /// </summary>
        /// <param name="makeMessage">The exception handler.</param>
        public GraphExceptionFilterAttribute(Func<Exception, string> makeMessage) {
            MakeMessage = makeMessage;
        }

        /// <summary>
        /// Called On Exception.
        /// </summary>
        /// <param name="context">The context.</param>
        public override void OnException(HttpActionExecutedContext context)
        {

            base.OnException(context);

            if (context != null)
            {
                Exception exceptionToBeThrown = context.Exception;
                var code = HttpStatusCode.InternalServerError;
                var message = MakeMessage(exceptionToBeThrown);
                if (exceptionToBeThrown is UnauthorizedAccessException)
                {
                    code = HttpStatusCode.Forbidden;
                }
                context.Response = new HttpResponseMessage(code)
                {
                    Content = new StringContent(message),
                    ReasonPhrase = message.Replace('\n', ',').Replace("\r", "")
                };
            }
        }

    }
}
