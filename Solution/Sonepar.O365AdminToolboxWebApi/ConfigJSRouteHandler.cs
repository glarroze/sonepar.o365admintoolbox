﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sonepar.O365AdminToolbox.Domain;
using System.Web;
using System.Web.Routing;

namespace Sonepar.O365AdminToolboxWebApi
{
    internal class ConfigJSRouteHandler : IRouteHandler
    {
        private IAppSettings settings;

        public ConfigJSRouteHandler(IAppSettings settings)
        {
            this.settings = settings;
        }

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new ConfigJSHandler(settings);
        }

        private class ConfigJSHandler : IHttpHandler
        {
            public ConfigJSHandler(IAppSettings settings)
            {
                this.settings = settings;
            }
            private IAppSettings settings;
            public bool IsReusable
            {
                get
                {
                    return true;
                }
            }

            public void ProcessRequest(HttpContext context)
            {
                var settingsObject = new
                {
                    BaseServiceUrl = settings.AppBaseUrl,
                    ApiBaseUrl = settings.AppBaseUrl + "api/",
                    clientId = settings.ClientId,
                    tenant = settings.TenantName,
                    appId = settings.Audience,

                    endpoints = new object(),
                    cacheExpiration = settings.CacheExpiration,
                    sharePointBaseUrl = settings.SharePointBaseUrl,

                    enableTeams = settings.EnableTeams,
                    enablePlans = settings.EnablePlans,
                    enableWorkspaces = settings.EnableWorkspaces,

                    groupNamePlaceholder = settings.GroupNamePlaceholder,
                    groupNameRegex = settings.GroupNameRegex,
                    groupNameRegexErrorMessage = settings.GroupNameRegexErrorMessage,

                    teamNamePlaceholder = settings.TeamNamePlaceholder,
                    teamNameRegex = settings.TeamNameRegex,
                    teamNameRegexErrorMessage = settings.TeamNameRegexErrorMessage,

                    planNamePlaceholder = settings.PlanNamePlaceholder,
                    planNameRegex = settings.PlanNameRegex,
                    planNameRegexErrorMessage = settings.PlanNameRegexErrorMessage,

                    workspaceNamePlaceholder = settings.WorkspaceNamePlaceholder,
                    workspaceNameRegex = settings.WorkspaceNameRegex,
                    workspaceNameRegexErrorMessage = settings.WorkspaceNameRegexErrorMessage
                };
                var json = JObject.FromObject(settingsObject).ToString(Formatting.None);

                var response = context.Response;
                response.Headers["Content-Type"] = "application/javascript";
                response.Write($@"angular.module('appConfig',[]).constant('appConfig',{json});");
            }
        }
    }
}