﻿using Microsoft.Owin.Security;
using Sonepar.O365AdminToolboxWebApi.Tools;
using System;
using System.IdentityModel.Tokens;

namespace Sonepar.O365AdminToolboxWebApi.Authentication
{
    public class JsonWebTokenFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly AppSettings appSettings;

        public JsonWebTokenFormat(AppSettings settings)
        {
            appSettings = settings;
        }

        public string Protect(AuthenticationTicket data)
        {
            DateTime notBefore = DateTime.UtcNow;
            DateTime expires = notBefore + TimeSpan.FromHours(24); //validity timer.

            var cert = new CertificateStore().GetCert();
            SigningCredentials cred = new X509SigningCredentials(cert); // your signing credentials.
            JwtHeader header = new JwtHeader(cred);
            //header.Add("x5t", ConfigurationManager.AppSettings["JWT_CERT"]);
            JwtPayload payload = new JwtPayload(appSettings.Issuer, appSettings.Audience, data.Identity.Claims, notBefore, expires);
            //payload.Add("x5t", ConfigurationManager.AppSettings["JWT_CERT"]);

            var jwtToken = new JwtSecurityToken(header, payload);
            var handler = new JwtSecurityTokenHandler();
            var jwt = handler.WriteToken(jwtToken);
            return jwt;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}