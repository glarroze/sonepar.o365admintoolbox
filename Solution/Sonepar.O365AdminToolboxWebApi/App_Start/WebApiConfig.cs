﻿using Newtonsoft.Json;
using System;
using System.Web.Http;
using System.Web.OData.Extensions;
using System.Web.OData.Builder;
using Sonepar.O365AdminToolbox.Domain;
using Sonepar.O365AdminToolboxWebApi.Filters;

namespace Sonepar.O365AdminToolboxWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config, Func<Type,Object> factory)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            var s = config.Formatters.JsonFormatter.SerializerSettings;
            s.DateTimeZoneHandling = DateTimeZoneHandling.Unspecified;
            s.NullValueHandling = NullValueHandling.Include;
            s.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            s.PreserveReferencesHandling = PreserveReferencesHandling.None;
            s.MissingMemberHandling = MissingMemberHandling.Ignore; 

            // New code:
            ODataModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<User>("User");
            config.MapODataServiceRoute(
                routeName: "ODataRoute",
                routePrefix: "odata",
                model: builder.GetEdmModel());

            config.Routes.MapHttpRoute(
                            name: "DefaultApi",
                            routeTemplate: "api/{controller}/{id}",
                            defaults: new { id = RouteParameter.Optional }

                );

            config.Filters.Add(new LogActionFilterAttribute());
            config.Filters.Add(new GraphExceptionFilterAttribute(((IExceptionHandler)factory(typeof(IExceptionHandler))).MakeMessage));
        }
    }
}
