using Microsoft.Practices.Unity;
using Sonepar.O365AdminToolbox.Domain;
using Sonepar.O365AdminToolbox.Entity;
using Sonepar.O365AdminToolbox.GraphPort;
using Sonepar.O365AdminToolboxWebApi.Authentication;
using Sonepar.O365AdminToolboxWebApi.Tools;
using System.Web.Http;
using Unity.WebApi;

namespace Sonepar.O365AdminToolboxWebApi
{
    public static class UnityConfig
    {
        public static UnityContainer RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<CertificateStore, CertificateStore>();
            container.RegisterType<IAdminCredentials, AdminCredentials>();
            container.RegisterType<IAppSettings, AppSettings>();
            container.RegisterType<IGlobalSettings, GlobalSettings>();
            container.RegisterType<IAuthenticationService,AadAuthenticationService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IGroupService, GroupService>();
            container.RegisterType<ITeamService, TeamService>();
            container.RegisterType<IPlanService, PlanService>();
            container.RegisterType<IWorkspaceService, WorkspaceService>();
            container.RegisterType<ILicenseService, LicenseService>();
            container.RegisterType<ICertificateStore, CertificateStore>();
            container.RegisterType<IFriendlyNameService, FriendlyNameService>();
            container.RegisterType<IExceptionHandler, ExceptionHandler>();
            container.RegisterType<ToolboxDbContext, ToolboxDbContext>();
            container.RegisterType<JsonWebTokenFormat>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
            LogTelemetryInitializer.AppSettingsFactory = () => container.Resolve<IAppSettings>();
            return container;
        }
    }
}