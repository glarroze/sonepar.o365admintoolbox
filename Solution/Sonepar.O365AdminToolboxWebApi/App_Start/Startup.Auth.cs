﻿using System;
using System.Configuration;
using System.IdentityModel.Tokens;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.ActiveDirectory;
using Owin;
using System.IdentityModel.Selectors;
using System.Security.Cryptography.X509Certificates;
using System.Collections.ObjectModel;
using System.Security.Claims;
using Sonepar.O365AdminToolboxWebApi.Tools;

namespace Sonepar.O365AdminToolboxWebApi
{
    public partial class Startup
    {
        private X509Certificate2 cert = new CertificateStore().GetCert();

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationType = "HS256",
                    TokenValidationParameters = new TokenValidationParameters
                    {
                        CertificateValidator = new Validator(),
                        //TODO Uncomment this line and delete the other IssuerSigningKey = (cert != null) ? new X509SigningCredentials(cert).SigningKey : null,
                        IssuerSigningKey = new X509SigningCredentials(cert).SigningKey,
                        // IssuerSigningKey = (cert != null) ? new X509SigningCredentials(cert).SigningKey : null,
                        RequireSignedTokens = true,
                        ValidateIssuerSigningKey = false,
                        ValidIssuer = ConfigurationManager.AppSettings["Issuer"],
                        ValidAudiences = new string[] { ConfigurationManager.AppSettings["ida:Audience"] }
                    }
                }
            );
            app.UseWindowsAzureActiveDirectoryBearerAuthentication(
                new WindowsAzureActiveDirectoryBearerAuthenticationOptions
                {
                    Tenant = ConfigurationManager.AppSettings["ida:Tenant"],
                    TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidAudience = ConfigurationManager.AppSettings["ida:Audience"]
                    }
                });

            app.UseWindowsAzureActiveDirectoryBearerAuthentication(
                new WindowsAzureActiveDirectoryBearerAuthenticationOptions
                {
                    Tenant = ConfigurationManager.AppSettings["ida:Tenant"],
                    TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidAudience = ConfigurationManager.AppSettings["ida:ClientId"]
                    }
                });


        }
    }

    internal class myHandler : JwtSecurityTokenHandler {
        public override ReadOnlyCollection<ClaimsIdentity> ValidateToken(SecurityToken token)
        {
            return base.ValidateToken(token);
        }
    }

    public class Validator : X509CertificateValidator {
        public override void Validate(X509Certificate2 certificate)
        {
            Console.WriteLine(certificate.ToString());
        }
    }
}
