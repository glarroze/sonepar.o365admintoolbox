﻿using Sonepar.O365AdminToolbox.Domain;
using System;
using System.Configuration;

namespace Sonepar.O365AdminToolboxWebApi
{
    public class AppSettings : IAppSettings
    {
        public AppSettings(GlobalSettings settings)
        {
            AuthString = settings.AuthString + "common/";
        }
        public string TenantId => ConfigurationManager.AppSettings["ida:TenantId"];
        public string TenantName => ConfigurationManager.AppSettings["ida:Tenant"];
        public string ClientId => ConfigurationManager.AppSettings["ida:ClientId"];
        public string ClientIdAdmin => ConfigurationManager.AppSettings["ClientIdAdmin"];
        public Uri AppRedirectUrl => new Uri(ConfigurationManager.AppSettings["redirectUrl"]);
        public string AppBaseUrl => ConfigurationManager.AppSettings["appBaseUrl"];
        public string AuthString { get; private set; }
        public string Issuer => ConfigurationManager.AppSettings["Issuer"];
        public string AadIssuer => ConfigurationManager.AppSettings["AadIssuer"];
        public string Audience => ConfigurationManager.AppSettings["ida:Audience"];
        public string ManagedGroupsRegEx => ConfigurationManager.AppSettings["AdGroupRegExp"];
        public string ManagedGroupsReplace => ConfigurationManager.AppSettings["AdGroupReplace"];

        public string AdminId => ConfigurationManager.AppSettings["AdminId"];
        public string AdminLogin => ConfigurationManager.AppSettings["AdminLogin"];
        public string AdminPassword => ConfigurationManager.AppSettings["AdminPassword"];
        public string MembersByRequest => ConfigurationManager.AppSettings["MembersByRequest"];
        public string LicencesNamesEditorGroup => ConfigurationManager.AppSettings["LicencesNamesEditorGroup"];

        public string SPOSignOutFunctionUrl => ConfigurationManager.AppSettings["SPOSignOutFunctionUrl"];
        public string GetMFAStatusFunctionUrl => ConfigurationManager.AppSettings["GetMFAStatusFunctionUrl"];
        public string ChangeMFAStatusFunctionUrl => ConfigurationManager.AppSettings["ChangeMFAStatusFunctionUrl"];

        public string GroupCountryExtendedPropertyName => ConfigurationManager.AppSettings["GroupCountryExtendedPropertyName"];
        public string AppInsightId => ConfigurationManager.AppSettings["AppInsightId"];
        public string AppInsightAppId => ConfigurationManager.AppSettings["AppInsightAppId"] ?? "AdminToolBox";

        public int CacheExpiration => Int32.Parse(ConfigurationManager.AppSettings["CacheExpiration"]);
        public int RetryCount => Int32.Parse(ConfigurationManager.AppSettings["RetryCount"]);
        public string SharePointRoot => ConfigurationManager.AppSettings["SharePointRoot"];
        public string SharePointBaseUrl => ConfigurationManager.AppSettings["SharePointBaseUrl"];

        public bool EnableTeams => Boolean.Parse(ConfigurationManager.AppSettings["EnableTeams"]);
        public bool EnablePlans => Boolean.Parse(ConfigurationManager.AppSettings["EnablePlans"]);
        public bool EnableWorkspaces => Boolean.Parse(ConfigurationManager.AppSettings["EnableWorkspaces"]);

        public string GroupNamePlaceholder => ConfigurationManager.AppSettings["GroupNamePlaceholder"];
        public string GroupNameRegex => ConfigurationManager.AppSettings["GroupNameRegex"];
        public string GroupNameRegexErrorMessage => ConfigurationManager.AppSettings["GroupNameRegexErrorMessage"];

        public string TeamNamePlaceholder => ConfigurationManager.AppSettings["TeamNamePlaceholder"];
        public string TeamNameRegex => ConfigurationManager.AppSettings["TeamNameRegex"];
        public string TeamNameRegexErrorMessage => ConfigurationManager.AppSettings["TeamNameRegexErrorMessage"];

        public string PlanNamePlaceholder => ConfigurationManager.AppSettings["PlanNamePlaceholder"];
        public string PlanNameRegex => ConfigurationManager.AppSettings["PlanNameRegex"];
        public string PlanNameRegexErrorMessage => ConfigurationManager.AppSettings["PlanNameRegexErrorMessage"];

        public string WorkspaceNamePlaceholder => ConfigurationManager.AppSettings["WorkspaceNamePlaceholder"];
        public string WorkspaceNameRegex => ConfigurationManager.AppSettings["WorkspaceNameRegex"];
        public string WorkspaceNameRegexErrorMessage => ConfigurationManager.AppSettings["WorkspaceNameRegexErrorMessage"];
    }

    public class GlobalSettings : IGlobalSettings
    {
        public string AuthString => "https://login.microsoftonline.com/";
        public string AadResourceUrl => "https://graph.windows.net";
        public string GraphResourceUrl => "https://graph.microsoft.com";
        public string GraphServiceObjectId => "00000002-0000-0000-c000-000000000000";
    }
}