﻿/// <binding BeforeBuild='default' />
///
// include plug-ins
var gulp = require('gulp');
var gutil = require('gulp-util');
var less = require('gulp-less');
var merge = require('merge-stream');
var debug = require('gulp-debug');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var del = require('del');
var minifyCSS = require('gulp-minify-css');
var copy = require('gulp-copy');
var bower = require('gulp-bower');
var sourcemaps = require('gulp-sourcemaps');
var rjs = require('gulp-requirejs');

var config = {
    appJs: ['app/**/*.js'],
    allJs: [
        'main.js',
        'app/**/*.js',
    ],
    appBundle: 'Scripts/app.js',

    adalJs: ['bower_components/adal-angular/lib/*.js'],
    momentJs: ['bower_components/moment/moment.js'],
    momentBundle: 'Scripts/moment.min.js',

    reqSrc: ['bower_components/requirejs/require.js'],
    reqBundle: 'Scripts/require.min.js',


    ngSrc: [
        'bower_components/angular/angular.js',
        'bower_components/angular-animate/angular-animate.js',
        'bower_components/angular-ui-select/dist/select.js',
        'scripts/angular-bootstrap-toggle.js',
        'bower_components/angular-ui-bootstrap-bower/ui-bootstrap.js',
        'bower_components/angular-ui-bootstrap-bower/ui-bootstrap-tpls.js',
        'bower_components/angular-notify/dist/angular-notify.js',
        'bower_components/angular-ui-router/release/angular-ui-router.js'
    ],
    ngbundle: 'Scripts/ng.min.js',
    //JavaScript files that will be combined into a jquery bundle
    jquerysrc: [
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/jquery-validation/dist/jquery.validate.min.js',
        'bower_components/jquery-validation-unobtrusive/jquery.validate.unobtrusive.min.js'
    ],
    jquerybundle: 'Scripts/jquery-bundle.min.js',

    //JavaScript files that will be combined into a Bootstrap bundle
    bootstrapsrc: [
        'bower_components/bootstrap/dist/js/bootstrap.min.js'
    ],
    bootstrapbundle: 'Scripts/bootstrap-bundle.min.js',

    //Modernizr
    modernizrsrc: ['bower_components/modernizr/modernizr.js'],
    modernizrbundle: 'Scripts/modernizer.min.js',

    //Bootstrap CSS and Fonts
    bootstrapcss: [
        'bower_components/angular-notify/dist/angular-notify.css',
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'bower_components/angular-ui-select/dist/select.css',
        'styles/angular-bootstrap-toggle.css'
    ],
    boostrapfonts: 'bower_components/bootstrap/dist/fonts/*.*',

    appless: 'Content/Site.less',
    fontsout: 'Content/dist/fonts',
    cssout: 'Content/dist/css',
    appcssout: 'Content'

}

// Synchronously delete the output script file(s)
gulp.task('clean-vendor-scripts', function () {
    return del([config.jquerybundle,
              config.bootstrapbundle,
              config.modernizrbundle, config.ngbundle, config.momentBundle, config.reqBundle]);
});
gulp.task('adal', ['clean-vendor-scripts', 'bower-restore'], function () {
    return gulp.src(config.adalJs).pipe(debug())
        .pipe(gulp.dest('Scripts'));
});
//Create a modernizr bundled file
gulp.task('angular', ['clean-vendor-scripts', 'bower-restore'], function () {
    return gulp.src(config.ngSrc).pipe(debug())
        .pipe(sourcemaps.init())
//        .pipe(uglify())
        .pipe(concat('ng.min.js'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('Scripts'));
});
gulp.task('moment', ['clean-vendor-scripts', 'bower-restore'], function () {
    return gulp.src(config.momentJs).pipe(debug())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(concat('moment.min.js'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('Scripts'));
});

gulp.task('app', [], function () {
    return gulp.src(config.appJs).pipe(debug())
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('Scripts'));
});


gulp.task('require', ['clean-vendor-scripts', 'bower-restore'], function () {
    return gulp.src(config.reqSrc).pipe(debug())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(concat('require.min.js'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('Scripts'));
});
gulp.task('alljs', ['vendor-scripts'], function () {
    return rjs({
        baseUrl: "app",
        // alias libraries paths
        paths: {
            'jquery': '../scripts/jquery-bundle.min',
            'bootstrap': '../scripts/bootstrap-bundle.min',
            'moment': '../scripts/moment.min',
            'angular': '../scripts/ng.min',
            'adal': '../scripts/adal',
            'adalAngular': '../scripts/adal-angular',
            'appconfig': 'empty:'
        },
        name: 'main',
        out: 'main.min.js',
        //generateSourceMaps: true,
        // angular does not support AMD out of the box, put it in a shim
        shim: {
            'angular': {
                exports: 'angular', deps: ['jquery']
            },
            'adalAngular': {
                deps: ['angular']
            },
            'bootstrap': {
                deps: ['jquery']
            },
            'appconfig': { deps: ['angular'] }
        },
        
        optimize: "uglify2"
    }
        ).on('error', gutil.log).pipe(gulp.dest('Scripts'))
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('Scripts'));
});


//Create a jquery bundled file
gulp.task('jquery-bundle', ['clean-vendor-scripts', 'bower-restore'], function () {
    return gulp.src(config.jquerysrc)
     .pipe(concat('jquery-bundle.min.js'))
     .pipe(gulp.dest('Scripts'));
});

//Create a bootstrap bundled file
gulp.task('bootstrap-bundle', ['clean-vendor-scripts', 'bower-restore'], function () {
    return gulp.src(config.bootstrapsrc)
     .pipe(debug())
     .pipe(sourcemaps.init())
     .pipe(concat('bootstrap-bundle.min.js'))
     .pipe(sourcemaps.write('maps'))
     .pipe(gulp.dest('Scripts'));
});

//Create a modernizr bundled file
gulp.task('modernizer', ['clean-vendor-scripts', 'bower-restore'], function () {
    return gulp.src(config.modernizrsrc)
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(concat('modernizer-min.js'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('Scripts'));
});

// Combine and the vendor files from bower into bundles (output to the Scripts folder)
gulp.task('vendor-scripts', ['jquery-bundle', 'bootstrap-bundle', 'modernizer', 'angular', 'require', 'moment', 'adal'], function () {

});

// Synchronously delete the output style files (css / fonts)
gulp.task('clean-styles', function () {
    return del([config.fontsout,
              config.cssout + "/*.css",
                config.appcssout + '/*.css']);
});

gulp.task('css', ['clean-styles', 'bower-restore'], function () {
    return gulp.src(config.bootstrapcss)
        .pipe(debug())
        .pipe(sourcemaps.init())
        .pipe(concat('app.css'))
        .pipe(gulp.dest(config.cssout))
        .pipe(minifyCSS())
        .pipe(concat('app.min.css'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(config.cssout));
});

gulp.task('less', [], function () {
    return gulp.src([config.appless]).pipe(sourcemaps.init()).pipe(less())
        .pipe(concat('site.css'))
        .pipe(gulp.dest(config.appcssout))
        .pipe(minifyCSS())
        .pipe(concat('site.min.css'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(config.appcssout));

})

gulp.task('fonts', ['clean-styles', 'bower-restore'], function () {
    return gulp.src(config.boostrapfonts)
        .pipe(gulp.dest(config.fontsout));
});

// Combine and minify css files and output fonts
gulp.task('styles', ['css', 'less', 'fonts'], function () {

});

//Restore all bower packages
gulp.task('bower-restore', function () {
    return bower();
});

//Set a default tasks
gulp.task('default', ['styles', 'alljs'], function () {

});