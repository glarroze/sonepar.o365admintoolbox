﻿using Microsoft.Owin.Security;
using Graph = Microsoft.Graph;
using Sonepar.O365AdminToolbox.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Sonepar.O365AdminToolboxWebApi.Authentication;
using Sonepar.O365AdminToolboxWebApi.Filters;
using Sonepar.O365AdminToolbox.Entity;

namespace Sonepar.O365AdminToolboxWebApi.Controllers
{
    public class UserController : BaseController
    {
        private readonly IAppSettings settings;
        private readonly JsonWebTokenFormat format;
        private readonly IUserService userService;

        public UserController(IAppSettings settings, JsonWebTokenFormat format, IUserService userService) : base(settings, format, userService)
        {
            this.settings = settings;
            this.format = format;
            this.userService = userService;
        }

        /// <summary>
        /// Logins the user.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.UnauthorizedAccessException">User has no right to connect to the application</exception>
        [AuthorizeClaimsAad(), HttpGet(), Route("api/authentication")]
        public async Task<TokenResult> LoginUser()
        {
            var id = new ClaimsIdentity((RequestContext.Principal as ClaimsPrincipal).Claims);
            TokenResult response = null;

            IEnumerable<Claim> managed = await userService.GetUserManagedGroups(User.Identity.Name);
            id.AddClaims(managed);
            if (managed != null && managed.Any())
            {
                AuthenticationTicket t = new AuthenticationTicket(id, new AuthenticationProperties());

                response = new TokenResult
                {
                    access_token = format.Protect(t),
                    token_type = "bearer",
                    expires_in = 3600 * 24
                };
            }
            if (response == null) {
                throw new System.UnauthorizedAccessException("User has no right to connect to the application");
            }
            return response;
        }


        /// <summary>
        /// List the users.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/User")]
        public async Task<IEnumerable<ManagedUsers>> Get()
        {
            var identity = User.Identity as ClaimsIdentity;
            var countries = GetUserCountries();
            var users = await userService.GetUsersManaged(countries);
            return users;
        }

        /// <summary>
        /// Get current user's group.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/User/groups")]
        public async Task<IEnumerable<Graph.Group>> GetUserGroups()
        {
            var identity = User.Identity as ClaimsIdentity;
            var countries = GetUserCountries();
            var groups = await userService.GetUserGroups(countries);
            return groups;
        }

        /// <summary>
        /// Get current user's group.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/user/group/allmembers/{groupId}")]

        public async Task<IEnumerable<User>> getGroupAllMembers([FromUri]string groupId)
        {
            var identity = User.Identity as ClaimsIdentity;
            var countries = GetUserCountries();
            var users = await userService.GetUsersByGroup(countries, groupId);
            return users;
        }

        /// <summary>
        /// Get current user's group.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPost(), Route("api/user/group/members")]

        public async Task<PagedUsersResponse> getGroupMembersPaged([FromBody]RequestPagedUserPayload payload)
        {            
            var identity = User.Identity as ClaimsIdentity;
            var countries = GetUserCountries();
            var users = await userService.getGroupMemberPaged(countries, payload.groupId, payload.groupDisplayName, payload.skipToken);
            return users;
        }
        

        /// <summary>
        /// Check if current can manage the licences names
        /// </summary>
        /// <returns>Boolean</returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/User/iscurrentuserlicencemanager")]
        public async Task<bool> IsCurrentUserLicencesManager()
        {
            var identity = User.Identity as ClaimsIdentity;
            return await userService.isCurrentUserMemberOfLicenceManagers(User.Identity.Name);
        }

        /// <summary>
        /// List the users.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/Users")]
        public async Task<IEnumerable<Graph.User>> GetAll()
        {
            var users = await userService.GetAllUsers();
            return users;
        }

        /// <summary>
        /// Get a specific user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/User/{userId}")]
        public async Task<User> GetById(string userId)
        {
            var identity = User.Identity as ClaimsIdentity;
            var countries = GetUserCountries();

            return await userService.GetUser(userId, countries);
        }

        /// <summary>
        /// Get the MFA status
        /// </summary>
        /// <param name="UserPrincipalName"></param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/user/{UserPrincipalName}/mfastatus")]
        public async Task<AzureFunctionResponse> GetMFAStatus([FromUri]string UserPrincipalName)
        {
            var identity = User.Identity as ClaimsIdentity;
            return await userService.GetMFAStatus(UserPrincipalName);
        }


        /// <summary>
        /// Change the user MFA status
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/user/mfastatut")]
        public async Task<AzureFunctionResponse> ChangeMFAStatus([FromBody] AzureFunctionResponse payload)
        {
            var identity = User.Identity as ClaimsIdentity;
            return await userService.ChangeMFAStatus(payload.userPrincipalName, payload.userStatut);
        }


        /// <summary>
        /// Create an Office 365 Unified team.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/user/{userId}/statut")]
        public async Task<RevokationState> UpdateUserStatutById(string userId, [FromBody] bool statut)
        {
            return await userService.UpdateUserStatut(userId, statut);
        }

    }   
}
