﻿using Sonepar.O365AdminToolbox.Domain;
using System.Collections.Generic;
using Sonepar.O365AdminToolboxWebApi.Authentication;
using System.Threading.Tasks;
using Sonepar.O365AdminToolboxWebApi.Filters;
using System.Web.Http;
using System;
using Sonepar.O365AdminToolbox.Entity;

namespace Sonepar.O365AdminToolboxWebApi.Controllers
{
    /// <summary>
    /// Manage Office 365 Unified groups
    /// </summary>
    /// <seealso cref="BaseController" />
    public class GroupController : BaseController
    {
        private readonly IAppSettings settings;
        private readonly JsonWebTokenFormat format;
        private readonly IUserService userService;
        private readonly IGroupService groupService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupController"/> class.
        /// </summary>
        /// <param name="settings">The app settings.</param>
        /// <param name="format">The JWT formatter.</param>
        /// <param name="userService">The user service.</param>
        /// <param name="groupService">The group service.</param>
        public GroupController(
            IAppSettings settings,
            JsonWebTokenFormat format,
            IUserService userService,
            IGroupService groupService) : base(settings, format, userService)
        {
            this.settings = settings;
            this.format = format;
            this.userService = userService;
            this.groupService = groupService;
        }

        /// <summary>
        /// Gets the list of Office 365 Unified groups.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/group")]
        public async Task<IEnumerable<Group>> GetGroups()
        {
            return await groupService.GetGroupsManagedByUser(base.GetUserCountries());
        }


        /// <summary>
        /// Gets the list of Office 365 unregistered groups.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/unregisteredgroups")]
        public async Task<IEnumerable<Microsoft.Graph.Group>> GetAllGroups()
        {
            return await groupService.GetUnregisteredGroups();
        }

        /// <summary>
        /// Gets a Office 365 Unified group.
        /// </summary>
        /// <param name="groupId">The group identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/group/{groupId}")]
        public async Task<Group> GetGroups(string groupId)
        {
            return await groupService.GetGroup(groupId, base.GetUserCountries());
        }

        /// <summary>
        /// Create an Office 365 Unified group.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/group")]
        public async Task<Group> AddGroup([FromBody]Group group)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(group.Name, settings.GroupNameRegex))
                throw new Exception(settings.GroupNameRegexErrorMessage);

            return await groupService.NewGroup(group, base.GetUserCountries());
        }

        /// <summary>
        /// Update an Office 365 Unified group.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/group/update")]
        public async Task<Group> UpdateGroup([FromBody]Group group)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(group.Name, settings.GroupNameRegex))
                throw new Exception(settings.GroupNameRegexErrorMessage);

            return await groupService.UpdateGroup(group, base.GetUserCountries());
        }

        /// <summary>
        /// Delete an Office 365 Unified group.
        /// </summary>
        /// <param name="groupId">The group identifier.</param>
        [AuthorizeClaimsSelf(), HttpDelete(), Route("api/group/{groupId}")]
        public async Task DeleteGroup([FromUri]string groupId)
        {
            await groupService.DeleteGroup(groupId, base.GetUserCountries());
        }

        /// <summary>
        /// Add an Office 365 Unified group owner.
        /// </summary>
        /// <param name="groupId">The group identifier (Guid-like).</param>
        /// <param name="owner">The owner.</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/group/{groupId}/owners")]
        public async Task AddOwner([FromUri] string groupId, [FromBody] Member owner)
        {
            await groupService.AddGroupOwner(groupId, owner.Id, allowedCountries:base.GetUserCountries());
        }
        /// <summary>
        /// Remove an Office 365 Unified group owner.
        /// </summary>
        /// <param name="groupId">The group identifier (Guid-like).</param>
        /// <param name="ownerId">The owner identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpDelete(), Route("api/group/{groupId}/owners/{ownerId}")]
        public async Task DeleteOwner([FromUri] string groupId, [FromUri] string ownerId)
        {
            await groupService.RemoveGroupOwner(groupId, ownerId, allowedCountries: base.GetUserCountries());
        }

        /// <summary>
        /// List the group members.
        /// </summary>
        /// <param name="groupId">The group identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/group/{groupId}/users")]
        public async Task<IEnumerable<Member>> GetGroupMembers(string groupId)
        {
            return await groupService.GetGroupMembers(groupId, GetUserCountries());
        }

        /// <summary>
        /// List the group owners.
        /// </summary>
        /// <param name="groupId">The group identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/group/{groupId}/owners")]
        public async Task<IEnumerable<Member>> GetGroupOwners(string groupId
            )
        {
            return await groupService.GetGroupOwners(groupId, GetUserCountries());
        }

        /// <summary>
        /// Remove a group member.
        /// </summary>
        /// <param name="groupId">The group identifier (Guid-like).</param>
        /// <param name="userId">The user identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpDelete(), Route("api/group/{groupId}/users/{userId}")]
        public async Task RemoveGroupMember(string groupId, string userId)
        {
            await groupService.RemoveGroupMember(groupId, userId, GetUserCountries());
        }
        /// <summary>
        /// Add a group member.
        /// </summary>
        /// <param name="groupId">The group identifier (Guid-like).</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/group/{groupId}/users")]
        public async Task AddGroupMember(string groupId, [FromBody] Member user)
        {
            await groupService.AddGroupMember(groupId, user.Id, base.GetUserCountries());
        }

    }
}
