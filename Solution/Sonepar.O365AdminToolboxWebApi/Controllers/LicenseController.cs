﻿using Sonepar.O365AdminToolbox.Domain;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Sonepar.O365AdminToolboxWebApi.Authentication;
using Sonepar.O365AdminToolboxWebApi.Filters;
using Sonepar.O365AdminToolbox.Entity;

namespace Sonepar.O365AdminToolboxWebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="BaseController" />
    public class LicenseController : BaseController
    {
        private readonly IAppSettings settings;
        private readonly JsonWebTokenFormat format;
        private readonly IUserService userService;
        private readonly ILicenseService servicePlanService;
        private readonly IFriendlyNameService friendlyNameService;

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseController"/> class.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="format">The format.</param>
        /// <param name="userService">The user service.</param>
        /// <param name="friendlyNameService"></param>
        /// <param name="servicePlanService">The service plan service.</param>
        public LicenseController(
            IAppSettings settings, 
            JsonWebTokenFormat format, 
            IUserService userService,
            IFriendlyNameService friendlyNameService,
            ILicenseService servicePlanService)
                :base(
                     settings,
                     format,
                     userService)
        {
            this.settings = settings;
            this.format = format;
            this.userService = userService;
            this.servicePlanService = servicePlanService;
            this.friendlyNameService = friendlyNameService;
        }

        /// <summary>
        /// List all licenses (service plans) available.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/ServicePlan")]
        public async Task<IEnumerable<License>> Get()
        {
            var sp = await servicePlanService.GetServicePlansAvailable();
            return sp;
        }
        /// <summary>
        /// Gets the licences assigned to a user
        /// </summary>
        /// <param name="userId">The user identifier (Guid like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/ServicePlan/{userId}/plans")]
        public async Task<IEnumerable<License>> Get(string userId)
        {
            var identity = User.Identity as ClaimsIdentity;
            var sp = await servicePlanService.GetServicePlansForUser(userId, GetUserCountries());
            return sp;
        }


        /// <summary>
        /// Gets the licences assigned to a user
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/ServicePlan/friendlynames")]
        public async Task<IEnumerable<LicencesNames>> GetLicencesFriendlyNames()
        {
            var identity = User.Identity as ClaimsIdentity;
            var lfn = await friendlyNameService.GetLicencesFriendlyNames();
            return lfn;
        }

        /// <summary>
        /// Update an Office 365 Team.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/ServicePlan/update")]
        public async Task<bool> UpdateLicencesFriendlyNames([FromBody]List<LicencesNames> licenceNameList)
        {
            return await friendlyNameService.UpdateTranslations(licenceNameList);
        }



        /// <summary>
        /// Add or update a license for a user.
        /// </summary>
        /// <param name="userId">The user identifier (Guid-like).</param>
        /// <param name="license">The license.</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPost(), Route("api/ServicePlan/{userId}/plans")]
        public async Task Post(string userId, [FromBody]License license)
        {
            await servicePlanService.AddOrUpdateServicePlanForUser(userId,license,GetUserCountries());
        }
        /// <summary>
        /// remove a license for a user.
        /// </summary>
        /// <param name="userId">The user identifier (Guid-like).</param>
        /// <param name="licenseId">The license identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpDelete(), Route("api/ServicePlan/{userId}/plans/{licenseId}")]
        public async Task Delete(string userId, Guid licenseId) {
            await servicePlanService.RemoveServicePlanForUser(userId, licenseId, GetUserCountries());
        }

    }
}
