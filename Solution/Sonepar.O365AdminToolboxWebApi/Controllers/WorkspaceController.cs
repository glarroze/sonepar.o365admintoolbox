﻿using Sonepar.O365AdminToolbox.Domain;
using System.Collections.Generic;
using Sonepar.O365AdminToolboxWebApi.Authentication;
using System.Threading.Tasks;
using Sonepar.O365AdminToolboxWebApi.Filters;
using System.Web.Http;
using System;
using Sonepar.O365AdminToolbox.Entity;
using System.Text.RegularExpressions;

namespace Sonepar.O365AdminToolboxWebApi.Controllers
{
    /// <summary>
    /// Manage Office 365 Unified workspaces
    /// </summary>
    /// <seealso cref="BaseController" />
    public class WorkspaceController : BaseController
    {
        private char[] NotAllowedCharacters = new[] { '~', '#', '%', '&', '*', '{', '}', '\\', ':', '<', '>', '?', '/', '|', '"' };

        private readonly IAppSettings settings;
        private readonly JsonWebTokenFormat format;
        private readonly IUserService userService;
        private readonly IWorkspaceService workspaceService;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkspaceController"/> class.
        /// </summary>
        /// <param name="settings">The app settings.</param>
        /// <param name="format">The JWT formatter.</param>
        /// <param name="userService">The user service.</param>
        /// <param name="workspaceService">The workspace service.</param>
        public WorkspaceController(
            IAppSettings settings,
            JsonWebTokenFormat format,
            IUserService userService,
            IWorkspaceService workspaceService) : base(settings, format, userService)
        {
            this.settings = settings;
            this.format = format;
            this.userService = userService;
            this.workspaceService = workspaceService;
        }

        /// <summary>
        /// Gets the list of Office 365 Unified workspaces.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/workspace")]
        public async Task<IEnumerable<OfficeObject>> GetWorkspaces()
        {
            return await workspaceService.GetWorkspacesManagedByUser(GetUserCountries());
        }

        /// <summary>
        /// Update Urls for workspaces in database
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/updateworkspaceurls")]
        public async Task<IEnumerable<OfficeObject>> UpdateUrlsInDatabase()
        {
            return await workspaceService.CheckAndFixWorkSpacesUrl();
        }


        /// <summary>
        /// Gets the list of Office 365 unregistered workSpaces.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/unregisteredworkspaces")]
        public async Task<IEnumerable<OfficeObject>> GetAllGroups()
        {
            return await workspaceService.GetUnregisteredWorkSpaces();
        }

        /// <summary>
        /// Gets a Office 365 Unified workspace.
        /// </summary>
        /// <param name="workspaceId">The workspace identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/workspace/{workspaceId}")]
        public async Task<OfficeObject> GetWorkspaces(string workspaceId)
        {
            return await workspaceService.GetWorkspace(workspaceId, GetUserCountries());
        }

        /// <summary>
        /// Create an Office 365 Unified workspace.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/workspace")]
        public async Task<OfficeObject> AddWorkspace([FromBody]Workspace workspace)
        {
            if (!Regex.IsMatch(workspace.Name, settings.WorkspaceNameRegex))
                throw new Exception(settings.WorkspaceNameRegexErrorMessage);
            if (workspace.Name.IndexOfAny(NotAllowedCharacters) != -1)
                throw new Exception($"Workspace name cannot contain any of the following characters {String.Join(",", NotAllowedCharacters)}");

            return await workspaceService.NewWorkspace(workspace, GetUserCountries());
        }


        /// <summary>
        /// Create an SharePoint TeamSite.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/workspace/createteamsite")]
        public async Task<OfficeObject> AddSharePointTeamSite([FromBody]Workspace workspace)
        {
            if (!Regex.IsMatch(workspace.Name, settings.WorkspaceNameRegex))
                throw new Exception(settings.WorkspaceNameRegexErrorMessage);
            if (workspace.Name.IndexOfAny(NotAllowedCharacters) != -1)
                throw new Exception($"Workspace name cannot contain any of the following characters {String.Join(",", NotAllowedCharacters)}");
            return await workspaceService.NewSharePointSite(workspace);
        }

        /// <summary>
        /// Add workspace owners.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/workspace/addowners")]
        public async Task<OfficeObject> AddWorkspaceOwners([FromBody]Workspace workspace)
        {
            if (!Regex.IsMatch(workspace.Name, settings.WorkspaceNameRegex))
                throw new Exception(settings.WorkspaceNameRegexErrorMessage);
            if (workspace.Name.IndexOfAny(NotAllowedCharacters) != -1)
                throw new Exception($"Workspace name cannot contain any of the following characters {String.Join(",", NotAllowedCharacters)}");

            return await workspaceService.AddWorkspaceOwners(workspace, GetUserCountries());
        }



        public class WorkSpaceUpdate
        {
            public string Name { get; set; }
            public string Url { get; set; }
        }
        /// <summary>
        /// Update an Office 365 Team.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/workspace/update/{workspaceId}")]
        public async Task<OfficeObject> UpdateWorkspace([FromUri]string workspaceId, [FromBody]WorkSpaceUpdate wsUpdate)
        {
            if (!Regex.IsMatch(wsUpdate.Name, settings.TeamNameRegex))
                throw new Exception(settings.TeamNameRegexErrorMessage);
            if (wsUpdate.Name.IndexOfAny(NotAllowedCharacters) != -1)
                throw new Exception($"Workspace name cannot contain any of the following characters {String.Join(",", NotAllowedCharacters)}");
            return await workspaceService.UpdateWorkSpace(workspaceId, wsUpdate.Url, wsUpdate.Name, base.GetUserCountries());
        }

        /// <summary>
        /// Delete Worksapce
        /// </summary>
        /// <param name="workspace">The workspace OfficeObject</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/workspace/delete")]
        public async Task DeleteWorkspace([FromBody]OfficeObject workspace)
        {
            await workspaceService.DeleteWorkspace(workspace.Url, GetUserCountries());
        }

        /// <summary>
        /// Add an Office 365 Unified workspace owner.
        /// </summary>
        /// <param name="workspaceId">The workspace identifier (Guid-like).</param>
        /// <param name="owner">The owner.</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/workspace/{workspaceId}/owners")]
        public async Task AddOwner([FromUri] string workspaceId, [FromBody] Member owner)
        {
            await workspaceService.AddWorkspaceOwner(workspaceId, owner.Id, GetUserCountries());
        }

        /// <summary>
        /// Remove an Office 365 Unified workspace owner.
        /// </summary>
        /// <param name="workspaceId">The workspace identifier (Guid-like).</param>
        /// <param name="ownerId">The owner identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpDelete(), Route("api/workspace/{workspaceId}/owners/{ownerId}")]
        public async Task DeleteOwner([FromUri] string workspaceId, [FromUri] string ownerId)
        {
            await workspaceService.RemoveWorkspaceOwner(workspaceId, ownerId, GetUserCountries());
        }

        /// <summary>
        /// List the workspace owners.
        /// </summary>
        /// <param name="workspaceId">The workspace identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/workspace/{workspaceId}/owners")]
        public async Task<IEnumerable<Member>> GetWorkspaceOwners(string workspaceId)
        {
            return await workspaceService.GetWorkspaceOwners(workspaceId, GetUserCountries());
        }
    }
}
