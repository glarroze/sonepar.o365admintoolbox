﻿using Sonepar.O365AdminToolbox.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using Sonepar.O365AdminToolboxWebApi.Authentication;

namespace Sonepar.O365AdminToolboxWebApi.Controllers
{
    public class BaseController : ApiController
    {
        private const string AdminOfClaimType = "https://governance.sonepar.com/claims/adminOf";
        private readonly IAppSettings settings;
        private readonly JsonWebTokenFormat format;
        private readonly IUserService userService;

        public BaseController(IAppSettings settings, JsonWebTokenFormat format, IUserService userService)
        {
            this.settings = settings;
            this.format = format;
            this.userService = userService;
        }

        protected IEnumerable<string> GetUserCountries()
        {
            var identity = User.Identity as ClaimsIdentity;
            return identity?.Claims?.Where(x => x.Type == AdminOfClaimType)?.Select(x => x.Value.ToUpper()) ?? Enumerable.Empty<string>();
        }

    }
}
