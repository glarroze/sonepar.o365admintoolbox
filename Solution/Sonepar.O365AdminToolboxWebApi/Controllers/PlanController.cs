﻿using Sonepar.O365AdminToolbox.Domain;
using System.Collections.Generic;
using Sonepar.O365AdminToolboxWebApi.Authentication;
using System.Threading.Tasks;
using Sonepar.O365AdminToolboxWebApi.Filters;
using System.Web.Http;
using System;
using Sonepar.O365AdminToolbox.Entity;
using System.Text.RegularExpressions;

namespace Sonepar.O365AdminToolboxWebApi.Controllers
{
    /// <summary>
    /// Manage Office 365 Unified plans
    /// </summary>
    /// <seealso cref="BaseController" />
    public class PlanController : BaseController
    {
        private readonly IAppSettings settings;
        private readonly JsonWebTokenFormat format;
        private readonly IUserService userService;
        private readonly IPlanService planService;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlanController"/> class.
        /// </summary>
        /// <param name="settings">The app settings.</param>
        /// <param name="format">The JWT formatter.</param>
        /// <param name="userService">The user service.</param>
        /// <param name="planService">The group service.</param>
        public PlanController(
            IAppSettings settings,
            JsonWebTokenFormat format,
            IUserService userService,
            IPlanService planService) : base(settings, format, userService)
        {
            this.settings = settings;
            this.format = format;
            this.userService = userService;
            this.planService = planService;
        }

        /// <summary>
        /// Gets the list of Office 365 Unified plans.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/plan")]
        public async Task<IEnumerable<OfficeObject>> GetPlans()
        {
            return await planService.GetPlansManagedByUser(GetUserCountries());
        }

        /// <summary>
        /// Gets a Office 365 Unified plan.
        /// </summary>
        /// <param name="planId">The plan identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/plan/{planId}")]
        public async Task<OfficeObject> GetPlans(string planId)
        {
            return await planService.GetPlanAsync(planId, GetUserCountries());
        }

        /// <summary>
        /// Create an Office 365 Unified plan.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/plan")]
        public async Task<OfficeObject> AddPlan([FromBody]OfficeObject plan)
        {
            if (!Regex.IsMatch(plan.Name, settings.PlanNameRegex))
                throw new Exception(settings.PlanNameRegexErrorMessage);
            if (!Regex.IsMatch(plan.Name, "[a-zA-Z0-9.\\-_]+"))
                throw new Exception("Plan name can only contain letters and numbers as well as period, hyphen and underscore characters");

            return await planService.NewPlan(plan, GetUserCountries());
        }

        /// <summary>
        /// Update an Office 365 Team.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/plan/update")]
        public async Task<OfficeObject> UpdatePlan([FromBody]OfficeObject plan)
        {
            if (!Regex.IsMatch(plan.Name, settings.TeamNameRegex))
                throw new Exception(settings.TeamNameRegexErrorMessage);

            return await planService.UpdatePlan(plan, base.GetUserCountries());
        }

        /// <summary>
        /// Delete an Office 365 Unified plan.
        /// </summary>
        /// <param name="planId">The plan identifier.</param>
        [AuthorizeClaimsSelf(), HttpDelete(), Route("api/plan/{planId}")]
        public async Task DeletePlan([FromUri]string planId)
        {
            await planService.DeletePlan(planId, GetUserCountries());
        }

        /// <summary>
        /// Add an Office 365 Unified plan owner.
        /// </summary>
        /// <param name="planId">The plan identifier (Guid-like).</param>
        /// <param name="owner">The owner.</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/plan/{planId}/owners")]
        public async Task AddOwner([FromUri] string planId, [FromBody] Member owner)
        {
            await planService.AddPlanOwner(planId, owner.Id, GetUserCountries());
        }
        /// <summary>
        /// Remove an Office 365 Unified plan owner.
        /// </summary>
        /// <param name="planId">The plan identifier (Guid-like).</param>
        /// <param name="ownerId">The owner identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpDelete(), Route("api/plan/{planId}/owners/{ownerId}")]
        public async Task DeleteOwner([FromUri] string planId, [FromUri] string ownerId)
        {
            await planService.RemovePlanOwner(planId, ownerId, GetUserCountries());
        }

        /// <summary>
        /// List the plan members.
        /// </summary>
        /// <param name="planId">The plan identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/plan/{planId}/users")]
        public async Task<IEnumerable<Member>> GetPlanMembers(string planId)
        {
            return await planService.GetPlanMembers(planId, GetUserCountries());
        }

        /// <summary>
        /// List the plan owners.
        /// </summary>
        /// <param name="planId">The plan identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/plan/{planId}/owners")]
        public async Task<IEnumerable<Member>> GetPlanOwners(string planId)
        {
            return await planService.GetPlanOwners(planId, GetUserCountries());
        }

        /// <summary>
        /// Remove a plan member.
        /// </summary>
        /// <param name="planId">The plan identifier (Guid-like).</param>
        /// <param name="userId">The user identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpDelete(), Route("api/plan/{planId}/users/{userId}")]
        public async Task RemovePlanMember(string planId, string userId)
        {
            await planService.RemovePlanMember(planId, userId, GetUserCountries());
        }
        /// <summary>
        /// Add a plan member.
        /// </summary>
        /// <param name="planId">The plan identifier (Guid-like).</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/plan/{planId}/users")]
        public async Task AddPlanMember(string planId, [FromBody] Member user)
        {
            await planService.AddPlanMember(planId, user.Id, GetUserCountries());
        }

    }
}
