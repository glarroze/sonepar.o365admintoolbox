﻿using Newtonsoft.Json.Linq;
using Sonepar.O365AdminToolbox.Domain;
using Sonepar.O365AdminToolbox.Entity;
using Sonepar.O365AdminToolbox.GraphPort;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;


namespace Sonepar.O365AdminToolboxWebApi.Tools
{
    public class FriendlyNameService : IFriendlyNameService
    {
        static private object staticSyncRoot = new object();
        private ToolboxDbContext toolboxContext;
        protected readonly IAuthenticationService helper;

        public FriendlyNameService(ToolboxDbContext toolboxContext)
        {
            this.toolboxContext = toolboxContext;
            if (LicenceNameList == null)
            {
                lock (staticSyncRoot)
                {
                    if (LicenceNameList == null)
                    {
                        LicenceNameList = new List<LicencesNames>();
                        try
                        {
                            Task.Run(async () =>
                            {
                                var m = await GetLicencesFriendlyNames();
                                LicenceNameList = m.ToList();
                            });
                            //var m = JObject.Parse(ConfigurationManager.AppSettings["ServiceNameMapping"]);
                            //mapping = m.Properties().ToDictionary(x => x.Name, x => x.Value.ToObject<string>());
                        }
                        catch { }
                    }
                }
            }
        }


        public async Task<bool> UpdateTranslations(List<LicencesNames> licenceNameUpdates)
        {
            foreach(var licenceUpdate in licenceNameUpdates)
            {
                var lineToUpdate =  toolboxContext.LicencesNames.Where(t => t.LicenceGuid == licenceUpdate.LicenceGuid);
                var entryExists = lineToUpdate.Count() > 0;
                if (entryExists)
                {
                    lineToUpdate.First().FriendlyName = licenceUpdate.FriendlyName;
                    await toolboxContext.SaveChangesAsync();
                }
                else
                {
                    toolboxContext.LicencesNames.Add(licenceUpdate);
                    await toolboxContext.SaveChangesAsync();
                }
            }

            //teamDB.Name = team.Name;
            //await toolboxContext.SaveChangesAsync();


            // var licences = await toolboxContext.LicencesNames.Where(l => l.LanguageKey == language).ToListAsync();
            var m = await GetLicencesFriendlyNames();
            LicenceNameList = m.ToList();
            return true;
        }

        /// <summary>
        /// Get licences friendly names with current user language
        /// </summary>
        /// <param name="language">2 chars language code (fr, en etc.)</param>
        /// <returns></returns>
        //public async Task<IEnumerable<LicencesNames>> GetLicencesFriendlyNames(string language)
        //{
        //    var licences = await toolboxContext.LicencesNames.Where(l => l.LanguageKey == language).ToListAsync();
        //    return licences;
        //}
        /// <summary>
        /// Get all friendly names for all languages
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<LicencesNames>> GetLicencesFriendlyNames()
        {
            var licences = await toolboxContext.LicencesNames.ToListAsync();
            return licences;
        }

        static List<LicencesNames> LicenceNameList;


        public string GetNameOrDefault(string defaultValue, string serviceName, string languageCode)
        {
            var language = !string.IsNullOrEmpty(languageCode) ? languageCode.Split('-').FirstOrDefault() : "en";

            var licenceFriendlyName = defaultValue;
            var licence = LicenceNameList.SingleOrDefault(l => l.LicenceGuid == serviceName);

            if(licence != null)
            {
                licenceFriendlyName = licence.FriendlyName;
            }
            return licenceFriendlyName;
        }

        //    public void Register(string registeredName, string serviceName, ServiceType Type)
        //    {
        //        mapping[serviceName] = registeredName;
        //    }

    }
}