﻿namespace Sonepar.O365AdminToolbox.Entity
{
    /// <summary>
    /// A directory object
    /// </summary>
    public class Member
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string Id { get; set; }
    }
}