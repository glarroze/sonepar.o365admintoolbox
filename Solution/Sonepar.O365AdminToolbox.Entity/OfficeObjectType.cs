﻿namespace Sonepar.O365AdminToolbox.Entity
{
    public enum OfficeObjectType
    {
        Team = 0,
        Plan = 1,
        Workspace = 2
    }
}