﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sonepar.O365AdminToolbox.Entity
{
    public partial class ManagedUsers
    {
        [Key]
        public string Id { get; set; }

        public string DisplayName { get; set; }

        public string GroupIds { get; set; }

        public string Mail { get; set; }

        public bool HasLicenseAssigned { get; set; }

        public DateTime LastTimeModified { get; set; }
    }
}
