﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sonepar.O365AdminToolbox.Entity
{
    public class LicencesNames
    {
        public int Id { get; set; }
        public string LicenceGuid { get; set; }
        public string FriendlyName { get; set; }
    }
}
