﻿using System;
using System.Data.Entity;

namespace Sonepar.O365AdminToolbox.Entity
{
    public class ToolboxDbContext : DbContext
    {
        public virtual DbSet<OfficeObject> OfficeObjects { get; set; }
        public virtual DbSet<LicencesNames> LicencesNames { get; set; }

        public virtual DbSet<ManagedUsers> ManagedUsers { get; set; }

        public ToolboxDbContext()
            : base("ToolboxContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
                throw new ArgumentNullException(nameof(modelBuilder));

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<OfficeObject>().ToTable("OfficeObjects");
            modelBuilder.Entity<OfficeObject>().Property(t => t.Id).IsRequired();
            modelBuilder.Entity<OfficeObject>().Property(t => t.Name).IsRequired().HasMaxLength(255);
            modelBuilder.Entity<OfficeObject>().Property(t => t.Url).HasMaxLength(255);
            modelBuilder.Entity<OfficeObject>().Property(t => t.Country).IsRequired().HasMaxLength(3);
            modelBuilder.Entity<OfficeObject>().Property(t => t.Visibility).HasMaxLength(255);


            modelBuilder.Entity<LicencesNames>().ToTable("LicencesNames");
            modelBuilder.Entity<LicencesNames>().Property(t => t.Id).IsRequired();
            modelBuilder.Entity<LicencesNames>().Property(t => t.LicenceGuid).IsRequired().HasMaxLength(128);
            modelBuilder.Entity<LicencesNames>().Property(t => t.FriendlyName).IsRequired().HasMaxLength(255);


            modelBuilder.Entity<ManagedUsers>().ToTable("ManagedUsers");
            modelBuilder.Entity<ManagedUsers>().Property(t => t.Id).IsRequired().HasMaxLength(128);
            modelBuilder.Entity<ManagedUsers>().Property(t => t.DisplayName).IsRequired();
            modelBuilder.Entity<ManagedUsers>().Property(t => t.Mail).IsRequired();            
            modelBuilder.Entity<ManagedUsers>().Property(t => t.GroupIds).IsRequired();
            modelBuilder.Entity<ManagedUsers>().Property(t => t.LastTimeModified).IsRequired();
            modelBuilder.Entity<ManagedUsers>().Property(t => t.HasLicenseAssigned).IsRequired();
        }
    }
}
