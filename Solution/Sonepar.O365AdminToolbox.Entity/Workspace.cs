﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sonepar.O365AdminToolbox.Entity
{
    public class Workspace
    {
        public string Id { get; set; }
        public string Country { get; set; }
        public string Name { get; set; }

        public string Url { get; set; }
        public OfficeObjectType Type { get; set; }
        public IEnumerable<Member> Owners { get; set; }
        public string Visibility { get; set; }
        public string Description { get; set; }
        public uint Lcid { get; set; }

        public int TimeZoneId { get; set; }
    }
}
