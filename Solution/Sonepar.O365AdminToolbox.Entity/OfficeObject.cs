﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Sonepar.O365AdminToolbox.Entity
{
    public class OfficeObject : Member
    {
        public string Country { get; set; }
        public string Name { get; set; }

        public string Url { get; set; }
        [JsonIgnore]
        public OfficeObjectType Type { get; set; }
        public IEnumerable<Member> Owners { get; set; }
        public string Visibility { get; set; }
    }
}
