namespace Sonepar.O365AdminToolbox.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OfficeObjects",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Country = c.String(nullable: false, maxLength: 3),
                        Name = c.String(nullable: false, maxLength: 255),
                        Type = c.Int(nullable: false),
                        Visibility = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.OfficeObjects");
        }
    }
}
