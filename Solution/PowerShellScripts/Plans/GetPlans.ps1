#################################################################################
#
#								Get plans
#
#################################################################################

param(
	[switch] $force
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Getting plans"
	$r = Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/plan" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	$r
}
catch
{
	Write-Error "Unable to get plans from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}