#################################################################################
#
#								Delete plan owner
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$planId,
	[Parameter(Mandatory=$true)]$userId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Deleting plan owner"
	$p = Invoke-RestMethod -Method Delete -Uri "$endPointTenant/api/plan/$planId/owners/$userId" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	Write-Host "Plan owner deleted"
}
catch
{
	Write-Error "Unable to delete plan owner. Exiting. $($_.Exception.Message)"
	throw
}