#################################################################################
#
#								Get plan
#
#################################################################################

param(
	[switch] $force,
	$id
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Getting plan"
	Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/plan/$id" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
}
catch
{
	Write-Error "Unable to get plan from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}