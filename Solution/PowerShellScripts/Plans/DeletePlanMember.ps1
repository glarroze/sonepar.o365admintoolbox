#################################################################################
#
#								Delete plan member
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$planId,
	[Parameter(Mandatory=$true)]$userId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Deleting plan member"
	$p = Invoke-RestMethod -Method Delete -Uri "$endPointTenant/api/plan/$planId/users/$userId" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	Write-Host "Plan member deleted"
}
catch
{
	Write-Error "Unable to delete plan member. Exiting. $($_.Exception.Message)"
	throw
}