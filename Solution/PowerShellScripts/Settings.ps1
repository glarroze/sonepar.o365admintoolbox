#################################################################################
#
#								Settings for the project
#
#################################################################################

# Azure Ad Tenant
$aadTenant="gla64.onmicrosoft.com"

# Administration Tool's Endpoint
$endPointTenant="https://o365toolboxhubcollabdev.azurewebsites.net"
$ApiEndpointUri="https://graph.microsoft.com"

# Used to acquire the Token Id for the Admin tools
$redirectUriAdminTools="http://localhost" 
$clientIdAdminTools="83e09b34-874d-45d5-98a9-8e71ade5f612" # PowerShell (Native) Application ID (OK)

#Required for the Aad Token acquisition
$adal = "${env:ProgramFiles(x86)}\WindowsPowerShell\Modules\Azure\5.1.2\Services\Microsoft.IdentityModel.Clients.ActiveDirectory.dll"
$adalforms = "${env:ProgramFiles(x86)}\WindowsPowerShell\Modules\Azure\5.1.2\Services\Microsoft.IdentityModel.Clients.ActiveDirectory.WindowsForms.dll"
$authorityUri = "https://login.windows.net/$aadTenant"

# Change the values for $redirectUri, $clientId and $aadTenant with appropriate values
$redirectUri="https://localhost" 
$clientId="b656e149-133a-4638-b3fd-6a4a976eb32e" # WebApi (Native) Application ID (OK)

#Graph client settings (i.e. Web Api)
$graphredirectUri="https://localhost" 
$graphClientId="b656e149-133a-4638-b3fd-6a4a976eb32e" # WebApi (Native) Application ID (OK)
$graphApiEndpointUri="https://graph.microsoft.com"
$applicationId="27963b10-bac2-49e0-82a8-8ffddc8a8884" # WebApp (Web App/API) Object ID (OK)

if (-not ([System.Management.Automation.PSTypeName]'ServerCertificateValidationCallback').Type)
{
$certCallback = @"
    using System;
    using System.Net;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
    public class ServerCertificateValidationCallback
    {
        public static void Ignore()
        {
            if(ServicePointManager.ServerCertificateValidationCallback ==null)
            {
                ServicePointManager.ServerCertificateValidationCallback += 
                    delegate
                    (
                        Object obj, 
                        X509Certificate certificate, 
                        X509Chain chain, 
                        SslPolicyErrors errors
                    )
                    {
                        return true;
                    };
            }
        }
    }
"@
    Add-Type $certCallback
 }
[ServerCertificateValidationCallback]::Ignore()