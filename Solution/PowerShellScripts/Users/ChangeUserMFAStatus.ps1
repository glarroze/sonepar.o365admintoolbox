#################################################################################
#
#								Update User MFA Status
#		
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$UserStatus
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Updating user MFA Status"
	Invoke-RestMethod -Method Put -Uri "$endPointTenant/api/user/mfastatut" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $AzureFunctionResponse) -ContentType application/json
	Write-Host "User MFA Status updated"
}
catch
{
	Write-Error "Unable to update user MFA Status. Exiting. $($_.Exception.Message)"
	throw
}