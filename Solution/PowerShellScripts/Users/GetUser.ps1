#################################################################################
#
#								Get user from Azure AD
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$userId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Debug "Getting user"
	Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/user/$userId" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }  
}
catch
{
	Write-Error "Unable to get user from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}
