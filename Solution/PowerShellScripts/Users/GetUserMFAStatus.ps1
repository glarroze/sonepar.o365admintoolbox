#################################################################################
#
#								Get user MFA Status
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$userPrincipalName
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Debug "Getting user MFA Status"
	Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/user/$userPrincipalName/mfastatus" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }  

	
}
catch
{
	Write-Error "Unable to get user from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}
