param(
	$Pfxfile,
	$PfxPassword,
	$Thumbprint,
	$GroupCountryExtendedPropertyName,
    $NameMapping,
	
	$AdminId,
	$AdminLogin,
	$AdminPassword,
	
	$CacheExpiration,
    $RetryCount,
    $SharePointRoot,

    $EnableTeams,
    $EnablePlans,
    $EnableWorkspaces,
    
    $GroupNamePlaceholder,
    $GroupNameRegex,
    $GroupNameRegexErrorMessage,
    
    $TeamNamePlaceholder,
    $TeamNameRegex,
    $TeamNameRegexErrorMessage,
    
    $PlanNamePlaceholder,
    $PlanNameRegex,
    $PlanNameRegexErrorMessage,
    
    $WorkspaceNamePlaceholder,
    $WorkspaceNameRegex,
    $WorkspaceNameRegexErrorMessage,
	
	[Parameter( Mandatory=$true, ParameterSetName="BySubId")] $SubscriptionId,
	[Parameter( Mandatory=$true, ParameterSetName="BySubName")]$SubscriptionName,
	[Parameter( Mandatory=$true)] $SiteName,
	[Parameter( Mandatory=$true)] $ResourceGroupName
	
)

function ConvertTo-PsCustomObjectFromHashtable { 
     param ( 
         [Parameter(  
             Position = 0,   
             Mandatory = $true,   
             ValueFromPipeline = $true,  
             ValueFromPipelineByPropertyName = $true  
         )] [object[]]$hashtable 
     ); 
     
     begin { $i = 0; } 
     
     process { 
         foreach ($myHashtable in $hashtable) { 
             if ($myHashtable.GetType().Name -eq 'hashtable') { 
                 $output = New-Object -TypeName PsObject; 
                 Add-Member -InputObject $output -MemberType ScriptMethod -Name AddNote -Value {  
                     Add-Member -InputObject $this -MemberType NoteProperty -Name $args[0] -Value $args[1]; 
                 }; 
                 $myHashtable.Keys | Sort-Object | % {  
                     $output.AddNote($_, $myHashtable.$_);  
                 } 
                 $output; 
             } else { 
                 Write-Warning "Index $i is not of type [hashtable]"; 
             } 
             $i += 1;  
         } 
     } 
}
function ConvertTo-HashtableFromPsCustomObject { 
     param ( 
         [Parameter(  
             Position = 0,   
             Mandatory = $true,   
             ValueFromPipeline = $true,  
             ValueFromPipelineByPropertyName = $true  
         )] [object[]]$psCustomObject 
     ); 
     
     process {
         foreach ($myPsObject in $psCustomObject) { 
             $output = @{} 
             $myPsObject | Get-Member -MemberType *Property | % { 
                 $output.($_.name) = $myPsObject.($_.name); 
             } 
             $output 
         } 
     } 
}

try {
	if ( -not (get-module AzureRm.Websites))
	{
		Import-Module AzureRm.Websites | Out-Null
	}
	if(-not $global:_azuremanagercred) {
		$global:_azuremanagercred = get-credential
	}

	Write-Progress -CurrentOperation "Connecting" -Activity "Azure"
	Add-AzureRmAccount -Credential $global:_azuremanagercred -ErrorAction SilentlyContinue | Out-Null
	if($PSCmdlet.ParameterSetName -eq "BySubId") {
		Select-AzureRmSubscription -SubscriptionId $SubscriptionId | Out-Null
	} else {
		Select-AzureRmSubscription -SubscriptionName $SubscriptionName | Out-Null
	}
	Write-Progress -CurrentOperation "Connecting" -Activity "Azure" -Completed
	$app = Get-AzureRmWebApp -Name $SiteName

	$resource = Invoke-AzureRmResourceAction -ResourceGroupName $ResourceGroupName -ResourceType Microsoft.Web/sites/config -ResourceName "$SiteName/appsettings" -Action list -ApiVersion 2015-08-01 -Force

	$setting =( $resource.Properties | ConvertTo-HashtableFromPsCustomObject)
	$set = $false
	if($GroupCountryExtendedPropertyName) {
		$setting["GroupCountryExtendedPropertyName"] = $GroupCountryExtendedPropertyName;
		$set = $true
	}
	if($nameMapping) {
		if($nameMapping.getType().Name -eq "String") {
			$nameMapping = ($nameMapping | ConvertFrom-Json)
		}
		$setting["ServiceNameMapping"] = ($nameMapping | ConvertTo-Json -Depth 1 -Compress)
		$set=$true;
	}
	if($pfxFile) {
		$pfxRaw= [Byte[]](Get-Content -Path $pfxfile -Encoding Byte)
		$pfxBlob = [System.Convert]::ToBase64String($pfxRaw)
		Write-Progress -CurrentOperation "Uploading Certificate" -Activity "Azure"
		$properties= @{
			"pfxBlob"= $pfxBlob;
			"password"= $pfxPassword;
		}
		$ResourceLocation = (Get-AzureRmResourceGroup -Name $app.ResourceGroup).location
		$ResourceName = "sonerpar_crypt_cert"
		$resourceGroupName = $app.ResourceGroup
		$result=New-AzureRmResource -ResourceName $ResourceName -Location $ResourceLocation -PropertyObject $properties -ResourceGroupName $resourceGroupName -ResourceType "Microsoft.Web/certificates" -ApiVersion "2015-08-01" -Force 
		Write-Progress -CurrentOperation "Uploading Certificate" -Completed -activity "certificate"
		$setting["JWT_CERT"] = $setting["WEBSITE_LOAD_CERTIFICATES"] = $result.Properties.thumbprint
		$set=$true
	} elseif ($thumbprint) {
		$setting["JWT_CERT"] = $setting["WEBSITE_LOAD_CERTIFICATES"] = $thumbprint
		$set=$true
	}
	
	if ($AdminId) {
		$setting["AdminId"] = $AdminId;
		$set = $true
	}
	if ($AdminLogin) {
		$setting["AdminLogin"] = $AdminLogin;
		$set = $true
	}
	if ($AdminPassword) {
		$setting["AdminPassword"] = (&".\EncryptPass.ps1" $AdminPassword);
		$set = $true
	}
	
	if ($CacheExpiration) {
		$setting["CacheExpiration"] = $CacheExpiration;
		$set = $true
	}
    if ($RetryCount) {
		$setting["RetryCount"] = $RetryCount;
		$set = $true
	}
    if ($SharePointRoot) {
		$setting["SharePointRoot"] = $SharePointRoot;
		$set = $true
	}
	
	# Menu visibility
	if ($EnableTeams) {
		$setting["EnableTeams"] = $EnableTeams;
		$set = $true
	}
    if ($EnablePlans) {
		$setting["EnablePlans"] = $EnablePlans;
		$set = $true
	}
    if ($EnableWorkspaces) {
		$setting["EnableWorkspaces"] = $EnableWorkspaces;
		$set = $true
	}
	
	# Group name
    if ($GroupNamePlaceholder) {
		$setting["GroupNamePlaceholder"] = $GroupNamePlaceholder;
		$set = $true
	}
    if ($GroupNameRegex) {
		$setting["GroupNameRegex"] = $GroupNameRegex;
		$set = $true
	}
    if ($GroupNameRegexErrorMessage) {
		$setting["GroupNameRegexErrorMessage"] = $GroupNameRegexErrorMessage;
		$set = $true
	}
    
	# Team name
    if ($TeamNamePlaceholder) {
		$setting["TeamNamePlaceholder"] = $TeamNamePlaceholder;
		$set = $true
	}
    if ($TeamNameRegex) {
		$setting["TeamNameRegex"] = $TeamNameRegex;
		$set = $true
	}
    if ($TeamNameRegexErrorMessage) {
		$setting["TeamNameRegexErrorMessage"] = $TeamNameRegexErrorMessage;
		$set = $true
	}
    
	# Plan name
    if ($PlanNamePlaceholder) {
		$setting["PlanNamePlaceholder"] = $PlanNamePlaceholder;
		$set = $true
	}
    if ($PlanNameRegex) {
		$setting["PlanNameRegex"] = $PlanNameRegex;
		$set = $true
	}
    if ($PlanNameRegexErrorMessage) {
		$setting["PlanNameRegexErrorMessage"] = $PlanNameRegexErrorMessage;
		$set = $true
	}
     
	# Workspace name
    if ($WorkspaceNamePlaceholder) {
		$setting["WorkspaceNamePlaceholder"] = $WorkspaceNamePlaceholder;
		$set = $true
	}
    if ($WorkspaceNameRegex) {
		$setting["WorkspaceNameRegex"] = $WorkspaceNameRegex;
		$set = $true
	}
    if ($WorkspaceNameRegexErrorMessage) {
		$setting["WorkspaceNameRegexErrorMessage"] = $WorkspaceNameRegexErrorMessage;
		$set = $true
	}

	if($set) {
		$app2=Set-AzureRmWebApp -Name $SiteName -AppSettings $setting -ResourceGroupName $app.resourcegroup
	}
	$setting
}
catch {
	$_.Exception
	Write-Warning "An Error happened, task canceled"
}