#Require -Version 5.0
using namespace System.Text;
using namespace System.Security.Cryptography;
using namespace System.Security.Cryptography.X509Certificates;

Param (
	$password,
	$certificateThumprint
)

if(-not $certificateThumprint) {
	$certificateThumprint =  '70930488BF6D47416D48B95C7D67D802367E0CA1'
}

$certStore = New-Object X509Store -ArgumentList My, CurrentUser
$certStore.Open([OpenFlags]::ReadOnly)

$cert=[X509Certificate2]$certStore.Certificates.Find([X509FindType]::FindByThumbprint, $certificateThumprint, $false)[0]
$rsa=[RSACryptoServiceProvider]$cert.PrivateKey
[Encoding]::UTF8.GetString( $rsa.Decrypt([Convert]::FromBase64String($password), $false))
