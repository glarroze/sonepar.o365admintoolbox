param (
	[Parameter( ValueFromPipeline=$true )]$groupName, 
	$country
)

. .\Settings.ps1

BEGIN {
	$graphtoken=& ".\GetGraphToken.ps1"
	$url = "https://graph.microsoft.com/beta/applications/$applicationId/extensionProperties"
	$res = Invoke-WebRequest -Method Get -Headers @{ Authorization="bearer $graphtoken" } -Uri $url -ContentType "application/json" | % {$_.Content} | ConvertFrom-Json | ? { $_.Value.name -like "*_sonepar_country"}
	$propName = $res.value.name
}

PROCESS {
	$filter = [uri]::EscapeDataString("(displayName eq '$groupName') and GroupTypes/any(x:x eq 'Unified')")
	Write-Debug $filter
	
	$urlGroup = "https://graph.microsoft.com/v1.0/groups/?`$filter=$filter"
	Write-Debug $urlGroup
	
	$res = Invoke-WebRequest -Method Get -Headers @{ Authorization="bearer $graphtoken" } -Uri $urlGroup  -ContentType "application/json" | % {$_.Content} | ConvertFrom-Json 
	Write-Debug ($res.value[0]|ConvertTo-Json)
	
	$g = @{}
	$res.value[0].psobject.properties | % { $g[$_.Name] = $_.Value }
	$g[$propname]=$country
	$j = ($g|ConvertTo-Json)
	Write-Debug $j
	
	$urlGroup = "https://graph.microsoft.com/v1.0/groups/$($g.Id)"
	$res = Invoke-WebRequest -Method Patch -Headers @{ Authorization="bearer $graphtoken" } -Uri $urlGroup  -ContentType "application/json" -Body $j | % {$_.Content} | ConvertFrom-Json  
}

