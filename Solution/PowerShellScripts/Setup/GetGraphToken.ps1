. .\Settings.ps1

function GetAuthToken {
   param
   (
        [Parameter(Mandatory=$true)]
        $ApiEndpointUri,
        [Parameter(Mandatory=$true)]
        $AADTenant,
        [Parameter(Mandatory=$true)]
        $ClientId,
        [Parameter(Mandatory=$true)]
        $redirectUri
   )
	
   [System.Reflection.Assembly]::LoadFrom($adal) | Out-Null
   [System.Reflection.Assembly]::LoadFrom($adalforms) | Out-Null

   Try
   {
      $authContext = New-Object "Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext" -ArgumentList $authorityUri	
      $authResult = $authContext.AcquireToken($graphApiEndpointUri, $graphClientId,$graphRedirectUri, "Auto")
   }
   Catch
   {
      $ErrorMessage = $_.Exception.Message
	  Write-Host $ErrorMessage
   }
   
   return $authResult
}
Write-Host "acquiring AAD token"

$script:aadtoken=((GetAuthToken -ApiEndpointUri $graphApiEndpointUri -AADTenant $aadTenant -redirectUri $graphredirectUri -ClientId $graphclientId).AccessToken)
$script:aadtoken| Set-Clipboard 
$script:aadtoken