#Require -Version 5.0
using namespace System.Text;
using namespace System.Security.Cryptography;
using namespace System.Security.Cryptography.X509Certificates;

Param (
	$password,
	$certificateThumprint
)

if(-not $certificateThumprint) {
	$certificateThumprint =  '1A57E0C50D346326EBC2B38F13D7DF7449C53E1D'
}

$certStore = New-Object X509Store -ArgumentList My, CurrentUser
$certStore.Open([OpenFlags]::ReadOnly)

$cert=[X509Certificate2]$certStore.Certificates.Find([X509FindType]::FindByThumbprint, $certificateThumprint, $false)[0]
$rsa=[RSACryptoServiceProvider]$cert.PublicKey.Key
[Convert]::ToBase64String( $rsa.Encrypt([Encoding]::UTF8.GetBytes($password), $false))
