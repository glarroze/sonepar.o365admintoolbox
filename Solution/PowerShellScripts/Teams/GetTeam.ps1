#################################################################################
#
#								Get team
#
#################################################################################

param(
	[switch] $force,
	$id
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Getting team"
	Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/team/$id" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
}
catch
{
	Write-Error "Unable to get team from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}