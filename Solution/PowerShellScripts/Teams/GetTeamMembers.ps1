#################################################################################
#
#								Get team members
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$teamId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Getting team members"
	$r = Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/team/$teamId/users" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	$r
}	
catch
{
	Write-Error "Unable to get team members from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}