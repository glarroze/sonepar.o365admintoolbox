#################################################################################
#
#								Add team
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$team
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Adding team"
	Invoke-RestMethod -Method Put -Uri "$endPointTenant/api/team" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $team) -ContentType application/json
	Write-Host "Team added"
}
catch
{
	Write-Error "Unable to add team. Exiting. $($_.Exception.Message)"
	throw
}