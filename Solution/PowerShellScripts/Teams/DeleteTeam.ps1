#################################################################################
#
#								Delete team
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$teamId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Deleting team"
	$p = Invoke-RestMethod -Method Delete -Uri "$endPointTenant/api/team/$teamId" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	Write-Host "Team deleted"
}
catch
{
	Write-Error "Unable to delete team. Exiting. $($_.Exception.Message)"
	throw
}