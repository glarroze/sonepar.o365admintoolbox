#################################################################################
#
#								Delete team member
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$teamId,
	[Parameter(Mandatory=$true)]$userId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Deleting team member"
	$p = Invoke-RestMethod -Method Delete -Uri "$endPointTenant/api/team/$teamId/users/$userId" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	Write-Host "Team member deleted"
}
catch
{
	Write-Error "Unable to delete team member. Exiting. $($_.Exception.Message)"
	throw
}