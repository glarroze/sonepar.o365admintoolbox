#################################################################################
#
#								Add team owner
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$teamId,
	[Parameter(Mandatory=$true)]$owner
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Adding team owner"
	Invoke-RestMethod -Method Put -Uri "$endPointTenant/api/team/$teamId/owners" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $owner) -ContentType application/json
	Write-Host "Team owner added"
}
catch
{
	Write-Error "Unable to add team owner. Exiting. $($_.Exception.Message)"
	throw
}