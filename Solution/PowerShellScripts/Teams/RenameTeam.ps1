#################################################################################
#
#								Update team
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$team
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Updating team"
	Invoke-RestMethod -Method Put -Uri "$endPointTenant/api/team/update" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $team) -ContentType application/json
	Write-Host "Team updated"
}
catch
{
	Write-Error "Unable to update team. Exiting. $($_.Exception.Message)"
	throw
}