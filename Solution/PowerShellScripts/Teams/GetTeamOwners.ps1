#################################################################################
#
#								Get team owners
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$teamId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Getting team owners"
	$r = Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/team/$teamId/owners" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	$r
}
catch
{
	Write-Error "Unable to get teams owners from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}