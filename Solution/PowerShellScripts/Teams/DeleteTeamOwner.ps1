#################################################################################
#
#								Delete team owner
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$teamId,
	[Parameter(Mandatory=$true)]$userId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Deleting team owner"
	$p = Invoke-RestMethod -Method Delete -Uri "$endPointTenant/api/team/$teamId/owners/$userId" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	Write-Host "Team owner deleted"
}
catch
{
	Write-Error "Unable to delete team owner. Exiting. $($_.Exception.Message)"
	throw
}