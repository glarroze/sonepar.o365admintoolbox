#################################################################################
#
#								Get teams
#
#################################################################################

param(
	[switch] $force
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Getting teams"
	$r = Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/team" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	$r
}
catch
{
	Write-Error "Unable to get teams from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}