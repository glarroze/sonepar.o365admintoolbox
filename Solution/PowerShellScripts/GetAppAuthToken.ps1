#################################################################################
#
#								Get Authentication Token
#                              
#################################################################################

function Get-ScriptDirectory
{
    Split-Path $MyInvocation.PSCommandPath
}
function GetAuthToken {
   param
   (
        [Parameter(Mandatory=$true)]
        $ApiEndpointUri,
        [Parameter(Mandatory=$true)]
        $AADTenant,
        [Parameter(Mandatory=$true)]
        $ClientId,
        [Parameter(Mandatory=$true)]
        $redirectUri,
        [Parameter(Mandatory=$false)]
        $userCredential,
	    [Switch]
        $force

   )
   try {
		[System.Reflection.Assembly]::LoadFrom($adal) | Out-Null
		[System.Reflection.Assembly]::LoadFrom($adalforms) | Out-Null
   
		$authContext = New-Object "Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext" -ArgumentList $authorityUri

		$prompt = "Auto"
		if($force) {
			$prompt="Always"
		}
		if(-not $userCredential) {
			$authResult = $authContext.AcquireToken($ApiEndpointUri, $clientId,$redirectUri, $prompt)
		} else {
			$credential = New-Object "Microsoft.IdentityModel.Clients.ActiveDirectory.UserCredential" -ArgumentList ($userCredential.UserName, $userCredential.Password)
			$authResult = $authContext.AcquireToken($ApiEndpointUri, $clientId,$credential) 
		}
		return $authResult
   } 
   catch {
		throw
   }
}

function GetAppToken([Switch]$Force, $userCredential){
	. .\Settings.ps1
	try {
		if(-not $script:apptoken) {
		write-host $endPointTenant/api/authentication;
		write-host $ApiEndpointUri;
		write-host $redirectUriAdminTools;
			$script:aadtoken=((GetAuthToken -ApiEndpointUri $ApiEndpointUri -AADTenant  $aadTenant -redirectUri $redirectUriAdminTools -ClientId $clientIdAdminTools -Force:$Force -userCredential $userCredential).AccessToken)
			write-host $script:aadtoken
			Write-host "Invoke-RestMethod -Method Get -Uri $endPointTenant/api/authentication -Headers @{Authorization = "bearer $script:aadtoken"}"
			$script:apptoken=(Invoke-RestMethod -Method Get -Uri $endPointTenant/api/authentication -Headers @{"Authorization"="Bearer $script:aadtoken"} )
		}
		$script:apptoken
	}
	catch {
		throw
	}
}