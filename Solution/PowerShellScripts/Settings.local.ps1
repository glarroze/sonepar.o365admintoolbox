#################################################################################
#
#								Settings for the project
#
#################################################################################

# Azure Ad Tenant
$aadTenant="sonepareu.onmicrosoft.com"

# Administration Tool's Endpoint
$endPointTenant="https://localhost:44306/"
$ApiEndpointUri="https://sonepareu.onmicrosoft.com/6fcb9b89-ab5b-4de3-a625-10400d24b767"

# Used to acquire the Token Id for the Admin tools
$redirectUriAdminTools="http://localhost" 
$clientIdAdminTools="461062d6-a101-4f30-959b-c73da7813fca" # PowerShell (Native) Application ID

#Required for the Aad Token acquisition
$adal = "${env:ProgramFiles(x86)}\WindowsPowerShell\Modules\Azure\5.1.2\Services\Microsoft.IdentityModel.Clients.ActiveDirectory.dll"
$adalforms = "${env:ProgramFiles(x86)}\WindowsPowerShell\Modules\Azure\5.1.2\Services\Microsoft.IdentityModel.Clients.ActiveDirectory.WindowsForms.dll"
$authorityUri = "https://login.windows.net/$aadTenant"

# Change the values for $redirectUri, $clientId and $aadTenant with appropriate values
$redirectUri="https://localhost" 
$clientId="d256d42c-0e3f-40eb-8770-6f7fe5f46a51" # WebApi (Native) Application ID

#Graph client settings (i.e. Web Api)
$graphredirectUri="https://localhost" 
$graphClientId="d256d42c-0e3f-40eb-8770-6f7fe5f46a51" # WebApi (Native) Application ID
$graphApiEndpointUri="https://graph.microsoft.com"
$applicationId="3354b505-bb85-47f4-94d4-67cb0a37b5d6" # WebApp (Web App/API) Object ID

if (-not ([System.Management.Automation.PSTypeName]'ServerCertificateValidationCallback').Type)
{
$certCallback = @"
    using System;
    using System.Net;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
    public class ServerCertificateValidationCallback
    {
        public static void Ignore()
        {
            if(ServicePointManager.ServerCertificateValidationCallback ==null)
            {
                ServicePointManager.ServerCertificateValidationCallback += 
                    delegate
                    (
                        Object obj, 
                        X509Certificate certificate, 
                        X509Chain chain, 
                        SslPolicyErrors errors
                    )
                    {
                        return true;
                    };
            }
        }
    }
"@
    Add-Type $certCallback
 }
[ServerCertificateValidationCallback]::Ignore()