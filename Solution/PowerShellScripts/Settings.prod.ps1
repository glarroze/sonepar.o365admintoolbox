#################################################################################
#
#								Settings for the project
#
#################################################################################

# Azure Ad Tenant
$aadTenant="sonepar.onmicrosoft.com"

# Administration Tool's Endpoint
$endPointTenant="https://o365toolbox.azurewebsites.net/"
$ApiEndpointUri="https://sonepar.onmicrosoft.com/07ff614e-d8fe-4652-8402-4d358d4c1baf"

# Used to acquire the Token Id for the Admin tools
$redirectUriAdminTools="http://localhost" 
$clientIdAdminTools="dc91cdea-e5b5-4190-9b0d-ee53b9c65abf" # PowerShell (Native) Application ID

#Required for the Aad Token acquisition
$adal = "${env:ProgramFiles(x86)}\WindowsPowerShell\Modules\Azure\5.1.2\Services\Microsoft.IdentityModel.Clients.ActiveDirectory.dll"
$adalforms = "${env:ProgramFiles(x86)}\WindowsPowerShell\Modules\Azure\5.1.2\Services\Microsoft.IdentityModel.Clients.ActiveDirectory.WindowsForms.dll"
$authorityUri = "https://login.windows.net/$aadTenant"

# Change the values for $redirectUri, $clientId and $aadTenant with appropriate values
$redirectUri="https://localhost" 
$clientId="a7fcbd81-7eaa-4f77-b6e8-56ebefa99724" # WebApi (Native) Application ID

#Graph client settings (i.e. Web Api)
$graphredirectUri="https://localhost" 
$graphClientId="a7fcbd81-7eaa-4f77-b6e8-56ebefa99724" # WebApi (Native) Application ID
$graphApiEndpointUri="https://graph.microsoft.com"
$applicationId="781363f0-85f3-4ea8-9a91-14b1826f0ba5" # WebApp (Web App/API) Object ID
