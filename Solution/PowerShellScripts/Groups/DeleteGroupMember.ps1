#################################################################################
#
#								Delete group member
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$groupId,
	[Parameter(Mandatory=$true)]$userId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Deleting group member"
	$p = Invoke-RestMethod -Method Delete -Uri "$endPointTenant/api/group/$groupId/users/$userId" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	Write-Host "Group member deleted"
}
catch
{
	Write-Error "Unable to delete group member. Exiting. $($_.Exception.Message)"
	throw
}