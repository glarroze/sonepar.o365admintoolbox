#################################################################################
#
#								Delete group owner
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$groupId,
	[Parameter(Mandatory=$true)]$userId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Deleting group owner"
	$p = Invoke-RestMethod -Method Delete -Uri "$endPointTenant/api/group/$groupId/owners/$userId" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	Write-Host "Group owner deleted"
}
catch
{
	Write-Error "Unable to delete group owner. Exiting. $($_.Exception.Message)"
	throw
}