#################################################################################
#
#								Get group owners
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$groupId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Getting group owners"
	$r = Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/group/$groupId/owners" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	$r
}
catch
{
	Write-Error "Unable to get groups owners from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}