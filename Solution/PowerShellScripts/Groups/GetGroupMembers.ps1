#################################################################################
#
#								Get group members
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$groupId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Getting group members"
	$r = Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/group/$groupId/users" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	$r
}	
catch
{
	Write-Error "Unable to get group members from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}