#################################################################################
#
#								Get groups
#
#################################################################################

param(
	[switch] $force
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Getting groups"
	$r = Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/group" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	$r
}
catch
{
	Write-Error "Unable to get groups from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}