#################################################################################
#
#								Add group
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$group
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Adding Office 365 group"
	Invoke-RestMethod -Method Put -Uri "$endPointTenant/api/group" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $group) -ContentType application/json
	Write-Host "Group added"
}
catch
{
	Write-Error "Unable to add group. Exiting. $($_.Exception.Message)"
	throw
}