#################################################################################
#
#								Delete group
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$groupId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Deleting group"
	$p = Invoke-RestMethod -Method Delete -Uri "$endPointTenant/api/group/$groupId" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	Write-Host "Group deleted"
}
catch
{
	Write-Error "Unable to delete group. Exiting. $($_.Exception.Message)"
	throw
}