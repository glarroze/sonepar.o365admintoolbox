#################################################################################
#
#								Add group owner
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$groupId,
	[Parameter(Mandatory=$true)]$owner
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Adding group owner"
	Invoke-RestMethod -Method Put -Uri "$endPointTenant/api/group/$groupId/owners" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $owner) -ContentType application/json
	Write-Host "Group owner added"
}
catch
{
	Write-Error "Unable to add group owner. Exiting. $($_.Exception.Message)"
	throw
}