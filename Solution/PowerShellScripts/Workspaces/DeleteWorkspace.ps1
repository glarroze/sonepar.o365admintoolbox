#################################################################################
#
#								Delete workspace
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$workspaceId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Deleting workspace"
	$p = Invoke-RestMethod -Method Delete -Uri "$endPointTenant/api/workspace/$workspaceId" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	Write-Host "Workspace deleted"
}
catch
{
	Write-Error "Unable to delete workspace. Exiting. $($_.Exception.Message)"
	throw
}