#################################################################################
#
#								Add workspace owner
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$workspaceId,
	[Parameter(Mandatory=$true)]$owner
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Adding workspace owner"
	Invoke-RestMethod -Method Put -Uri "$endPointTenant/api/workspace/$workspaceId/owners" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $owner) -ContentType application/json
	Write-Host "Workspace owner added"
}
catch
{
	Write-Error "Unable to add workspace owner. Exiting. $($_.Exception.Message)"
	throw
}