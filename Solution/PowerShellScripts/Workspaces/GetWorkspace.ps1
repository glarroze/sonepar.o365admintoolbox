#################################################################################
#
#								Get workspace
#
#################################################################################

param(
	[switch] $force,
	$id
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Getting workspace"
	Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/workspace/$id" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
}
catch
{
	Write-Error "Unable to get workspace from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}