#################################################################################
#
#								Add workspace
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$workspace
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Adding workspace"
	Invoke-RestMethod -Method Put -Uri "$endPointTenant/api/workspace" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $workspace) -ContentType application/json
	Write-Host "Workspace added"
}
catch
{
	Write-Error "Unable to add workspace. Exiting. $($_.Exception.Message)"
	throw
}