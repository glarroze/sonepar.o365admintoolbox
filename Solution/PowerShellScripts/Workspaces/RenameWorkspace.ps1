#################################################################################
#
#								Update workspace
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$workspaceId
	[Parameter(Mandatory=$true)]$workspace
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Updating workspace"
	Invoke-RestMethod -Method Put -Uri "$endPointTenant/api/workspace/update/$workspaceId" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $workspace) -ContentType application/json
	Write-Host "Workspace updated"
}
catch
{
	Write-Error "Unable to update workspace. Exiting. $($_.Exception.Message)"
	throw
}