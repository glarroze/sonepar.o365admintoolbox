#################################################################################
#
#								Delete workspace owner
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$workspaceId,
	[Parameter(Mandatory=$true)]$userId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Deleting workspace owner"
	$p = Invoke-RestMethod -Method Delete -Uri "$endPointTenant/api/workspace/$workspaceId/owners/$userId" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	Write-Host "Workspace owner deleted"
}
catch
{
	Write-Error "Unable to delete workspace owner. Exiting. $($_.Exception.Message)"
	throw
}