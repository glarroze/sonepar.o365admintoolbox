#################################################################################
#
#				Get Auth Token : we must run this script the first time once.
#
#################################################################################
$ApiEndpointUri="https://graph.windows.net"
function GetAuthToken {
   param
   (
        [Parameter(Mandatory=$true)]
        $ApiEndpointUri,
        [Parameter(Mandatory=$true)]
        $AADTenant,
        [Parameter(Mandatory=$true)]
        $ClientId,
        [Parameter(Mandatory=$true)]
        $redirectUri
   )
   . .\Settings.ps1

   try {
   
	   [System.Reflection.Assembly]::LoadFrom($adal) | Out-Null
	   [System.Reflection.Assembly]::LoadFrom($adalforms) | Out-Null
   
	#################################################################################
	#
	#								Getting AAD Token
	#
	#################################################################################


	   $authContext = New-Object "Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext" -ArgumentList $authorityUri
		
	   $authResult = $authContext.AcquireToken($ApiEndpointUri, $clientId,$redirectUri, "Auto")
	   #$authResult = [Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContextIntegratedAuthExtensions]::AcquireTokenAsync($authContext,$ApiEndpointUri, $clientId,$userCredential) 

	   return $authResult
   }
   catch{
		throw
   }
}

Write-Host "acquiring AAD token"

try{
	$script:aadtoken=((GetAuthToken -ApiEndpointUri $ApiEndpointUri -AADTenant  $aadTenant -redirectUri $redirectUri -ClientId $clientId).AccessToken)
	#$script:aadtoken | Set-Clipboard 
	$script:aadtoken 
}
catch {
	Write-Error "Unable to get token from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}