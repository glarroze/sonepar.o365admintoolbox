#################################################################################
#
#								Disable service plan
#
#################################################################################
param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$userId,
	[Parameter(Mandatory=$true)]$planid
)
. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Disabling service plan"
	$p = Invoke-RestMethod -Method Delete -Uri "$endPointTenant/api/ServicePlan/$userId/plans/$planid" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } 
	Write-Host "Service plan disabled"
}
catch
{
	Write-Error "Unable to disable service plan. Exiting. $($_.Exception.Message)"
	throw
}