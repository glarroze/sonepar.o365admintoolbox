#################################################################################
#
#								Get user service plans
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$userId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1 

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Get user service plans"
	$r = Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/ServicePlan/$userId/plans" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	$r
}
catch
{
	Write-Error "Unable to get user service plans. Exiting. $($_.Exception.Message)"
	throw
}