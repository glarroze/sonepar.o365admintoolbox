﻿namespace Sonepar.O365AdminToolbox.Domain
{
    public interface IGlobalSettings
    {
        string AuthString { get; }
        string GraphServiceObjectId { get; }
        string AadResourceUrl { get; }
        string GraphResourceUrl { get; }
    }
}