﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sonepar.O365AdminToolbox.Domain
{
    public class License
    {
        public Guid? Id { get; set; }
        public string FriendlyName { get; set; }
        public string Name { get; set; }
        public string UsageLocation { get; set; }
        public IEnumerable<ServiceAssignement> Services { get; set; }
        public string AppliesTo { get; set; }
        public int? ConsumedUnits { get; set; }
        public int? PrepaidUnitsEnabled { get; set; }

        public static License Copy(License plan)
        {
            return new License()
            {
                Id = plan.Id,
                FriendlyName = plan.FriendlyName,
                Name = plan.Name,
                UsageLocation = plan.UsageLocation,
                PrepaidUnitsEnabled = plan.PrepaidUnitsEnabled,
                ConsumedUnits = plan.ConsumedUnits,
                AppliesTo = plan.AppliesTo,
                Services = plan.Services.Select
                (x => new ServiceAssignement
                {
                    IsActive = x.IsActive,
                    Service = x.Service
                }).ToList()
            };
        }
        public License Copy()
        {
            return Copy(this);
        }

    }
}
