﻿
using System;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace Sonepar.O365AdminToolbox.Domain
{
    public class AdminCredentials : IAdminCredentials
    {
        private readonly ICertificateStore certificateStore;
        private readonly IAppSettings settings;

        public AdminCredentials(IAppSettings settings, ICertificateStore store) {
            this.settings = settings;
            certificateStore = store;
        }

        public UserPassCredentials GetAdminCredentials()
        {
            var p = new SecureString();

            var adminPassword = Convert.FromBase64String(settings.AdminPassword);

            Encoding.UTF8.GetString((certificateStore.GetCert().PrivateKey as RSACryptoServiceProvider).Decrypt(adminPassword, false)).ToList().ForEach(c => p.AppendChar(c));
            return new UserPassCredentials() { UserLogin = settings.AdminLogin, Password = p  };
        }

    }
}
