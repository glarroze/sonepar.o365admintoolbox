﻿using System;

namespace Sonepar.O365AdminToolbox.Domain
{
    public interface IExceptionHandler
    {
        string MakeMessage(Exception e);
    }
}
