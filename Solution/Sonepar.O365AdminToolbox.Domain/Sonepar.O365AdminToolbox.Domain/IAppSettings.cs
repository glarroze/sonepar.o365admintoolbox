﻿using System;

namespace Sonepar.O365AdminToolbox.Domain
{
    public interface IAppSettings
    {
        Uri AppRedirectUrl { get; }
        string AppBaseUrl { get; }
        string AuthString { get; }
        string ClientId { get; }
        string ClientIdAdmin { get; }
        string TenantId { get; }
        string TenantName { get; }
        string SPOSignOutFunctionUrl { get; }
        string GetMFAStatusFunctionUrl { get; }
        string ChangeMFAStatusFunctionUrl { get; }
        string AadIssuer { get; }
        string Issuer { get; }
        string Audience { get; }

        string AdminId { get; }
        string AdminLogin { get; }
        string AdminPassword { get; }
        string LicencesNamesEditorGroup { get; }
        string MembersByRequest { get; }
        string GroupCountryExtendedPropertyName { get; }

        string ManagedGroupsRegEx { get; }
        string ManagedGroupsReplace { get; }

        string AppInsightId { get; }

        int CacheExpiration { get; }
        int RetryCount { get; }
        string SharePointRoot { get; }
        string SharePointBaseUrl { get; }

        bool EnableTeams { get; }
        bool EnablePlans { get; }
        bool EnableWorkspaces { get; }

        string GroupNamePlaceholder { get; }
        string GroupNameRegex { get; }
        string GroupNameRegexErrorMessage { get; }

        string TeamNamePlaceholder { get; }
        string TeamNameRegex { get; }
        string TeamNameRegexErrorMessage { get; }

        string PlanNamePlaceholder { get; }
        string PlanNameRegex { get; }
        string PlanNameRegexErrorMessage { get; }

        string WorkspaceNamePlaceholder { get; }
        string WorkspaceNameRegex { get; }
        string WorkspaceNameRegexErrorMessage { get; }
    }
}