﻿namespace Sonepar.O365AdminToolbox.Domain
{
    public enum ServiceType
    {
        None = 0,
        Service,
        Plan
    }
}