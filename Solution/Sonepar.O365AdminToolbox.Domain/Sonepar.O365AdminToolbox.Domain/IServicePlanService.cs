﻿using Sonepar.O365AdminToolbox.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sonepar.O365AdminToolbox.Domain
{
    public interface ILicenseService
    {
        Task AddOrUpdateServicePlanForUser(string userId, License servicePlan, IEnumerable<string> countries);        
        Task<IEnumerable<License>> GetServicePlansAvailable();
        Task<IEnumerable<License>> GetServicePlansForUser(string userId, IEnumerable<string> countries);
        Task RemoveServicePlanForUser(string userId, Guid servicePlanId, IEnumerable<string> countries);
    }
}