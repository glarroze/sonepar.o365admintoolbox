﻿using Sonepar.O365AdminToolbox.Entity;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Graph = Microsoft.Graph;

namespace Sonepar.O365AdminToolbox.Domain
{
    public interface IUserService
    {
        void BindUser(UserPassCredentials user);
        Task<IEnumerable<Claim>> GetUserManagedGroups(string userName);

        Task<IEnumerable<ManagedUsers>> GetUsersManaged(IEnumerable<string> countries);

        Task<IEnumerable<Graph.Group>> GetUserGroups(IEnumerable<string> countries);

        Task<IEnumerable<User>> GetUsersByGroup(IEnumerable<string> countries, string groupId);

        Task<PagedUsersResponse> getGroupMemberPaged(IEnumerable<string> countries, string groupId, string groupDisplayName, string skipToken);

        Task<IEnumerable<Graph.User>> GetAllUsers();

        Task<bool> isCurrentUserMemberOfLicenceManagers(string userPrincipalName);

        Task<RevokationState> UpdateUserStatut(string userId, bool statut);

        Task<User> GetUser(string Id, IEnumerable<string> countries);

        Task CheckUser(string Id, IEnumerable<string> countries);
        bool IsAadIdentity(ClaimsIdentity user);
        bool IsAppIdentity(ClaimsIdentity user);

        Task<AzureFunctionResponse> ChangeMFAStatus(string userPrincipalName, string status);

        Task<AzureFunctionResponse> RevokeSPOSignIn(string userPrincipalName);

        Task<AzureFunctionResponse> GetMFAStatus(string userPrincipalName);
    }
}