﻿using System.Security.Cryptography.X509Certificates;

namespace Sonepar.O365AdminToolbox.Domain
{
    public interface ICertificateStore
    {
        X509Certificate2 GetCert();
    }
}