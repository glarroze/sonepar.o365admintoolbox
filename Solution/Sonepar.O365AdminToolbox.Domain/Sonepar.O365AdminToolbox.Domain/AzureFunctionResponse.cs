﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sonepar.O365AdminToolbox.Domain
{
    public class AzureFunctionResponse
    {
        public string userPrincipalName { get; set; }
        public string userStatut { get; set; }
    }
}