﻿using System;

namespace Sonepar.O365AdminToolbox.Domain
{
    public class Service
    {
        public Guid   Id { get; set; }
        public string FriendlyName { get; set; }
        public string Name { get; set; }
        public string AppliesTo { get; set; }
    }
}