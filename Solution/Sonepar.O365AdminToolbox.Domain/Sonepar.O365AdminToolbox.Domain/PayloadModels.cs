﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sonepar.O365AdminToolbox.Domain
{
    public class PagedUsersResponse
    {
        public IEnumerable<User> users { get; set; }

        public string skipToken { get; set; }
    }

    public class RequestPagedUserPayload
    {
        public string skipToken { get; set; }
        public string groupId { get; set; }
        public string groupDisplayName { get; set; }
    }
}
