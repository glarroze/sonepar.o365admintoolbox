﻿using Sonepar.O365AdminToolbox.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sonepar.O365AdminToolbox.Domain
{
    public interface IWorkspaceService
    {
        Task<IEnumerable<OfficeObject>> GetWorkspacesManagedByUser(IEnumerable<string> countries);
        Task<OfficeObject> GetWorkspace(string id, IEnumerable<string> countries);

        Task<List<OfficeObject>> CheckAndFixWorkSpacesUrl();

        Task<IEnumerable<OfficeObject>> GetUnregisteredWorkSpaces();

        Task<OfficeObject> NewWorkspace(Workspace workspace, IEnumerable<string> countries);

        Task<OfficeObject> NewSharePointSite(Workspace workspace);
        Task<OfficeObject> AddWorkspaceOwners(Workspace workspace, IEnumerable<string> countries);

        Task<OfficeObject> UpdateWorkSpace(string workspaceId, string siteUrl, string newTitle, IEnumerable<string> countries);
        Task DeleteWorkspace(string workspaceId, IEnumerable<string> countries);

        Task<IEnumerable<Member>> GetWorkspaceOwners(string id, IEnumerable<string> countries);
        Task AddWorkspaceOwner(string id, string userId, IEnumerable<string> countries);
        Task RemoveWorkspaceOwner(string id, string userId, IEnumerable<string> countries);


    }
}