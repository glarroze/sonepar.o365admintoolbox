﻿using System;
using System.Collections.Generic;
using Sonepar.O365AdminToolbox.Domain;

namespace Sonepar.O365AdminToolbox.Mock
{
    public class NameServiceMock
    {

        static Dictionary<string, string> mapping = new Dictionary<string, string>
        {
            ["ENTERPRISEPREMIUM"] = "Office 365 Enterprise E5",
            ["PROJECTPREMIUM"] = "Project Online Premium",
            ["ADALLOM_S_O365"] = "Office 365 Advanced Security Management",
            ["ATP_ENTERPRISE"] = "Exchange Online Advanced Threat Protection",
            ["BI_AZURE_P2"] = "Power BI Pro",
            ["EQUIVIO_ANALYTICS"] = "Office 365 Advanced eDiscovery",
            ["EXCHANGE_ANALYTICS"] = "Microsoft MyAnalytics",
            ["EXCHANGE_S_ENTERPRISE"] = "Exchange Online (Plan 2)",
            ["FLOW_O365_P3"] = "Flow for Office 365",
            ["INTUNE_O365"] = "Mobile Device Management for Office 365",
            ["LOCKBOX_ENTERPRISE"] = "Customer Lockbox",
            ["MCOEV"] = "Skype for Business Cloud PBX",
            ["MCOMEETADV"] = "Skype for Business PSTN Conferencing",
            ["MCOSTANDARD"] = "Skype for Business Online (Plan 2)",
            ["OFFICESUBSCRIPTION"] = "Office 365 ProPlus",
            ["POWERAPPS_O365_P3"] = "PowerApps for Office 365",
            ["PROJECTWORKMANAGEMENT"] = "Microsoft Planner",
            ["PROJECT_CLIENT_SUBSCRIPTION"] = "Project Online Desktop Client",
            ["RMS_S_ENTERPRISE"] = "Azure Rights Management",
            ["SHAREPOINTENTERPRISE"] = "SharePoint Online (Plan 2)",
            ["SHAREPOINTWAC"] = "Office Online",
            ["SHAREPOINT_PROJECT"] = "Project Online Service",
            ["SWAY"] = "Sway",
            ["TEAMS1"] = "Microsoft Teams",
            ["YAMMER_ENTERPRISE"] = "Yammer Enterprise",
        };

        static Dictionary<Guid, string> mappingId = new Dictionary<Guid, string>();



        public string GetNameOrDefault(string defaultValue, string serviceName, ServiceType type)
        {
            return mapping.ContainsKey(serviceName) ? mapping[serviceName] : defaultValue;
        }

        public void Register(string registeredName, string serviceName, ServiceType Type)
        {
            mapping[serviceName] = registeredName;
        }
    }
}
